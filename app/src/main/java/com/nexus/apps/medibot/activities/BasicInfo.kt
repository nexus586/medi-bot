package com.nexus.apps.medibot.activities

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.*
import com.agrawalsuneet.loaderspack.loaders.PulseLoader
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.models.FBaseUser
import com.nexus.apps.medibot.utils.FIREBASE_REFERENCE
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class BasicInfo : AppCompatActivity() {

    private val TAG = this::class.java.simpleName

    private lateinit var textEmail: TextInputEditText
    private lateinit var textUsername: TextInputEditText
    private lateinit var textFirstName: TextInputEditText
    private lateinit var textLastName: TextInputEditText
    private lateinit var textPhoneNumber: TextInputEditText
    private lateinit var textGender: TextInputEditText
    private lateinit var textDateOfBirth: TextInputEditText
    private lateinit var textLoading: TextView
    private lateinit var genderHolder: TextInputLayout

    private lateinit var genderRadioGroup: RadioGroup
    private lateinit var maleRadioButton: RadioButton
    private lateinit var femaleRadioButton: RadioButton

    private lateinit var dateButton: ImageButton

    private lateinit var toolBar: Toolbar


    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var databaseReference: DatabaseReference

    private lateinit var tempDate: Date

    private lateinit var pulseLoader: PulseLoader

    private lateinit var btnUpdate: Button
    private lateinit var btnEnable: Button

    private var isLoading = true

    private var isEnabled = false

    private lateinit var user: FBaseUser

    private var wantsToUpdate = false

    private var enforceValidation = false

    private var enforceStay = false

    private var date: Date = Date()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basic_info)

        initFirebase()
        initViews()

        if(callingActivity != null) {
            if(callingActivity.className.equals(Home::class.java)) {
                enforceStay = true
                enforceValidation = true
            }
        }

        if (isLoading) {
            hideViews()
        }
    }


    override fun onBackPressed() {
        if(enforceStay) {
            Toast.makeText(this, "Please update your profile", Toast.LENGTH_SHORT).show()
        }
    }

    private fun initViews() {
        textEmail = findViewById(R.id.tv_email)
        textUsername = findViewById(R.id.tv_username)
        textFirstName = findViewById(R.id.tv_first_name)
        textLastName = findViewById(R.id.tv_last_name)
        textGender = findViewById(R.id.tv_gender)
        textPhoneNumber = findViewById(R.id.tv_phone)
        textDateOfBirth = findViewById(R.id.edt_dob)
        btnEnable = findViewById(R.id.btn_enable)
        btnUpdate = findViewById(R.id.btn_update)
        pulseLoader = findViewById(R.id.pulse_loader)
        textLoading = findViewById(R.id.isLoadingText)
        genderHolder = findViewById(R.id.gender_holder)
        genderRadioGroup = findViewById(R.id.gender_group)
        maleRadioButton = findViewById(R.id.gender_male)
        femaleRadioButton = findViewById(R.id.gender_female)
        textDateOfBirth.showSoftInputOnFocus = false
        dateButton = findViewById(R.id.dateButton)

//        toolBar = findViewById(R.id.toolbar)
//        setSupportActionBar(toolBar)

        val actionBar = supportActionBar
        actionBar?.apply {
            title = "Basic Information"
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_outline_profile)
        }
    }

    private fun initFirebase() {
        firebaseAuth = FirebaseAuth.getInstance()
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase.getReference(FIREBASE_REFERENCE)

        retrieveUserDataFromFirebase()
    }

    private fun retrieveUserDataFromFirebase() {

        databaseReference.child("users").child(firebaseAuth.currentUser!!.uid).child("basic_information").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Toast.makeText(this@BasicInfo, "Error Fetching data", Toast.LENGTH_LONG).show()
                Log.e(TAG, p0.toException().toString())
            }

            override fun onDataChange(p0: DataSnapshot) {
                user = FBaseUser()
                user = if(p0.getValue(FBaseUser::class.java) == null) {
                    FBaseUser()
                }else {
                    p0.getValue(FBaseUser::class.java)!!
                }
                Log.i(TAG, "user = $user")
                isLoading = false

                showViews()
                initListeners()
                setUpData()
            }

        })
    }

    private fun initListeners() {
        btnEnable.setOnClickListener {
            if (isEnabled) {
                btnEnable.text = "Enable"
                disableViews()
            } else {
                btnEnable.text = "Disable"
                enableViews()

            }
        }

        dateButton.setOnClickListener {
            showDatePickerDialog()
        }


        btnUpdate.setOnClickListener {
            Log.i(TAG, "gotDate in ms: $date")
            if(enforceValidation) {
                var isValid = false
                isValid = validate()

                if (isValid) {
                    updateUserDetailsOnFirebase()
                }
            }else {
                updateUserDetailsOnFirebase()
            }
        }

        genderRadioGroup.setOnCheckedChangeListener { _, p1 ->
            when (p1) {
                R.id.gender_male -> {
                    textGender.setText(resources.getString(R.string.male))
                }
                R.id.gender_female -> {
                    textGender.setText(resources.getString(R.string.female))
                }
            }
        }
    }

    private fun enableViews(): Boolean {
        textDateOfBirth.isEnabled = true
        textEmail.isEnabled = true
        textPhoneNumber.isEnabled = true
        textLastName.isEnabled = true
        textUsername.isEnabled = true
        textFirstName.isEnabled = true
        btnUpdate.isEnabled = true
        maleRadioButton.isEnabled = true
        femaleRadioButton.isEnabled = true
        genderRadioGroup.isEnabled = true
        dateButton.isEnabled = true

        isEnabled = true
        return isEnabled
    }

    private fun disableViews(): Boolean {
        textDateOfBirth.isEnabled = false
        textEmail.isEnabled = false
        textPhoneNumber.isEnabled = false
        textLastName.isEnabled = false
        textUsername.isEnabled = false
        textFirstName.isEnabled = false
        btnUpdate.isEnabled = false
        maleRadioButton.isEnabled = false
        femaleRadioButton.isEnabled = false
        genderRadioGroup.isEnabled = false
        dateButton.isEnabled = false

        isEnabled = false

        return isEnabled
    }


    private fun showViews() {
//        Visible Views
        textDateOfBirth.visibility = View.VISIBLE
        textEmail.visibility = View.VISIBLE
        textPhoneNumber.visibility = View.VISIBLE
        textLastName.visibility = View.VISIBLE
        textGender.visibility = View.VISIBLE
        textUsername.visibility = View.VISIBLE
        textFirstName.visibility = View.VISIBLE
        btnUpdate.visibility = View.VISIBLE
        btnEnable.visibility = View.VISIBLE
//        cardView.visibility = View.VISIBLE

//        Hidden Views
        textLoading.visibility = View.GONE
        pulseLoader.visibility = View.GONE
    }

    private fun hideViews() {
//        visible views
        pulseLoader.visibility = View.VISIBLE
        textLoading.visibility = View.VISIBLE


//        hidden Views
        textDateOfBirth.visibility = View.GONE
        textEmail.visibility = View.GONE
        textPhoneNumber.visibility = View.GONE
        textLastName.visibility = View.GONE
        textGender.visibility = View.GONE
        textUsername.visibility = View.GONE
        textFirstName.visibility = View.GONE
        btnEnable.visibility = View.GONE
        btnUpdate.visibility = View.GONE
//        cardView.visibility = View.GONE
    }

    private fun setUpData() {

        textFirstName.setText(user.first_name ?: "First name")
        textLastName.setText(user.last_name ?: "Last name")
        textGender.setText(user.gender ?: "Gender")

        if (textGender.text.toString().equals("male", true)) {
            maleRadioButton.isSelected = true
            maleRadioButton.isChecked = true
            maleRadioButton.isActivated = true
        }else if(textGender.text.toString().equals("female", true)) {
            femaleRadioButton.isSelected = true
            femaleRadioButton.isChecked = true
            femaleRadioButton.isActivated = true
        }

        textPhoneNumber.setText(user.phoneNumber ?: "Phone Number")
        if(user.dateOfBirth == null){
            textDateOfBirth.setText("Date of Birth")
        }else {
            textDateOfBirth.setText(formatDate(user.dateOfBirth!!))
        }

        if (user.dateOfBirth != null) {
            tempDate = user.dateOfBirth!!
        }
        textUsername.setText(user.username ?: "Username")
        textEmail.setText(user.email ?: "Email")
    }

    private fun formatDate(date: Date): String? {
        return DateFormat.getDateInstance(DateFormat.LONG).format(date)
    }

    private fun showDatePickerDialog() {

        val simpleFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)

        val minDate = simpleFormat.parse("1960-01-01")


        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)

        val datePicker = DatePickerDialog(this, android.R.style.Theme_Material_Dialog, DatePickerDialog.OnDateSetListener { _, p1, p2, p3 ->
            val gotDate = Calendar.getInstance()
            gotDate.set(p1, p2, p3)
            date = Date(gotDate.timeInMillis)
            textDateOfBirth.setText(formatDate(date))
        }, year, month, day)
        datePicker.datePicker.minDate = minDate.time
        datePicker.datePicker.maxDate = Date().time
        datePicker.show()
    }


    private fun updateUserDetailsOnFirebase() {

        if (user.dateOfBirth == null) {
                user.dateOfBirth = date
            } else {
                user.dateOfBirth = tempDate

                if ((date.before(tempDate) && (date != Date())) || (date.after(tempDate) && (date != Date()))) {
                    user.dateOfBirth = date
                }
            }

            user.gender = textGender.text.toString().toLowerCase()

            databaseReference.child("users").child(firebaseAuth.currentUser!!.uid).child("basic_information").setValue(user).addOnCompleteListener { task ->

                if (task.isSuccessful) {
                    Toast.makeText(this@BasicInfo, "Update Completed Successfully", Toast.LENGTH_LONG).show()
                    this@BasicInfo.onBackPressed()
                } else {
                    Log.e(TAG, task.exception.toString())
                    Toast.makeText(this@BasicInfo, "Unable to update profile. Please try again later", Toast.LENGTH_LONG).show()
                    this.onBackPressed()
                }
            }

    }

    private fun validate(): Boolean {
        clearErrors()

        if (textUsername.text.isNullOrEmpty()
                || textFirstName.text.isNullOrEmpty()
                || textLastName.text.isNullOrEmpty()
                || textPhoneNumber.text.isNullOrEmpty()
                || textEmail.text.isNullOrEmpty()
                || textGender.text.isNullOrEmpty()
                || textDateOfBirth.text.isNullOrEmpty()){
            return false
            showErrorDialog("Required fields are empty or missing")
        } else if (!Patterns.EMAIL_ADDRESS.matcher(textEmail.text.toString()).matches()){
            return false
            showErrorDialog("Please enter a valid email.")
        }

        return true
    }

    private fun clearErrors() {
        textUsername.error = null
        textFirstName.error = null
        textLastName.error = null
        textPhoneNumber.error = null
        textEmail.error = null
        textGender.error = null
        textDateOfBirth.error = null

    }

    private fun showErrorDialog(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
                .setMessage(message)
                .setCancelable(true)
                .show()
    }
}
