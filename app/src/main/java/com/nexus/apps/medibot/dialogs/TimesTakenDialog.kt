package com.nexus.apps.medibot.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.nexus.apps.medibot.R

class TimesTakenDialog(context: Context, var data: String) : Dialog(context) {

    private lateinit var recycler: TextView
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.times_taken_dialog)

        initView()
    }


    private fun initView() {
        recycler = findViewById(R.id.times_taken_text_view)
        button = findViewById(R.id.closeButton)
        recycler.text = data
        setCancelable(true)

        button.setOnClickListener {
            this.dismiss()
        }
    }
}