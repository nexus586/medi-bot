package com.nexus.apps.medibot.utils

fun getFaqs(): HashMap<String, ArrayList<String>> {
    val faq = HashMap<String, ArrayList<String>>()

    val diag = ArrayList<String>()
    diag.add("We use an artificial intelligence to process your symptoms and based on your symptoms we try to narrow down the symptoms to check what illness matches your symptoms")

    val data = ArrayList<String>()
    data.add("For user privacy and autonomy, no one has access to your data except you the user. We do not send your personal information to anyone, we only use it to personalize your experience")




    faq["How does the diagnosis work?"] = diag
    faq["Who has access to my data?"] = data
    return faq
}