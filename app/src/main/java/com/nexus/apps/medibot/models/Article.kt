package com.nexus.apps.medibot.models

import java.util.*

data class Article(
        var source: Source,
        var author: String,
        var title: String,
        var description: String,
        var url: String,
        var imgUrl: String,
        var publishedAt: Date? = null,
        var content: String
)