package com.nexus.apps.medibot.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ExpandableListView
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.adapters.FaqAdapter
import com.nexus.apps.medibot.utils.getFaqs

class Faq : AppCompatActivity() {

    private lateinit var expandableListView: ExpandableListView
    private lateinit var adapter: FaqAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_faq)

        initViews()
        initAdapters()
    }

    private fun initViews() {
        expandableListView = findViewById(R.id.faq_list)
        title = "FAQs"
    }

    private fun initAdapters() {

        val data = getFaqs()
        val headers = ArrayList(data.keys)
        actionBar?.apply{
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_back)
        }

        adapter = FaqAdapter(this, headers, data)

        expandableListView.setAdapter(adapter)

    }
}
