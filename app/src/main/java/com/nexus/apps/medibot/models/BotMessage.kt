package com.nexus.apps.medibot.models

data class BotMessage(
        var message: String = "",
        var type: String = "",
        var option1: String = "",
        var option2: String = "",
        var option3: String = "",
        var option4: String = "",
        var option1Id: String = "",
        var option2Id: String = "",
        var option3Id: String = "",
        var option4Id: String = "",
        var symptomId: String = "",
        var groupMultipleQuestion: String = "",
        var senderId: Int = 0,
        var symptoms: ArrayList<Symptoms>? = null,
        var conditions: ArrayList<Condition>? = null
)