package com.nexus.apps.medibot.adapters

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.activities.AnonymousDiagnosis
import com.nexus.apps.medibot.activities.PersonalDiagnosis
import com.nexus.apps.medibot.models.BotMessage
import com.nexus.apps.medibot.utils.*

class BotAdapter(private var context: Context, private var botMessages: ArrayList<BotMessage>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG = this::class.java.simpleName

    private val OPPONENT = 2

    override fun getItemViewType(position: Int): Int {
        val isOutgoing = botMessages[position].senderId == 1

        val me = 1
        return if (isOutgoing)
            me
        else
            OPPONENT
    }

    fun add(botMessage: BotMessage) {
        botMessages.add(botMessage)
    }

    fun add(botMessages: ArrayList<BotMessage>) {
        for (botMessage in botMessages) {
            botMessages.add(botMessage)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewHolder: RecyclerView.ViewHolder
        val inflater = LayoutInflater.from(parent.context)

        viewHolder = when (viewType) {
            OPPONENT -> {
                if (context is AnonymousDiagnosis) {
                    val view = inflater.inflate(R.layout.anonymous_bot_message, parent, false)
                    ViewHolder1(view)
                } else {
                    val view = inflater.inflate(R.layout.bot_message, parent, false)
                    ViewHolder1(view)
                }
            }
            else -> {
                if (context is AnonymousDiagnosis) {
                    val view = inflater.inflate(R.layout.anonymous_user_message, parent, false)
                    ViewHolder(view)
                } else {
                    val view = inflater.inflate(R.layout.user_message, parent, false)
                    ViewHolder(view)
                }
            }
        }

        return viewHolder
    }

    override fun getItemCount(): Int = botMessages.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewType = holder.itemViewType
        when (viewType) {
            OPPONENT -> {
                val viewHolder1: ViewHolder1 = holder as ViewHolder1
                configureViewHolder1(viewHolder1, position)
            }

            else -> {
                val viewHolder: ViewHolder = holder as ViewHolder
                configureViewHolder(viewHolder, position)
            }
        }
    }

    private fun configureViewHolder(viewHolder: ViewHolder, position: Int) {
        val botMessage = botMessages[position]
        setViewVisibility(viewHolder.text_message_view, View.VISIBLE)
        viewHolder.textMessage.text = botMessage.message
    }

    private fun configureViewHolder1(viewHolder1: ViewHolder1, position: Int) {
        val botMessage = botMessages[position]

        when {
            botMessage.type.equals(TYPE_SINGLE, true) -> {
                resetOpponentUI(viewHolder1)
                setViewVisibility(viewHolder1.text_message_view_options, View.VISIBLE)
                setViewVisibility(viewHolder1.options_layout, View.VISIBLE)
                viewHolder1.message.text = botMessage.message

                try {
                    if (!botMessage.option1.equals(EMPTY, true)) {
                        setViewVisibility(viewHolder1.option1, View.VISIBLE)
                        viewHolder1.option1.text = botMessage.option1

                        viewHolder1.option1.setOnClickListener {
                            if (context is PersonalDiagnosis) {
                                when {
                                    botMessage.option1Id.equals("affirm", true) -> {
                                        (context as PersonalDiagnosis).sendMyMessage(botMessage.option1)
                                        (context as PersonalDiagnosis).continueWithSymptoms()
                                    }
                                    else -> {
                                        (context as PersonalDiagnosis).sendMyReplyMessage(botMessage.option1, botMessage.symptomId, "present")
                                    }
                                }
                                removeOptions(position, botMessage.message)
                            } else if (context is AnonymousDiagnosis) {
                                when {
                                    botMessage.option1.equals("male", true) -> {
                                        (context as AnonymousDiagnosis).sendMyMessage(botMessage.option1)
                                    }
                                    botMessage.option1Id.equals("affirm", true) -> {
                                        (context as AnonymousDiagnosis).sendMyMessage(botMessage.option1)
                                        (context as AnonymousDiagnosis).continueWithSymptoms()
                                    }
                                    else -> (context as AnonymousDiagnosis).sendMyReplyMessage(botMessage.option1, botMessage.symptomId, "present")
                                }
                            }
                        }
                    }
                } catch (ex: Exception) {
                    Log.e(TAG, ex.message)
                }

                try {
                    if (!botMessage.option2.equals(EMPTY, true)) {
                        setViewVisibility(viewHolder1.option2, View.VISIBLE)
                        viewHolder1.option2.text = botMessage.option2

                        viewHolder1.option2.setOnClickListener {
                            viewHolder1.options_layout.visibility = View.GONE
                            if (context is PersonalDiagnosis) {
                                when{
                                    botMessage.option2Id.equals("negate", true) -> {
                                        (context as PersonalDiagnosis).sendMyMessage(botMessage.option2)
                                        (context as PersonalDiagnosis).beginDiagnosis()
                                    }
                                    else -> {
                                        (context as PersonalDiagnosis).sendMyReplyMessage(botMessage.option2, botMessage.symptomId, "present")
                                    }
                                }

                                removeOptions(position, botMessage.message)
                            } else if (context is AnonymousDiagnosis) {
                                if (botMessage.option2.equals("female", true))
                                    (context as AnonymousDiagnosis).sendMyMessage(botMessage.option2)
                                else if(botMessage.option2Id.equals("negate", true)) {
                                    (context as AnonymousDiagnosis).sendMyMessage(botMessage.option2)
                                    (context as AnonymousDiagnosis).beginDiagnosis()
                                }
                                else
                                    (context as AnonymousDiagnosis).sendMyReplyMessage(botMessage.option2, botMessage.symptomId, "present")

                                removeOptions(position, botMessage.message)
                            }

                        }
                    }
                } catch (ex: Exception) {
                    Log.e(TAG, ex.message)
                }

                try {
                    if (!botMessage.option3.equals(EMPTY, true)) {
                        setViewVisibility(viewHolder1.option3, View.VISIBLE)
                        viewHolder1.option3.text = botMessage.option3

                        viewHolder1.option3.setOnClickListener {
                            if (context is PersonalDiagnosis) {
                                (context as PersonalDiagnosis).sendMyReplyMessage(botMessage.option3, botMessage.symptomId, "unknown")

                                removeOptions(position, botMessage.message)
                            } else if (context is AnonymousDiagnosis) {
                                (context as AnonymousDiagnosis).sendMyReplyMessage(botMessage.option3, botMessage.symptomId, "unknown")

                                removeOptions(position, botMessage.message)
                            }

                        }
                    }
                } catch (ex: Exception) {
                    Log.e(TAG, ex.message)
                }

            }
            botMessage.type.equals(TYPE_TEXT, true) -> {
                resetOpponentUI(viewHolder1)
                setViewVisibility(viewHolder1.text_message_view, View.VISIBLE)
                viewHolder1.txtMessage.text = botMessage.message
            }
            botMessage.type.equals(TYPE_GROUP_SINGLE, true) -> {
                resetOpponentUI(viewHolder1)
                viewHolder1.group_message.text = botMessage.message
                setViewVisibility(viewHolder1.text_message_view_group_options, View.VISIBLE)
                setViewVisibility(viewHolder1.group_options_layout, View.VISIBLE)

//            option 1
                try {
                    if (!botMessage.option1.equals(EMPTY, true)) {
                        setViewVisibility(viewHolder1.group_option1, View.VISIBLE)
                        viewHolder1.group_option1.text = botMessage.option1

                        viewHolder1.group_option1.setOnClickListener {
                            if (context is PersonalDiagnosis) {
                                viewHolder1.group_options_layout.visibility = View.GONE
                                (context as PersonalDiagnosis).sendMyReplyMessage(botMessage.option1, botMessage.option1Id, "present")
                            } else if (context is AnonymousDiagnosis) {
                                viewHolder1.group_options_layout.visibility = View.GONE
                                (context as AnonymousDiagnosis).sendMyReplyMessage(botMessage.option1, botMessage.option1Id, "present")
                            }

                            removeOptions(position, botMessage.message)
                        }
                    }
                } catch (ex: Exception) {
                    Log.e(TAG, ex.message)
                }

//            option 2
                try {
                    if (!botMessage.option2.equals(EMPTY, true)) {
                        setViewVisibility(viewHolder1.group_option2, View.VISIBLE)
                        viewHolder1.group_option2.text = botMessage.option2

                        viewHolder1.group_option2.setOnClickListener {
                            if (context is PersonalDiagnosis) {
                                viewHolder1.group_options_layout.visibility = View.GONE
                                (context as PersonalDiagnosis).sendMyReplyMessage(botMessage.option2, botMessage.option2Id, "present")
                            } else if (context is AnonymousDiagnosis) {
                                viewHolder1.group_options_layout.visibility = View.GONE
                                (context as AnonymousDiagnosis).sendMyReplyMessage(botMessage.option2, botMessage.option2Id, "present")
                            }

                            removeOptions(position, botMessage.message)
                        }
                    }
                } catch (ex: Exception) {
                    Log.e(TAG, ex.message)
                }
//            option 3

                try {
                    if (!botMessage.option3.equals(EMPTY, true)) {
                        setViewVisibility(viewHolder1.group_option3, View.VISIBLE)
                        viewHolder1.group_option3.text = botMessage.option3

                        viewHolder1.group_option3.setOnClickListener {
                            if (context is PersonalDiagnosis) {
                                viewHolder1.group_options_layout.visibility = View.GONE
                                (context as PersonalDiagnosis).sendMyReplyMessage(botMessage.option3, botMessage.option3Id, "present")
                            } else if (context is AnonymousDiagnosis) {
                                viewHolder1.group_options_layout.visibility = View.GONE
                                (context as AnonymousDiagnosis).sendMyReplyMessage(botMessage.option3, botMessage.option3Id, "present")
                            }

                            removeOptions(position, botMessage.message)
                        }
                    }
                } catch (ex: Exception) {
                    Log.e(TAG, ex.message)
                }

//            option 4
                try {
                    if (!botMessage.option4.equals(EMPTY, true)) {
                        setViewVisibility(viewHolder1.group_option4, View.VISIBLE)
                        viewHolder1.group_option4.text = botMessage.option4

                        viewHolder1.group_option4.setOnClickListener {
                            if (context is PersonalDiagnosis) {
                                viewHolder1.group_options_layout.visibility = View.GONE
                                (context as PersonalDiagnosis).sendMyReplyMessage(botMessage.option4, botMessage.option4Id, "present")
                            } else if (context is AnonymousDiagnosis) {
                                viewHolder1.group_options_layout.visibility = View.GONE
                                (context as AnonymousDiagnosis).sendMyReplyMessage(botMessage.option4, botMessage.option4Id, "present")
                            }

                            removeOptions(position, botMessage.message)
                        }
                    }
                } catch (ex: Exception) {
                    Log.e(TAG, ex.message)
                }
            }
            botMessage.type.equals(TYPE_GROUP_MULTIPLE, true) -> {

                resetOpponentUI(viewHolder1)
                viewHolder1.message.text = botMessage.message
                setViewVisibility(viewHolder1.text_message_view_options, View.VISIBLE)
                setViewVisibility(viewHolder1.options_layout, View.VISIBLE)

//            option 1
                try {
                    if (!botMessage.option1.equals(EMPTY, true)) {
                        setViewVisibility(viewHolder1.option1, View.VISIBLE)
                        viewHolder1.option1.text = botMessage.option1

                        viewHolder1.option1.setOnClickListener {
                            if (context is PersonalDiagnosis) {
                                (context as PersonalDiagnosis).sendMyReplyMessageWithoutQuery(botMessage.option1, botMessage.symptomId, "present")
                            } else if (context is AnonymousDiagnosis) {
                                (context as AnonymousDiagnosis).sendMyReplyMessageWithoutQuery(botMessage.option1, botMessage.symptomId, "present")
                            }
                            removeOptions(position, botMessage.message)
                        }
                    }
                } catch (ex: Exception) {
                    Log.e(TAG, ex.message)
                }

//            option 2
                try {
                    if (!botMessage.option2.equals(EMPTY, true)) {
                        setViewVisibility(viewHolder1.option2, View.VISIBLE)
                        viewHolder1.option2.text = botMessage.option2

                        viewHolder1.option2.setOnClickListener {
                            viewHolder1.options_layout.visibility = View.GONE
                            if (context is PersonalDiagnosis) {
                                (context as PersonalDiagnosis).sendMyReplyMessageWithoutQuery(botMessage.option2, botMessage.symptomId, "absent")
                            } else if (context is AnonymousDiagnosis) {
                                (context as AnonymousDiagnosis).sendMyReplyMessageWithoutQuery(botMessage.option2, botMessage.symptomId, "absent")
                            }
                            removeOptions(position, botMessage.message)
                        }
                    }
                } catch (ex: Exception) {
                    return
                }

//            option 3
                try {
                    if (!botMessage.option3.equals(EMPTY, true)) {
                        setViewVisibility(viewHolder1.option3, View.VISIBLE)
                        viewHolder1.option3.text = botMessage.option3

                        viewHolder1.option3.setOnClickListener {
                            if (context is PersonalDiagnosis) {
                                (context as PersonalDiagnosis).sendMyReplyMessageWithoutQuery(botMessage.option3, botMessage.symptomId, "unknown")
                            } else if (context is AnonymousDiagnosis) {
                                (context as AnonymousDiagnosis).sendMyReplyMessageWithoutQuery(botMessage.option3, botMessage.symptomId, "unknown")
                            }
                            removeOptions(position, botMessage.message)
                        }
                    }
                } catch (ex: Exception) {
                    Log.e(TAG, ex.message)
                }

            }
            botMessage.type.equals(TYPE_DIAGNOSIS_RESULTS, true) -> {

                resetOpponentUI(viewHolder1)
                setViewVisibility(viewHolder1.conditions, View.VISIBLE)

                val adapter = DiagnosisResultsAdapter(context, botMessage.conditions!!)
                val manager = LinearLayoutManager(context)
                manager.orientation = LinearLayoutManager.HORIZONTAL
                viewHolder1.conditions.layoutManager = manager
                viewHolder1.conditions.adapter = adapter
            }
        }
    }

    fun getMessages(): ArrayList<BotMessage> {
        return this.botMessages
    }

    private fun removeOptions(position: Int, message: String) {
        val botMessage = BotMessage(
                senderId = 2,
                message = message,
                type = TYPE_TEXT
        )

        botMessages[position] = botMessage
        notifyItemChanged(position)
    }

    private fun resetOpponentUI(viewHolder: ViewHolder1) {
        setViewVisibility(viewHolder.text_message_view_options, View.GONE)
        setViewVisibility(viewHolder.option1, View.GONE)
        setViewVisibility(viewHolder.option2, View.GONE)
        setViewVisibility(viewHolder.option3, View.GONE)

        setViewVisibility(viewHolder.group_option1, View.GONE)
        setViewVisibility(viewHolder.group_option2, View.GONE)
        setViewVisibility(viewHolder.group_option3, View.GONE)
        setViewVisibility(viewHolder.group_option4, View.GONE)
        setViewVisibility(viewHolder.group_options_layout, View.GONE)
        setViewVisibility(viewHolder.text_message_view_group_options, View.GONE)
        setViewVisibility(viewHolder.options_layout, View.GONE)
        setViewVisibility(viewHolder.text_message_view, View.GONE)
        setViewVisibility(viewHolder.conditions, View.GONE)
    }

    private fun setViewVisibility(view: View, visibility: Int) {
        view.visibility = visibility
    }

    private class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textMessage: TextView
        var text_message_view: LinearLayout

        init {
            textMessage = itemView.findViewById(R.id.txtMessage)
            text_message_view = itemView.findViewById(R.id.text_message_view)
        }
    }


    private class ViewHolder1(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //        Text Views
        var txtMessage: TextView
        var message: TextView
        var option1: TextView
        var option2: TextView
        var option3: TextView
        var group_message: TextView
        var group_option1: TextView
        var group_option2: TextView
        var group_option3: TextView
        var group_option4: TextView

//        Recycler View

        var conditions: RecyclerView


        //        Linear Layouts
        var text_message_view_options: LinearLayout
        var group_options_layout: LinearLayout
        var options_layout: LinearLayout
        var text_message_view_group_options: LinearLayout
        var text_message_view: LinearLayout


        init {
//            init text views
            txtMessage = itemView.findViewById(R.id.txtMessage)
            message = itemView.findViewById(R.id.message)
            option1 = itemView.findViewById(R.id.option1)
            option2 = itemView.findViewById(R.id.option2)
            option3 = itemView.findViewById(R.id.option3)
            group_message = itemView.findViewById(R.id.group_message)
            group_option1 = itemView.findViewById(R.id.group_option1)
            group_option2 = itemView.findViewById(R.id.group_option2)
            group_option3 = itemView.findViewById(R.id.group_option3)
            group_option4 = itemView.findViewById(R.id.group_option4)

//            init recycler view
            conditions = itemView.findViewById(R.id.conditions)

//            init linear layouts
            text_message_view_options = itemView.findViewById(R.id.text_message_view_options)
            group_options_layout = itemView.findViewById(R.id.group_options_layout)
            options_layout = itemView.findViewById(R.id.options_layout)
            text_message_view = itemView.findViewById(R.id.text_message_view)
            text_message_view_group_options = itemView.findViewById(R.id.text_message_view_group_options)
        }

    }
}