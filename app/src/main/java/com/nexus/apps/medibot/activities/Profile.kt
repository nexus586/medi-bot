package com.nexus.apps.medibot.activities

import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.widget.GridView
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.adapters.ProfileActionAdapter
import com.nexus.apps.medibot.models.ProfileAction
import com.nexus.apps.medibot.utils.GlideApp
import com.nexus.apps.medibot.utils.PrefsManager
import jp.wasabeef.glide.transformations.BlurTransformation
import jp.wasabeef.glide.transformations.GrayscaleTransformation
import jp.wasabeef.glide.transformations.gpu.SepiaFilterTransformation
import me.toptas.fancyshowcase.FancyShowCaseView
import smartdevelop.ir.eram.showcaseviewlib.GuideView
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity

class Profile : AppCompatActivity() {

//    private lateinit var username: TextView
    private lateinit var gridView: GridView
    private lateinit var toolBar: Toolbar
    private lateinit var mAuth: FirebaseAuth
    private lateinit var profileImage: ImageView
    private lateinit var collapseActionView: CollapsingToolbarLayout
    private lateinit var prefsManager: PrefsManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        initFirebase()
        initViews()
        initAdapter()
    }

    private fun initViews() {
//        username = findViewById(R.id.tv_user_display_name)
        prefsManager = PrefsManager(this)
        gridView = findViewById(R.id.profile_action_container)
        toolBar = findViewById(R.id.toolbar)
        profileImage = findViewById(R.id.profile_image)
        setSupportActionBar(toolBar)
        collapseActionView = findViewById(R.id.collapseActionView)

        collapseActionView.apply {
            setExpandedTitleColor(Color.WHITE)
            setCollapsedTitleTextColor(Color.WHITE)
            setContentScrimColor(resources.getColor(R.color.colorPrimary))
        }

        if(!prefsManager.getHasSeenProfileTip()) {
            GuideView.Builder(this)
                    .setTargetView(profileImage)
                    .setTitle("Tip")
                    .setContentText("Scroll this view up to see more options")
                    .setDismissType(DismissType.anywhere)
                    .setGravity(Gravity.auto)
                    .build()
                    .show()
            prefsManager.setHasSeenProfileTip()
        }


//        username.text = mAuth.currentUser!!.displayName

        Log.d(this::class.java.simpleName, "photoURL: ${mAuth.currentUser?.photoUrl}")

//        Toast.makeText(this, "photoURL ${mAuth.currentUser?.photoUrl}", Toast.LENGTH_SHORT).show()

        if(mAuth.currentUser?.photoUrl != null) {
            GlideApp.with(this)
                    .load(mAuth.currentUser!!.photoUrl)
                    .fallback(R.drawable.background_doodle)
                    .placeholder(R.drawable.background_doodle)
                    .apply(RequestOptions.bitmapTransform(GrayscaleTransformation()))
                    .override(profileImage.width, 200)
                    .into(profileImage)
        }

        val actionBar: ActionBar? = supportActionBar
        actionBar?.apply {
            setHomeButtonEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_outline_home)
            setDisplayHomeAsUpEnabled(true)
            title = "My Profile"
        }
    }

    private fun initFirebase() {
        mAuth = FirebaseAuth.getInstance()
    }

    private fun initAdapter() {
        val adapter = ProfileActionAdapter(this, getData())
        gridView.adapter = adapter
    }

    private fun getData(): ArrayList<ProfileAction> {
        val list: ArrayList<ProfileAction> = ArrayList()

        var action = ProfileAction(name = "Basic Info", drawable = R.drawable.basic_info)
        list.add(action)

        action = ProfileAction(name = "Health Info", drawable = R.drawable.health_info)
        list.add(action)

        action = ProfileAction(name = "Medication", drawable = R.drawable.medication)
        list.add(action)

        action = ProfileAction(name = "Allergies", drawable = R.drawable.allergy)
        list.add(action)

        action = ProfileAction(name = "History", drawable = R.drawable.chat_history)
        list.add(action)

        action = ProfileAction(name = "Statistics", drawable = R.drawable.stats)
        list.add(action)

        return list
    }
}
