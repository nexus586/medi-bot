package com.nexus.apps.medibot.utils

import android.util.Log
import com.nexus.apps.medibot.models.*
import org.json.JSONException
import org.json.JSONObject

@Throws(JSONException::class)
fun getDiagnosis(jsonObject: JSONObject): DiagnosisObj {
    val question = JSONObject(jsonObject.getString("question"))

    val symptoms: ArrayList<Symptoms> = ArrayList()
    val conditions: ArrayList<Condition> = ArrayList()

    val diagnosisObj = DiagnosisObj(question = " ", type = question.getString("type"))

    if (diagnosisObj.type.equals(TYPE_GROUP_MULTIPLE, true)) {
        diagnosisObj.extraQuestion = question.getString("text")
    } else {
        diagnosisObj.question = question.getString("text")
    }

    val questionItems = question.getJSONArray("items")
    for (i in (0 until questionItems.length())) {
        val c = questionItems.getJSONObject(i)
        val symptom = Symptoms(id = c.getString("id"), name = c.getString("name"))

        symptoms.add(symptom)
    }

    diagnosisObj.symptoms = symptoms

    val conditionItems = jsonObject.getJSONArray("conditions")
    for (i in (0 until conditionItems.length())) {
        val c = conditionItems.getJSONObject(i)
        val condition = Condition(c.getString("id"), name = c.getString("name"), probability = (c.getDouble("probability") * 100).toInt())
        conditions.add(condition)
    }

    diagnosisObj.conditions = conditions

    Log.d("Response Utils Test", "diagnosisObj: $diagnosisObj")
    return diagnosisObj
}

fun convertDiagnosisToBotMessage(diagnosisObj: DiagnosisObj): BotMessage {
    val botMessage = BotMessage(senderId = 2, type = diagnosisObj.type, message = diagnosisObj.question)

    when {
        diagnosisObj.type.equals(TYPE_SINGLE, true) -> {
            botMessage.option1 = "Yes"
            botMessage.option2 = "No"
            botMessage.option3 = "Don't know"
            botMessage.symptomId = diagnosisObj.symptoms!![0].id
        }
        diagnosisObj.type.equals(TYPE_GROUP_SINGLE, true) -> {
            val symptoms: ArrayList<Symptoms> = diagnosisObj.symptoms!!

            for (i in (0 until symptoms.size)) {
                val symptom = symptoms[i]

                when (i) {
                    0 -> {
                        botMessage.option1 = symptom.name; botMessage.option1Id = symptom.id
                    }
                    1 -> {
                        botMessage.option2 = symptom.name; botMessage.option2Id = symptom.id
                    }
                    2 -> {
                        botMessage.option3 = symptom.name; botMessage.option3Id = symptom.id
                    }
                    3 -> {
                        botMessage.option4 = symptom.name; botMessage.option4Id = symptom.id
                    }
                }
            }
        }
        diagnosisObj.type.equals(TYPE_GROUP_MULTIPLE, true) -> {
            botMessage.groupMultipleQuestion = diagnosisObj.question
            botMessage.symptoms = diagnosisObj.symptoms
            botMessage.option1 = "Yes"
            botMessage.option2 = "No"
            botMessage.option3 = "Don't know"
        }
    }

    Log.d("nexusTest: ", "botMessage: $botMessage")

    return botMessage
}

@Throws(JSONException::class)
fun getConditionFromResults(jsonObject: JSONObject): Condition {

    return Condition(name = jsonObject.getString("name"),
            prevalence = jsonObject.getString("prevalence"),
            severity = jsonObject.getString("severity"),
            common_name = jsonObject.getString("common_name"),
            diagnosis = jsonObject.getJSONObject("extras").getString("hint"))
}

@Throws(JSONException::class)
fun getIssueFromResults(jsonObject: JSONObject): Issue {

    return Issue(
            ProfName = jsonObject.getString("ProfName"),
            Id = jsonObject.getInt("ID"),
            IcdName = jsonObject.getString("IcdName"),
            Name = jsonObject.getString("Name"),
            Accuracy = jsonObject.getInt("Accuracy").toFloat()
    )
}

fun getDiag(jsonObject: JSONObject): Diag {
    val diag = Diag()

    var issues: Issue
    val specialization: ArrayList<Specialization> = ArrayList()

    val issueObj = jsonObject.getJSONObject("Issue")
    issues = getIssueFromResults(issueObj)

    val specObj = jsonObject.getJSONArray("Specialisation")
    for (i in (0 until specObj.length())) {
        val c = specObj.getJSONObject(i)
        val obj = getSpecializationFromResult(c)
        specialization.add(obj)
    }

    diag.issue = issues
    diag.specialization = specialization

    return diag
}

@Throws(JSONException::class)
fun getSpecializationFromResult(jsonObject: JSONObject): Specialization {
    return Specialization(name = jsonObject.getString("Name"),
            id = jsonObject.getInt("ID"),
            specialistId = jsonObject.getInt("SpecialistID"))
}

@Throws(JSONException::class)
fun getIssueInfoFromResults(jsonObject: JSONObject): IssueInfo {
    return IssueInfo(
            description = jsonObject.getString("Description"),
            descriptionShort = jsonObject.getString("DescriptionShort"),
            medicalCondition = jsonObject.getString("MedicalCondition"),
            name = jsonObject.getString("Name"),
            possibleSymptoms = jsonObject.getString("PossibleSymptoms"),
            profName = jsonObject.getString("ProfName"),
            synonyms = jsonObject.getString("Synonyms"),
            treatmentDescription = jsonObject.getString("TreatmentDescription")
    )
}

@Throws(JSONException::class)
fun getArticlesFromResult(jsonObject: JSONObject): ArrayList<Article> {
    val articles = jsonObject.getJSONArray("articles")
    val articleList = ArrayList<Article>()

    for (i in (0 until articles.length())) {
        val article = articles.getJSONObject(i)
        val c = convertResultToArticle(article)
        articleList.add(c)
    }

    return articleList
}

@Throws(JSONException::class)
fun convertResultToArticle(jsonObject: JSONObject): Article {
    return Article(
            author = jsonObject.getString("author"),
            description = jsonObject.getString("description"),
            source = convertObjectToSource(jsonObject.getJSONObject("source")),
            title = jsonObject.getString("title"),
            url = jsonObject.getString("url"),
            imgUrl = jsonObject.getString("urlToImage"),
            content = jsonObject.getString("content")
    )
}

@Throws(JSONException::class)
fun convertObjectToSource(jsonObject: JSONObject): Source {
    return Source(
            id = null,
            name = jsonObject.getString("name")
    )
}
