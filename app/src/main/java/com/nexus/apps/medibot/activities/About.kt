package com.nexus.apps.medibot.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.nexus.apps.medibot.BuildConfig
import com.nexus.apps.medibot.R

class About : AppCompatActivity() {

    private lateinit var versionText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        initViews()
    }

    private fun initViews() {
        title = "About"
        versionText = findViewById(R.id.version_text)
        actionBar?.apply{
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_back)
        }

        versionText.text = String.format("Version: %s", getVersionName())
    }

    private fun getVersionName(): String {
        return BuildConfig.VERSION_NAME
    }
}
