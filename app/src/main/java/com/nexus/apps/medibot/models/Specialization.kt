package com.nexus.apps.medibot.models

data class Specialization(
        var name: String? = "",
        var id: Int? = 0,
        var specialistId: Int? = 0
)