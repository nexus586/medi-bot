package com.nexus.apps.medibot.adapters

import android.content.Context
import android.net.Uri
import android.support.customtabs.CustomTabsIntent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.models.Specialization

class SpecialistAdapter(private var context: Context, private var specialist: ArrayList<Specialization>) : RecyclerView.Adapter<SpecialistAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewHolder = LayoutInflater.from(parent.context).inflate(R.layout.specialist_card_item, parent, false)
        return ViewHolder(viewHolder)
    }

    override fun getItemCount(): Int {
        return this.specialist.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val specialist = specialist[position]
        val specialistName = specialist.name

        if (specialist.name!!.endsWith("gy", true)) {
            specialistName?.replace("gy", "gist")
        }

        holder.specialistText.text = specialistName

        holder.cardView.setOnClickListener {
            //            Log.d("nexusTest", "condition name: ${condition.common_name}")
            var urlTextString = ""
            urlTextString = if (specialist.name!!.contains(" ")) {
                specialist.name!!.replace(" ", "_")
            } else {
                specialist.name!!
            }
            val url = "https://en.wikipedia.org/wiki/" + urlTextString.toLowerCase()

            Log.d("nexusTest", "nhsUrl :$url")
            val tabBuilder = CustomTabsIntent.Builder()
            tabBuilder.setToolbarColor(context.resources.getColor(R.color.colorPrimary))
            val tabsIntent = tabBuilder.build()
            tabsIntent.launchUrl(context, Uri.parse(url))
        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var specialistText: TextView = itemView.findViewById(R.id.specialist_text)
        var cardView: RelativeLayout = itemView.findViewById(R.id.specialist_holder)
    }
}