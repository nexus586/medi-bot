package com.nexus.apps.medibot.models

data class DiagnosisChat(
        var messages: ArrayList<BotMessage> = ArrayList(),
        var id: String = "",
        var date: String = "",
        var recommendation: String = ""
)