package com.nexus.apps.medibot.utils

import android.content.Context
import android.content.SharedPreferences

class PrefsManager(context: Context) {

    private var preferences: SharedPreferences
    private var editor: SharedPreferences.Editor

    private val HAS_SEEN_ONBOARDING = "has_seen_onboarding"
    private val IS_LOGGED_IN = "is_logged_in"
    private val USER_DISPLAY_NAME = "user_display_name"
    private val USER_ID = "user_id"
    private val USER_EMAIL = "user_email"
    private val HAS_SEEN_PERSONAL_DIAGNOSIS_TUTORIAL = "has_seen_personal_diagnosis_tutorial"
    private val HAS_SEEN_ANONYMOUS_DIAGNOSIS_TUTORIAL = "has_seen_anonymous_diagnosis_tutorial"
    private val HAS_SEEN_CLICK_ON_ISSUE_TEXT = "has_seen_click_on_issue_text"
    private val HAS_SEEN_LOGGED_IN_EMAIL = "has_seen_logged_in_email"
    private val PIN = "pin_code"
    private val WANTS_TO_USE_PIN = "wants_to_use_pin"
    private val HAS_SEEN_SECURITY_INTRO = "has_seen_security_intro"
    private val HAS_SEEN_PROFILE_TIP = "has_seen_profile_tip"


    fun getHasSeenOnboarding() = preferences.getBoolean(HAS_SEEN_ONBOARDING, false)

    fun setHasSeenOnboarding() = editor.putBoolean(HAS_SEEN_ONBOARDING, true).apply()

    fun getIsLoggedIn() = preferences.getBoolean(IS_LOGGED_IN, false)

    fun setIsLoggedIn() = editor.putBoolean(IS_LOGGED_IN, true).apply()

    fun setUserDisplayName(name: String) = editor.putString(USER_DISPLAY_NAME, name).apply()

    fun getUserDisplayName() = preferences.getString(USER_DISPLAY_NAME, null)

    fun setUserId(userId: String) = editor.putString(USER_ID, userId).apply()

    fun getUserId() = preferences.getString(USER_ID, null)

    fun setUserEmail(email: String) = editor.putString(USER_EMAIL, email).apply()

    fun getUserEmail() = preferences.getString(USER_EMAIL, null)

    fun getHasSeenClickOnIssueText() = preferences.getBoolean(HAS_SEEN_CLICK_ON_ISSUE_TEXT, false)

    fun setHasSeenClickOnIssueText() = editor.putBoolean(HAS_SEEN_CLICK_ON_ISSUE_TEXT, true).apply()

    fun setHasSeenPersonalDiagnosisTutorial() = editor.putBoolean(HAS_SEEN_PERSONAL_DIAGNOSIS_TUTORIAL, true).apply()

    fun getHasSeenPersonalDiagnosisTutorial() = preferences.getBoolean(HAS_SEEN_PERSONAL_DIAGNOSIS_TUTORIAL, false)

    fun getHasSeenAnonymousDiagnosisTutorial() = preferences.getBoolean(HAS_SEEN_ANONYMOUS_DIAGNOSIS_TUTORIAL, false)

    fun setHasSeenAnonymousDiagnosisTutorial() = editor.putBoolean(HAS_SEEN_ANONYMOUS_DIAGNOSIS_TUTORIAL, true).apply()

    fun setHasSeenLoggedInEmail() = editor.putBoolean(HAS_SEEN_LOGGED_IN_EMAIL, true).apply()

    fun getHasSeenLoggedInEmail() = preferences.getBoolean(HAS_SEEN_LOGGED_IN_EMAIL, false)

    fun setEncryptedPin(pin: String) = editor.putString(PIN, pin).apply()

    fun getEncryptedPin() = preferences.getString(PIN, null)

    fun setWantsToUsePin(bool: Boolean) = editor.putBoolean(WANTS_TO_USE_PIN, bool).apply()

    fun getWantsToUsePin() = preferences.getBoolean(WANTS_TO_USE_PIN, false)

    fun setHasSeenSecurityIntro(seen: Boolean) = editor.putBoolean(HAS_SEEN_SECURITY_INTRO, seen).apply()

    fun getHasSeenSecurityIntro() = preferences.getBoolean(HAS_SEEN_SECURITY_INTRO, false)

    fun setHasSeenProfileTip() = editor.putBoolean(HAS_SEEN_PROFILE_TIP, true).apply()

    fun getHasSeenProfileTip() = preferences.getBoolean(HAS_SEEN_PROFILE_TIP, false)

    fun clearPreferences() {
        editor.clear().apply()
    }

    init {
        val PRIVATE_MODE = 0
        preferences = context.getSharedPreferences("Medibot", PRIVATE_MODE)
        editor = preferences.edit()
    }

}