package com.nexus.apps.medibot.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.activities.AddMedication
import com.nexus.apps.medibot.activities.Medication
import com.nexus.apps.medibot.models.Medications

class MedicationsAdapter(private var context: Context, private var medications: ArrayList<Medications>): RecyclerView.Adapter<MedicationsAdapter.ViewHolder>(){

    private val TAG = this::class.java.simpleName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.med_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return this.medications.size
    }

    override fun onBindViewHolder(holder: MedicationsAdapter.ViewHolder, position: Int) {
        val medication = medications[position]
        holder.medication.text = medication.medicationName
        holder.dosage.text = medication.dosage
        holder.medicationId.text = medication.medicationId

        holder.editButton.setOnClickListener {
            val editMedicationIntent = Intent(context, AddMedication::class.java)
            editMedicationIntent.putExtra("MEDICATION_ID", medication.medicationId)
            context.startActivity(editMedicationIntent)
        }

        holder.deleteButton.setOnClickListener {
            if(context is Medication) {
                (context as Medication).showDeleteDialog(medication.medicationName, medication.medicationId)
            }
        }
    }

    fun add (medication: Medications){
        medications.add(medication)
    }

    fun add (medications: ArrayList<Medications>){
        for (medication in medications){
            medications.add(medication)
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var medication: TextView = itemView.findViewById(R.id.med_name)
        var dosage: TextView = itemView.findViewById(R.id.med_dosage)
        var medicationId: TextView = itemView.findViewById(R.id.med_id)
        var deleteButton: ImageButton = itemView.findViewById(R.id.btn_delete)
        var editButton: ImageButton = itemView.findViewById(R.id.btn_edit)
    }
}