package com.nexus.apps.medibot.activities

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONArrayRequestListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.adapters.SpecialistAdapter
import com.nexus.apps.medibot.adapters.SymptomAdapter
import com.nexus.apps.medibot.models.Diag
import com.nexus.apps.medibot.models.FBaseUser
import com.nexus.apps.medibot.models.NewSymptoms
import com.nexus.apps.medibot.utils.*
import com.yarolegovich.lovelydialog.LovelyInfoDialog
import me.toptas.fancyshowcase.FancyShowCaseView
import org.json.JSONArray
import org.json.JSONObject
import smartdevelop.ir.eram.showcaseviewlib.GuideView
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity
import java.util.*
import kotlin.collections.ArrayList

class SpecialistCenter : AppCompatActivity() {

    private val TAG = this::class.java.simpleName

    private var symptomsJsonArray = JSONArray()
    private var symptoms = ArrayList<NewSymptoms>()
    private var symptomsArray = JSONArray()

    private lateinit var symptomsList: RecyclerView
    private lateinit var controlsContainer: LinearLayout
    private lateinit var btnAdd: Button
    private lateinit var btnSubmit: Button
    private lateinit var issueTextContainer: AutoCompleteTextView

    private lateinit var resultsContainer: CardView
    private lateinit var issueText: TextView
    private lateinit var specialistContainer: RecyclerView
    private lateinit var loadingContainer: RelativeLayout

    private lateinit var mFirebaseAuth: FirebaseAuth
    private lateinit var mFirebaseDatabaseReference: DatabaseReference
    private lateinit var mFirebaseDatabase: FirebaseDatabase

    private lateinit var symptomsAdapter: ArrayAdapter<String>
    private lateinit var specialistAdapter: SpecialistAdapter
    private lateinit var specialistLayoutManager: LinearLayoutManager

    private lateinit var specialSymptomsAdapter: SymptomAdapter

    private lateinit var prefsManager: PrefsManager

    private lateinit var user: FBaseUser
    private var gender = ""
    private var dob = 0

    private var hasLoadedSymptoms = false

    private var token = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_specialist_center)

        prefsManager = PrefsManager(this)
        initFirebase()
        initViews()

        if (hasLoadedSymptoms) {
            initAdapters()
            initListeners()
        } else {
            LoadSymptoms().execute()
        }


    }


    private fun initViews() {
        token = intent.getStringExtra("token")
        symptomsList = findViewById(R.id.symptoms_list)
        controlsContainer = findViewById(R.id.control_container)
        btnAdd = findViewById(R.id.btn_add_symptom)
        btnSubmit = findViewById(R.id.btn_submit)

        issueTextContainer = findViewById(R.id.issue_text_container)
        resultsContainer = findViewById(R.id.results_container)
        issueText = findViewById(R.id.issue_text)
        specialistContainer = findViewById(R.id.specialist_container)
        loadingContainer = findViewById(R.id.loading_container)

        title = "Specialist Center"

        val actionBar: ActionBar? = supportActionBar
        actionBar?.apply {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_back)
        }
    }

    private fun initFirebase() {
        mFirebaseAuth = FirebaseAuth.getInstance()
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mFirebaseDatabaseReference = mFirebaseDatabase.getReference(FIREBASE_REFERENCE)
        getUserDetailsFromFirebase()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = MenuInflater(this)
        inflater.inflate(R.menu.sp_center_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item!!.itemId) {

            R.id.tip -> {
                showGuideView()
                true
            }

            else -> false
        }
    }

    private fun showGuideView() {
        if(resultsContainer.visibility != View.GONE) {
            GuideView.Builder(this)
                    .setTitle("Tip")
                    .setContentText("Click on diagnosis to proceed to issue center")
                    .setDismissType(DismissType.anywhere)
                    .setTargetView(issueText)
                    .build()
                    .show()
        }
    }

    private fun initAdapters() {
        symptomsAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, getSymptoms(symptoms))
        issueTextContainer.setAdapter(symptomsAdapter)

        symptomsList.layoutManager = LinearLayoutManager(this).apply {
            isSmoothScrollbarEnabled = true
            orientation = LinearLayoutManager.HORIZONTAL
        }

        val symptomsArrayList = ArrayList<String>()

        specialSymptomsAdapter = SymptomAdapter(this, symptomsArrayList)
        symptomsList.adapter = specialSymptomsAdapter

//        symptomsList.adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, )
    }

    private fun initListeners() {
        btnAdd.setOnClickListener {
            if (issueTextContainer.text.isNullOrEmpty()) {
                Toast.makeText(this@SpecialistCenter, "Please select a symptom", Toast.LENGTH_LONG).show()
            } else {
                val text = issueTextContainer.text
                val issueId = getJsonItemIdByName(text.toString())

                try {
                    symptomsArray.put(issueId)
                    showSymptoms(issueTextContainer.text.toString())
                    issueTextContainer.text.clear()
                } catch (ex: Exception) {
                    Log.e(TAG, ex.message)
                }
            }
        }

        btnSubmit.setOnClickListener {
            if(symptomsArray.length() == 0) {
                Toast.makeText(this, "Please select at least one symptom before submitting", Toast.LENGTH_LONG).show()
            }else {
                controlsContainer.visibility = View.GONE
                processSymptomsAndReturnResults()
                hideSoftKeyboard()
                loadingContainer.visibility = View.VISIBLE
            }
        }
    }

    private fun getJsonItemIdByName(name: String): Int {
        var id = 0

        for (i in (0 until symptomsJsonArray.length())) {
            val jb = symptomsJsonArray.getJSONObject(i)

            if (jb.getString("Name").equals(name, true)) {
                id = jb.getInt("ID")
            }
        }

        return id
    }

    private fun hideSoftKeyboard() {
        if (currentFocus != null) {
            val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(currentFocus.windowToken, 0)
        }
    }

    private fun showSymptoms(symptom: String) {
        runOnUiThread {
            specialSymptomsAdapter.add(symptom)
            specialSymptomsAdapter.notifyDataSetChanged()
        }
    }

    private fun getUserDetailsFromFirebase() {
        user = FBaseUser()

        mFirebaseDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("basic_information").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.e(TAG, p0.message)
            }

            override fun onDataChange(p0: DataSnapshot) {
                user = p0.getValue(FBaseUser::class.java)!!

                gender = user.gender ?: "male"

                dob = if (user.dateOfBirth != null) {
                    getDOB(user.dateOfBirth!!)
                } else {
                    1996
                }
            }

        })
    }

    private fun getDOB(date: Date): Int {
        val userDOB = Calendar.getInstance()
        userDOB.time = date

        return userDOB.get(Calendar.YEAR)
    }

    private fun getSymptoms(symptoms: ArrayList<NewSymptoms>): ArrayList<String> {
        val symptomsList = ArrayList<String>()

        for (i in (0 until symptoms.size)) {
            symptomsList.add(symptoms[i].Name!!)
        }

        return symptomsList
    }

    private fun processSymptomsAndReturnResults() {
        this.runOnUiThread {

            try {

                val url = StringBuilder(API_MEDIC_LIVE_HEALTH_SERVICE)
                        .append("diagnosis?")
                        .append("symptoms=")
                        .append(symptomsArray.toString())
                        .append("&token=$staticToken&")
                        .append("gender=$gender&")
                        .append("year_of_birth=$dob")
                        .append("&language=en-gb")
                        .toString()

                AndroidNetworking
                        .get(url)
                        .setTag(TAG)
                        .setPriority(Priority.IMMEDIATE)
                        .build()
                        .getAsJSONArray(object : JSONArrayRequestListener {
                            override fun onResponse(response: JSONArray?) {
                                Log.i(TAG, "response = $response")
                                var diag = Diag()
                                if (response!!.length() > 0) {
                                    diag = getDiag(response.getJSONObject(0))
                                    Log.d(TAG, "received diag = $diag")

                                    specialistAdapter = SpecialistAdapter(this@SpecialistCenter, diag.specialization!!)

                                    issueText.text = diag.issue!!.Name.toString()

                                    issueText.setOnClickListener {
                                        val intent = Intent(this@SpecialistCenter, IssueCenter::class.java)
                                        intent.putExtra("ISSUE_TEXT", issueText.text.toString())
                                        intent.putExtra("token", token)
                                        startActivity(intent)
                                    }

                                    specialistLayoutManager = LinearLayoutManager(this@SpecialistCenter)
                                    specialistLayoutManager.apply {
                                        orientation = LinearLayoutManager.VERTICAL
                                        isSmoothScrollbarEnabled = true
                                    }

                                    specialistContainer.adapter = specialistAdapter
                                    specialistContainer.layoutManager = specialistLayoutManager

                                    loadingContainer.visibility = View.GONE

                                    resultsContainer.visibility = View.VISIBLE

                                    if(!prefsManager.getHasSeenClickOnIssueText()) {
                                        showGuideView()
                                        prefsManager.setHasSeenClickOnIssueText()
                                    }

                                    resultsContainer.animate().alpha(1f).duration = 5000
                                } else {
                                    showErrorDialog("I'm sorry, we do not have any available information at this time")
                                    loadingContainer.visibility = View.GONE
                                }
                            }

                            override fun onError(anError: ANError?) {
                                Log.e(TAG, "error: ${anError!!.message}")
                                Log.e(TAG, "error: ${anError.errorDetail}")
                                showErrorDialog(anError.message)
                                Log.e(TAG, "error: ${anError.response}")
                            }

                        })

            } catch (ex: Exception) {
                Log.e(TAG, ex.message)
            }

        }
    }

    private fun showErrorDialog(message: String?) {
        LovelyInfoDialog(this)
                .setTitle("Error")
                .setMessage(message)
                .setConfirmButtonText("OK")
                .setTopColor(resources.getColor(android.R.color.holo_red_dark))
                .show()
    }

    inner class LoadSymptoms : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg p0: String?): String? {

            try {
                val jsonObject = JSONObject(loadJsonFromAsset("symptomsone.json", this@SpecialistCenter))

                val jsonArray = jsonObject.getJSONArray("symptoms")

                symptomsJsonArray = jsonArray

                for (i in (0 until jsonArray.length())) {
                    val jb = jsonArray.getJSONObject(i)

                    val symptom = NewSymptoms(
                            Name = jb.getString("Name"),
                            ID = jb.getInt("ID")
                    )

                    symptoms.add(symptom)
                }

                Log.d(TAG, "issueArray: $symptoms")

            } catch (ex: Exception) {
                Log.e(TAG, ex.message)
            }

            return null
        }

        override fun onPostExecute(result: String?) {
            initListeners()
            initAdapters()
            hasLoadedSymptoms = true
        }

    }


}
