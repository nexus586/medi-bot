package com.nexus.apps.medibot.activities

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.adapters.BotAdapter
import com.nexus.apps.medibot.models.BotMessage
import com.nexus.apps.medibot.models.Condition
import com.nexus.apps.medibot.models.DiagnosisObj
import com.nexus.apps.medibot.models.Symptoms
import com.nexus.apps.medibot.utils.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class AnonymousDiagnosis : AppCompatActivity() {

    private val TAG = AnonymousDiagnosis::class.java.simpleName

    private lateinit var recyclerView: RecyclerView
    private lateinit var sendButton: Button
    private lateinit var symptomText: EditText
    private lateinit var autoCompleteTextView: AutoCompleteTextView
    private lateinit var adapter: BotAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var toolbar: Toolbar
    private lateinit var bottomLayout: LinearLayout
    private lateinit var conditions: ArrayList<Condition>
    private lateinit var autoCompleteAdapter: ArrayAdapter<String>
    private lateinit var coordinatorLayout: CoordinatorLayout

    var resultConditions = ArrayList<Condition>()

    private var symptoms: ArrayList<Symptoms>? = ArrayList()
    private var symptomsArray = JSONArray()
    private var symptomsJsonArray = JSONArray()
    private var questionCount = 0
    private var count = 0
    private var symptomCount = 0
    private var diagnosisJSONObject = JSONObject()
    private var hasLoadedSymptoms = false
    private var isAge = false
    private var isSex = false
    private var isInfoComplete = false

    private var counter = 0
    private lateinit var responses: Array<String>

    private lateinit var prefsManager: PrefsManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anonymous_diagnosis)

        initViews()
        if (hasLoadedSymptoms) {
            initAdapters()
            initListeners()
        } else {
            autoCompleteTextView.isEnabled = false
            symptomText.isEnabled = false
            LoadSymptoms().execute()
        }
    }

    private fun initViews() {
        prefsManager = PrefsManager(this)
        title = "Anonymous Diagnosis"
        recyclerView = findViewById(R.id.message_container)
        sendButton = findViewById(R.id.btn_send)
        symptomText = findViewById(R.id.symptom_text)
        toolbar = findViewById(R.id.toolbar)
        bottomLayout = findViewById(R.id.chat_container)
        autoCompleteTextView = findViewById(R.id.autoCompleteTextView)
        autoCompleteTextView.threshold = 1
        responses = getResponses()
        coordinatorLayout = findViewById(R.id.coordinator)

        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        actionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_back)
            setHomeButtonEnabled(true)
        }
    }

    private fun initListeners() {
        sendButton.setOnClickListener {
            if (isAge) {
                try {
                    val age = Integer.parseInt(symptomText.text.toString())

                    if(age < 0 || age > 120) {
                        Toast.makeText(this@AnonymousDiagnosis, "Please enter a valid age between 1 and 120", Toast.LENGTH_LONG).show()
                    }else {
                        sendMyMessage(age.toString())

                        symptomText.setText("")

                        val botMessage = BotMessage(
                                type = TYPE_SINGLE,
                                message = resources.getString(R.string.sex),
                                option1 = "Male",
                                option2 = "Female")

                        sendButton.isEnabled = false
                        hideSoftKeyboard()
                        bottomLayout.visibility = View.GONE
                        isSex = true

                        showMessage(botMessage)
                    }

                } catch (e: Exception) {
                    Toast.makeText(this@AnonymousDiagnosis, "Please enter a number", Toast.LENGTH_LONG).show()
                }
            } else {
                sendMyMessage(symptomText.text.toString())
            }
        }

        autoCompleteTextView.setOnItemClickListener { _, _, _, _ ->
            if (autoCompleteTextView.text.toString().equals("done", true)) {
                symptomText.visibility = View.VISIBLE
                autoCompleteTextView.visibility = View.GONE

            } else {
                val symptom = autoCompleteTextView.text.toString()
                try {
                    val jsonArray = JSONArray()

                    if (symptomsArray.length() == 0) {
                        val jb = JSONObject()
                        try {
                            jb.put("id", findJsonItemIdByName(symptom))
                            jb.put("choice_id", "present")
                            jb.put("initial", true)
                        } catch (e: Exception) {
                            Log.e(TAG, e.message)
                        }
                        jsonArray.put(jb)
                    } else {
                        val len = symptomsArray.length() + 1

                        for (i in (0 until len)) {
                            if (i == (len - 1)) {
                                val jb = JSONObject()
                                jb.put("id", findJsonItemIdByName(symptom))
                                jb.put("choice_id", "present")
                                jb.put("initial", true)
                                jsonArray.put(jb)
                            } else {
                                jsonArray.put(symptomsArray.getJSONObject(i))
                            }
                        }
                    }
                    symptomsArray = jsonArray
                } catch (e: Exception) {
                    Log.e(TAG, e.message)
                }

                sendMyMessage(symptom)
                hideSoftKeyboard()
                bottomLayout.visibility = View.GONE
                sendResponse()
                autoCompleteTextView.setText("")
            }
        }
    }

    private fun findJsonItemIdByName(name: String): String {
        var id = ""

        for (i in (0 until symptomsJsonArray.length())) {
            val jb = symptomsJsonArray.getJSONObject(i)
            if (jb.getString("name").equals(name, ignoreCase = true)) {
                id = jb.getString("id")
            }
        }

        return id
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = MenuInflater(this)
        inflater.inflate(R.menu.diagnosis_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {

            R.id.home -> {
                super.onOptionsItemSelected(item)
                true
            }

            R.id.refresh -> {
                finish()
                startActivity(intent)
                true
            }

            else -> false
        }
    }

    private fun getResponses(): Array<String> {
        return resources.getStringArray(R.array.options)
    }

    private fun sendResponse() {
        if (counter == responses.size) {
            counter = 0
        }
        val botMessage = BotMessage(message = responses[counter], option1 = "Yes", option1Id = "affirm",option2 = "No", option2Id = "negate",type = TYPE_SINGLE)
        showMessage(botMessage)
        counter++
    }

    fun continueWithSymptoms() {
        bottomLayout.visibility = View.VISIBLE
    }

    fun beginDiagnosis() {
        autoCompleteTextView.visibility = View.GONE
        val botMessage = BotMessage(
                senderId = 2,
                message = resources.getString(R.string.age),
                type = TYPE_TEXT
        )

        isAge = true
        sendButton.isEnabled = true
        bottomLayout.visibility = View.VISIBLE
        symptomText.visibility = View.VISIBLE

        showMessage(botMessage)
    }

    private fun initAdapters() {
        val messages = java.util.ArrayList<BotMessage>()

        Log.d(TAG, "${prefsManager.getHasSeenAnonymousDiagnosisTutorial()}")

        if(prefsManager.getHasSeenAnonymousDiagnosisTutorial()) {
            val botMessage1 = BotMessage(senderId = 2, type = TYPE_TEXT, message = resources.getString(R.string.anony_other_intro))
            messages.add(botMessage1)
        }else{
            val botMessage1 = BotMessage(senderId = 2, type = TYPE_TEXT, message = resources.getString(R.string.anony_intro))
            messages.add(botMessage1)

            val botMessage2 = BotMessage(senderId = 2, type = TYPE_TEXT, message = resources.getString(R.string.prototype))
            messages.add(botMessage2)
            val botMessage4 = BotMessage(senderId = 2, type = TYPE_TEXT, message = resources.getString(R.string.info))
            messages.add(botMessage4)
            prefsManager.setHasSeenAnonymousDiagnosisTutorial()
        }

        adapter = BotAdapter(this, messages)
        layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        layoutManager.isSmoothScrollbarEnabled = true

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    private fun hideSoftKeyboard() {
        if (currentFocus != null) {
            val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(currentFocus.windowToken, 0)
        }
    }

    private fun showMessage(botMessage: BotMessage) {
        adapter.add(botMessage)
        runOnUiThread {
            adapter.notifyDataSetChanged()
            scrollDown()
        }

        if (isInfoComplete){
            isInfoComplete = false
            CheckDiagnosis().execute()
        }
    }

    private fun scrollDown() = layoutManager.scrollToPosition(adapter.itemCount - 1)

    fun sendMyMessage(message: String) {
        val botMessage = BotMessage(message = message, senderId = 1, type = TYPE_TEXT)

        if (isAge) {
            try {
                diagnosisJSONObject.put("age", Integer.parseInt(message))
                isAge = false
            } catch (ex: JSONException) {
                Log.e(TAG, ex.message)
            }
        }

        if (isSex) {
            try {
                diagnosisJSONObject.put("sex", message.toLowerCase())
                isSex = false
                isInfoComplete = true
            } catch (ex: JSONException) {
                Log.e(TAG, ex.message)
            }
        }

        showMessage(botMessage)
    }

    fun sendMyReplyMessage(message: String, symptom_id: String, choice_id: String) {
        val botMessage = BotMessage(senderId = 1, message = message, type = TYPE_TEXT)

        showMessage(botMessage)

        try {
            val array = JSONArray()

            val len = symptomsArray.length() + 1
            for (i in (0 until len)) {
                if (i == (len - 1)) {
                    val jb = JSONObject()
                    jb.put("id", symptom_id)
                    jb.put("choice_id", choice_id)
                    array.put(jb)
                } else {
                    array.put(symptomsArray.getJSONObject(i))
                }
            }

            symptomsArray = array
        } catch (e: Exception) {
            Log.e(TAG, e.message)
        }

        if (questionCount > 13) {
            val newBotMessage = BotMessage(senderId = 2, type = TYPE_TEXT, message = resources.getString(R.string.conclusion))
            showMessage(newBotMessage)
            GetCondition(conditions[0].id, conditions[0].probability).execute()
        } else {
            CheckDiagnosis().execute()
        }
    }

    fun sendMyReplyMessageWithoutQuery(message: String, symptom_id: String, choice_id: String) {
        val botMessage = BotMessage(senderId = 1, message = message, type = TYPE_TEXT)

        showMessage(botMessage)

        try {

            val jArray = JSONArray()
            val len = symptomsArray.length() + 1
            for (i in (0 until len)) {
                if (i == (len - 1)) {
                    val obj = JSONObject()
                    obj.put("id", symptom_id)
                    obj.put("choice_id", choice_id)
                    jArray.put(obj)
                } else {
                    jArray.put(symptomsArray.getJSONObject(i))
                }
            }

            symptomsArray = jArray

            handleMultipleQuestions(symptoms!![symptomCount])
        } catch (e: Exception) {
            Log.e(TAG, e.message)
        }
    }

    fun handleMultipleQuestions(symptom: Symptoms) {
        val botMessage = BotMessage(senderId = 2)

        if (symptomCount == (symptoms!!.size - 1)) {
            botMessage.type = TYPE_SINGLE
            symptomCount = 0
        } else {
            botMessage.type = TYPE_GROUP_MULTIPLE
        }
        botMessage.message = symptom.name
        botMessage.symptomId = symptom.id
        botMessage.option1 = "Yes"
        botMessage.option2 = "No"
        botMessage.option3 = "Don't know"
        symptomCount++

        showMessage(botMessage)
    }

    private fun isDiseaseFound(): Boolean {
        for (condition in conditions) {
            if (condition.probability > 75)
                return true
        }
        return false
    }

    fun getConditions(conditions: ArrayList<Condition>): ArrayList<String> {
        val arrayList = ArrayList<String>()

        for (i in (0 until conditions.size)) {
            arrayList.add(conditions[i].name)
        }

        return arrayList
    }

    inner class GetCondition(private var conditionId: String, private var probability: Int) : AsyncTask<String, String, String>() {
        private var success = 0
        private var condition = Condition()

        override fun doInBackground(vararg p0: String?): String? {

            val httpEndpoint: URL?
            try {
                httpEndpoint = URL("https://api.infermedica.com/v2/conditions/$conditionId")
                val secureConnection = httpEndpoint.openConnection() as HttpsURLConnection
                secureConnection.setRequestProperty("App-Id", SECONDARY_INFERMEDICA_APP_ID)
                secureConnection.setRequestProperty("App-Key", SECONDARY_INFERMEDICA_APP_KEY)
                secureConnection.setRequestProperty("Content-Type", "application/json")

                secureConnection.requestMethod = "GET"

                if (secureConnection.responseCode == 200) {
                    success = 1

                    val responseBody = secureConnection.inputStream

                    val bR = BufferedReader(InputStreamReader(responseBody))
                    var line: String?

                    val responseBuilder = StringBuilder()
                    do {
                        line = bR.readLine() ?: null
                        if (line == null)
                            break
                        else
                            responseBuilder.append(line)
                    } while (true)
                    responseBody.close()

                    val result = JSONObject(responseBuilder.toString())
                    condition = getConditionFromResults(result)
                    condition.probability = probability

                } else {
                    success = 0
                    Log.e("nexus", "failure")
                }

                secureConnection.disconnect()

            } catch (e: Exception) {
                Log.e(TAG, e.message)
                Snackbar.make(coordinatorLayout, "Sorry, an unexpected error occurred. Please try again later.", Snackbar.LENGTH_LONG).show()
            }

            return null
        }

        override fun onPostExecute(result: String?) {

            if (success == 1) {
                resultConditions.add(condition)

                if (count <= 4) {

                    val botMessage = BotMessage(type = TYPE_DIAGNOSIS_RESULTS,
                            conditions = resultConditions)

                    showMessage(botMessage)

                    val diagnosis = BotMessage(type = TYPE_TEXT, message = resultConditions[0].diagnosis)
                    showMessage(diagnosis)

                    val botMessage2 = BotMessage(type = TYPE_TEXT, message = resources.getString(R.string.disclosure))
                    showMessage(botMessage2)
                } else {
                    GetCondition(conditions[count].id, conditions[count].probability).execute()
                    count++
                }
            }
        }
    }

    inner class CheckDiagnosis : AsyncTask<String, String, String>() {

        private var diagnosisObj: DiagnosisObj = DiagnosisObj()
        private var success: Int = 0

        override fun onPreExecute() {
            toolbar.subtitle = "Processing information..."
        }

        override fun doInBackground(vararg p0: String?): String? {
            val url: URL?

            try {
                url = URL("https://api.infermedica.com/v2/diagnosis")
                val secureConnection = url.openConnection() as HttpsURLConnection

                secureConnection.setRequestProperty("App-Id", SECONDARY_INFERMEDICA_APP_ID)
                secureConnection.setRequestProperty("App-Key", SECONDARY_INFERMEDICA_APP_KEY)
                secureConnection.setRequestProperty("Content-Type", "application/json")

                secureConnection.requestMethod = "POST"

                diagnosisJSONObject.put("evidence", symptomsArray)

                Log.i(TAG, "diagnosisObject being sent: $diagnosisJSONObject")

                val myData = diagnosisJSONObject.toString()

                secureConnection.doOutput = true

                secureConnection.outputStream.write(myData.toByteArray())

                if (secureConnection.responseCode == 200) {
                    success = 1

                    val responseBody = secureConnection.inputStream

                    val bR = BufferedReader(InputStreamReader(responseBody))
                    var line: String?

                    val responseBuilder = StringBuilder()
                    do {
                        line = bR.readLine() ?: null
                        if (line == null)
                            break
                        else
                            responseBuilder.append(line)

                    } while (true)

                    responseBody.close()

                    val result = JSONObject(responseBuilder.toString())
                    diagnosisObj = getDiagnosis(result)
                    conditions.clear()
                    conditions = diagnosisObj.conditions!!

                } else {
                    val responseBody = secureConnection.inputStream

                    success = 0
                    Log.e("nexus", "failure: an unexpected error occurred: $responseBody")
                }

                secureConnection.disconnect()

            } catch (e: java.lang.Exception) {
                Snackbar.make(coordinatorLayout, "Sorry, an unexpected error occurred. Please try again later.", Snackbar.LENGTH_LONG).show()
                Log.e(TAG, e.message)
            }

            return null
        }

        override fun onPostExecute(result: String?) {
            toolbar.subtitle = ""
            if (success == 1) {
                questionCount++

                if (isDiseaseFound()) {
                    val botMessage = BotMessage(
                            senderId = 2,
                            type = TYPE_TEXT,
                            message = resources.getString(R.string.conclusion))

                    showMessage(botMessage)

                    GetCondition(conditions[0].id, conditions[0].probability).execute()
                } else {
                    if (diagnosisObj.type.equals(TYPE_GROUP_MULTIPLE, true)) {

                        val botMessage = BotMessage(
                                senderId = 2,
                                type = TYPE_TEXT,
                                message = diagnosisObj.extraQuestion
                        )

                        showMessage(botMessage)
                        symptoms = convertDiagnosisToBotMessage(diagnosisObj).symptoms
                        handleMultipleQuestions(symptom = symptoms!![0])
                    } else {
                        showMessage(convertDiagnosisToBotMessage(diagnosisObj))
                    }
                }
            }else {
                Snackbar.make(coordinatorLayout, "An error occurred. Please check your connection and try again later", Snackbar.LENGTH_SHORT).show()
            }
        }
    }


    inner class LoadSymptoms : AsyncTask<String, String, String>() {

        override fun doInBackground(vararg p0: String?): String? {
            try {
                val jsonObject = JSONObject(loadJsonFromAsset("symptoms.json", this@AnonymousDiagnosis))

                val jsonArray = jsonObject.getJSONArray("symptoms")

                symptomsJsonArray = jsonArray

                conditions = ArrayList()

                for (i in (0 until jsonArray.length())) {
                    val jb = jsonArray.getJSONObject(i)

                    val condition = Condition(name = jb.getString("name"), id = jb.getString("id"))

                    conditions.add(condition)
                }

            } catch (e: Exception) {
                Log.e("nexus", e.message)
            }

            return null
        }

        override fun onPostExecute(result: String?) {
            autoCompleteTextView.isEnabled = true
            symptomText.isEnabled = true
            autoCompleteAdapter = ArrayAdapter(
                    this@AnonymousDiagnosis,
                    android.R.layout.simple_list_item_1,
                    getConditions(conditions)
            )
            autoCompleteTextView.setAdapter(autoCompleteAdapter)
            initAdapters()
            initListeners()
            hasLoadedSymptoms = true
        }
    }
}
