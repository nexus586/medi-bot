package com.nexus.apps.medibot.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.dialogs.LoginDialog
import com.nexus.apps.medibot.utils.FIREBASE_REFERENCE
import com.nexus.apps.medibot.utils.PrefsManager
import com.yarolegovich.lovelydialog.LovelyInfoDialog
import com.yarolegovich.lovelydialog.LovelyProgressDialog

class Login : AppCompatActivity() {

    private lateinit var tvEmail: TextInputEditText
    private lateinit var tvPassword: TextInputEditText
    private lateinit var gSignButton: SignInButton
    private lateinit var btnSignIn: Button
    private lateinit var btnSignUp: Button
    private lateinit var mAuth: FirebaseAuth
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var prefsManager: PrefsManager
    private lateinit var dialog: LoginDialog
    private val TAG = this.javaClass.name
    private val RC_SIGN_IN = 586
    private lateinit var mDataBase: FirebaseDatabase
    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var mLoadingIndicator: LovelyProgressDialog

    private lateinit var emailTIL: TextInputLayout
    private lateinit var passwordTIL: TextInputLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        prefsManager = PrefsManager(this)

        prefsManager.setHasSeenOnboarding()
        prefsManager.setHasSeenSecurityIntro(false)

        initViews()
        initListeners()
        initGoogle()
    }


    private fun initViews() {
        mLoadingIndicator = LovelyProgressDialog(this)
        tvEmail = findViewById(R.id.tt_email)
        tvPassword = findViewById(R.id.tt_password)
        btnSignIn = findViewById(R.id.btn_sign_in)
        btnSignUp = findViewById(R.id.btn_sign_up)
        emailTIL = findViewById(R.id.emailTIL)
        passwordTIL = findViewById(R.id.passwordTIL)
        dialog = LoginDialog(this, resources.getString(R.string.login))
    }

    private fun initListeners() {
        btnSignUp.setOnClickListener {
            val intent = Intent(this, SignUp::class.java)
            startActivity(intent)
        }

        btnSignIn.setOnClickListener {
            clearError()

            if(tvEmail.text.isNullOrEmpty() && tvPassword.text.isNullOrEmpty()){
                emailTIL.error = "Email should not be empty"
                passwordTIL.error = "Password should not be empty"
            }else if (tvEmail.text.isNullOrEmpty() || tvPassword.text.isNullOrEmpty()) {
                if(tvEmail.text.isNullOrEmpty()) {
                    emailTIL.error = "Email should not be empty"
                    passwordTIL.error = null
                }else {
                    passwordTIL.error = "Password should not be empty"
                    emailTIL.error = null
                }
            } else if (!Patterns.EMAIL_ADDRESS.matcher(tvEmail.text.toString()).matches()) {
                emailTIL.error = "Please enter a valid email"
                passwordTIL.error = null
            } else {
                emailTIL.error = null
                passwordTIL.error = null
                showLoadingDialog()
                mAuth.signInWithEmailAndPassword(tvEmail.text.toString(), tvPassword.text.toString()).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "signInWithEmail:success")
                        val user = mAuth.currentUser
                        Log.i(TAG, "user email ${user!!.email}")
                        prefsManager.setUserDisplayName(user.displayName.toString())
                        prefsManager.setUserEmail(user.email.toString())
                        prefsManager.setIsLoggedIn()
                        hideDialog()
                        if(prefsManager.getHasSeenSecurityIntro()) {
                            goToHome()
                        }else {
                            goToSecurityIntro()
                        }
                    } else {
                        Log.w(TAG, "signInWithEmail:failure", task.exception)
                        hideDialog()
                        showErrorDialog(task.exception!!.message)
                    }
                }
            }
        }
    }

    private fun signIn() {
        val intent = mGoogleSignInClient.signInIntent
        startActivityForResult(intent, RC_SIGN_IN)
        showLoadingDialog()
    }


    private fun initGoogle() {
        gSignButton = findViewById(R.id.google_button)

        gSignButton.setOnClickListener {
            signIn()
        }

        val gso = GoogleSignInOptions.Builder()
                .requestIdToken(resources.getString(R.string.default_web_client_id))
                .requestEmail()
                .requestProfile()
                .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        mAuth = FirebaseAuth.getInstance()
        mDataBase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDataBase.getReference(FIREBASE_REFERENCE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                if (account != null) {
                    firebaseAuthWithGoogle(account)
                }
            } catch (e: Exception) {
                mLoadingIndicator.dismiss()
                showErrorDialog("Google sign in failed. Please check your connection and try again later")
                Log.w(TAG, "Google sign in failed", e)
            }
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.d(TAG, "firebaseAuthWithGoogle: " + acct.id!!)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(this@Login, "Sign in successful", Toast.LENGTH_LONG).show()
                        hideDialog()
                        Log.d(TAG, "signInWithCredential:success")
                        val user = mAuth.currentUser
                        Log.i(TAG, "user email ${user!!.email}")
                        prefsManager.setIsLoggedIn()
                        prefsManager.setUserDisplayName(user.displayName.toString())
                        prefsManager.setUserId(user.uid)
                        if(prefsManager.getHasSeenSecurityIntro()) {
                            goToHome()
                        }else {
                            goToSecurityIntro()
                        }
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithCredential:failure", task.exception)
                        showErrorDialog(task.exception!!.message)
                    }
                }
    }

    private fun showWarning() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Warning")
                .setMessage("Required fields are empty")
                .setCancelable(true)
                .show()
    }

    private fun showEmailError() {
        val builder = LovelyInfoDialog(this)
        builder.setTopColor(resources.getColor(android.R.color.holo_orange_light))
                .setTitle("Warning")
                .setMessage("Enter a valid Email")
                .setCancelable(true)
                .show()
    }

    private fun showSuccessDialog() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setMessage(R.string.success_sign_in).setCancelable(true).show()
    }

    private fun hideDialog() {
        mLoadingIndicator.dismiss()
    }

    private fun goToHome() {
        val intent = Intent(this, Home::class.java)
        startActivity(intent)
        finish()
    }

    private fun showLoadingDialog() {
        mLoadingIndicator.setTitle("Logging In")
                .setMessage("Please wait while we log you into your account")
                .setIcon(R.drawable.ic_info_outline)
                .setTopColor(Color.CYAN)
                .setCancelable(false)
                .show()
    }

    private fun showErrorDialog(message: String?) {
        val dialog = LovelyInfoDialog(this)
        dialog.setTitle("Error")
                .setMessage(message)
                .setTopColor(Color.RED)
                .setIcon(resources.getDrawable(R.drawable.ic_info_outline))
                .setConfirmButtonText("OK")
                .show()
    }

    private fun goToSecurityIntro() {
        startActivity(Intent(this, SettingsIntro::class.java))
        finish()
    }


    private fun clearError() {
        emailTIL.error = null
        passwordTIL.error = null
    }
}
