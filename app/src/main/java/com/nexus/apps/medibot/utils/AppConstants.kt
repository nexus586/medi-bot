package com.nexus.apps.medibot.utils

import com.nexus.apps.medibot.models.Article


/********************************
 * API MEDIC CONSTANTS
 **********************************/
// live account constants
const val API_MEDIC_LIVE_USERNAME = "s4X7E_GMAIL_COM_AUT"
const val API_MEDIC_LIVE_PASS = "o7S8DiAy3g9LYj25R"
const val API_MEDIC_LIVE_AUTH_SERVICE = "https://authservice.priaid.ch/login"
const val API_MEDIC_LIVE_HEALTH_SERVICE = "https://healthservice.priaid.ch/"

// sandbox constants
const val API_MEDIC_SANDBOX_USERNAME = "lazerx506@gmail.com"
const val API_MEDIC_SANDBOX_PASS = "o5C8WaJx49PgAc7z2"
const val API_MEDIC_SANDBOX_AUTH_SERVICE = "https://sandbox-authservice.priaid.ch/login"
const val API_MEDIC_SANDBOX_HEALTH_SERVICE = "https://sandbox-healthservice.priaid.ch"

// infermedica constants
const val INFERMEDICA_APP_KEY = "5a332e9eec7cfa7bd20917d622ddf739"
const val INFERMEDICA_APP_ID = "32ac4ea6"
const val SECONDARY_INFERMEDICA_APP_KEY = "3a65c7d409361e8eb47ba56e9fb437ad"
const val SECONDARY_INFERMEDICA_APP_ID = "ebfeacfd"


const val FIREBASE_REFERENCE = "medi-bot-9c905"

// chat bot details
const val TYPE_TEXT = "text"
const val TYPE_SINGLE = "single"
const val TYPE_GROUP_SINGLE = "group_single"
const val TYPE_GROUP_MULTIPLE = "group_multiple"
const val TYPE_DIAGNOSIS_RESULTS = "diagnosis_results"
const val EMPTY = ""

// firebase nodes
const val BASIC_INFORMATION = "basic_information"
const val ALLERGIES = "allergies"
const val MEDICATION = "medication"
const val HEALTH_INFO = "health_information"

// newsapi api_key
const val NEWS_API_API_KEY = "ddf9e5f55f3e47ab81e7e88f9912e969"
const val NEWS_API_URL = "https://newsapi.org/v2/top-headlines"

const val APP_ID = "App-Id"
const val APP_KEY = "App-Key"
const val CONTENT_TYPE = "Content-Type"
const val APPLICATION_JSON_VALUE = "application/json"


// SharedPreferences Constants
const val HAS_SEEN_ONBOARDING = "has_seen_onboarding"
const val IS_LOGGED_IN = "is_logged_in"
const val USER_DISPLAY_NAME = "user_display_name"
const val USER_ID = "user_id"
const val USER_EMAIL = "user_email"
const val HAS_SEEN_PERSONAL_DIAGNOSIS_TUTORIAL = "has_seen_personal_diagnosis_tutorial"
const val HAS_SEEN_ANONYMOUS_DIAGNOSIS_TUTORIAL = "has_seen_anonymous_diagnosis_tutorial"
const val HAS_SEEN_ISSUE_CENTER_INTRO = "has_seen_issue_center_intro"

var staticToken = ""

var staticNews: ArrayList<Article> = ArrayList()

var hasVerifiedPin = false

var medicationId = ""