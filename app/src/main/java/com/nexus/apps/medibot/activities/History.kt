package com.nexus.apps.medibot.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.agrawalsuneet.loaderspack.loaders.PulseLoader
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.adapters.ChatAdapter
import com.nexus.apps.medibot.models.DiagnosisChat
import com.nexus.apps.medibot.utils.FIREBASE_REFERENCE

class History : AppCompatActivity() {

    private var TAG = this::class.java.simpleName

    private lateinit var messagesView: RecyclerView
    private lateinit var goButton: FloatingActionButton
    private lateinit var loader: PulseLoader
    private lateinit var isLoadingText: TextView

    private var isLoading = true

    private lateinit var mFirebaseAuth: FirebaseAuth
    private lateinit var mDatabase: FirebaseDatabase
    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var adapter: ChatAdapter
    private lateinit var notFoundLayout: RelativeLayout

    private var chatList: ArrayList<DiagnosisChat> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        initViews()
        initFirebase()
        if (isLoading) {
            hideViews()
        }
    }


    private fun initFirebase() {
        mFirebaseAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase.getReference(FIREBASE_REFERENCE)
        getChatsFromFirebase()
    }

    private fun initViews() {
        messagesView = findViewById(R.id.messages)
        goButton = findViewById(R.id.go_to_chat)
        loader = findViewById(R.id.pulse_loader)
        isLoadingText = findViewById(R.id.isLoadingText)
        notFoundLayout = findViewById(R.id.no_chat_container)

        val actionBar = supportActionBar
        actionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_outline_profile)
            setHomeButtonEnabled(true)
            title = "Diagnosis History"
        }
    }

    private fun hideViews() {
        messagesView.visibility = View.GONE
        goButton.visibility = View.GONE
        notFoundLayout.visibility = View.GONE

        loader.visibility = View.VISIBLE
        isLoadingText.visibility = View.VISIBLE
    }

    private fun showViews() {
        goButton.visibility = View.VISIBLE

        if (chatList.size == 0) {
            notFoundLayout.visibility = View.VISIBLE
        } else {
            messagesView.visibility = View.VISIBLE
        }

        loader.visibility = View.GONE
        isLoadingText.visibility = View.GONE
    }

    private fun initAdapters() {
        val layoutManager = LinearLayoutManager(this)
        messagesView.layoutManager = layoutManager
        adapter = ChatAdapter(this@History, { diagnosisChat: DiagnosisChat -> diagnosisChatClicked(diagnosisChat) }, chatList)
        messagesView.adapter = adapter
    }

    private fun initListeners() {
        goButton.setOnClickListener {
            val intent = Intent(this, PersonalDiagnosis::class.java)
            startActivity(intent)
        }
    }

    private fun diagnosisChatClicked(diagnosisChat: DiagnosisChat) {
        Log.i(TAG, "Diagnosis: " + diagnosisChat.id + " clicked. Opening PersonalDiagnosis Activity")
        val diagnosisChatId = diagnosisChat.id
        val showDetailActivityIntent = Intent(this, PersonalDiagnosis::class.java)
        showDetailActivityIntent.putExtra(Intent.EXTRA_TEXT, diagnosisChatId)
        startActivity(showDetailActivityIntent)
    }

    private fun getChatsFromFirebase() {
        Log.i(TAG, "Calling firebase to retrieve diagnosis chats")
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("diagnoses").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", p0.toException())
            }

            override fun onDataChange(p0: DataSnapshot) {
                for (postSnapshot in p0.children) {
                    val temp = DiagnosisChat(
                            id = postSnapshot.child("id").value.toString(),
                            recommendation = postSnapshot.child("recommendation").value.toString(),
                            date = postSnapshot.child("date").value.toString()
                    )
                    chatList.add(temp)
                }
                isLoading = false
                Log.i(TAG, "Chats loaded successfully. Initializing views")
                initAdapters()
                initListeners()
                showViews()
            }

        })
    }
}
