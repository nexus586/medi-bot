package com.nexus.apps.medibot.adapters

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.nexus.apps.medibot.R
import kotlinx.android.synthetic.main.activity_faq.view.*
import kotlinx.android.synthetic.main.faq_item.view.*




class FaqAdapter(private val context: Context,private val headers: ArrayList<String>, private val dataChild: HashMap<String, ArrayList<String>>) : BaseExpandableListAdapter() {
    override fun getGroup(p0: Int): Any {
        return this.headers[p0]
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean {
        return false
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(p0: Int, p1: Boolean, p2: View?, p3: ViewGroup?): View {
        var convertView = p2
        val listTitle = getGroup(p0) as String
        if (convertView == null) {
            val layoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.faq_header, null)
        }
        val listTitleTextView = convertView!!.findViewById<TextView>(R.id.lblListHeader)
        listTitleTextView.setTypeface(null, Typeface.BOLD)
        listTitleTextView.text = listTitle
        return convertView


    }

    override fun getChildrenCount(p0: Int): Int {
        return this.dataChild[this.headers[p0]]!!.size
    }

    override fun getChild(p0: Int, p1: Int): Any {
        return this.dataChild[this.headers[p0]]!![p1]
    }

    override fun getGroupId(p0: Int): Long {
        return  p0.toLong()
    }

    override fun getChildView(listPosition: Int, expandedListPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {

        var convertView = convertView
        val expandedListText = getChild(listPosition, expandedListPosition) as String
        if (convertView == null) {
            val layoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.faq_item, null)
        }
        val expandedListTextView = convertView!!.findViewById<TextView>(R.id.lblListItem)
        expandedListTextView.text = expandedListText
        return convertView
    }



//    override fun getChildView(groupPosition: Int, childPosition: Int,
//                              isLastChild: Boolean, convertView: View?, parent: ViewGroup): View {
//        var convertView = convertView
//
//        val childText = getChild(groupPosition, childPosition) as String
//
//        if (convertView == null) {
//            val infalInflater = this._context
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//            convertView = infalInflater.inflate(R.layout.item_faqs_child, null)
//        }
//
//        convertView!!.txtFaqAnswer.text = childText
//
//        /*val txtListChild = convertView!!
//                .findViewById(R.id.lblListItem) as TextView
//        txtListChild.text = childText
//        */
//        return convertView!!
//    }


    override fun getChildId(p0: Int, p1: Int): Long {
        return p1.toLong()
    }

    override fun getGroupCount(): Int {
        return this.headers.size
    }
}