package com.nexus.apps.medibot.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.customtabs.CustomTabsIntent
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBar
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.beautycoder.pflockscreen.PFFLockScreenConfiguration
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment
import com.beautycoder.pflockscreen.security.PFFingerprintPinCodeHelper
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.Settings
import com.nexus.apps.medibot.fragments.Diagnosis
import com.nexus.apps.medibot.fragments.MedicalCenter
import com.nexus.apps.medibot.fragments.News
import com.nexus.apps.medibot.models.FBaseUser
import com.nexus.apps.medibot.utils.*
import com.yarolegovich.lovelydialog.LovelyStandardDialog

class Home : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val TAG = Home::class.simpleName

    private lateinit var toolBar: Toolbar
    private lateinit var bottomNavigation: BottomNavigationView
    private lateinit var navigationView: NavigationView
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var drawerToggle: ActionBarDrawerToggle
    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var prefs: PrefsManager
    private lateinit var mAuth: FirebaseAuth
    private lateinit var mDatabase: FirebaseDatabase
    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var headerView: View
    private lateinit var email: TextView
    private lateinit var username: TextView
    private lateinit var newsFragment: News
    private lateinit var diagnosis: Diagnosis
    private lateinit var medCenter: MedicalCenter
    private lateinit var profileImage: ImageView
    private lateinit var frameLayout: FrameLayout
    private lateinit var mFBaseUser: FBaseUser
    private var selectedItemId: Int = 0

    private lateinit var mLoginListener: PFLockScreenFragment.OnPFLockScreenLoginListener
    private lateinit var mCodeCreateListener: PFLockScreenFragment.OnPFLockScreenCodeCreateListener


    private var counter = 4

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        initFireBase()

        prefs = PrefsManager(this)

        initViews()
        initListeners()
        initDrawer()
        initFragments()
        setDefaultFragment()
        setNavHeaderDetails()
    }

    private fun initViews() {
        toolBar = findViewById(R.id.toolbar)
        setSupportActionBar(toolBar)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu)
        }
        title = "Home"
        bottomNavigation = findViewById(R.id.bottom_navigation_view)
        val layoutParams = bottomNavigation.layoutParams as CoordinatorLayout.LayoutParams
        layoutParams.behavior = BottomNavigationViewBehavior()
        navigationView = findViewById(R.id.nav_view)
        drawerLayout = findViewById(R.id.drawer_layout)
        coordinatorLayout = findViewById(R.id.coordinator)
        frameLayout = findViewById(R.id.main_content)

        headerView = navigationView.getHeaderView(0)
        email = headerView.findViewById(R.id.tv_email)
        username = headerView.findViewById(R.id.tv_username)
        profileImage = headerView.findViewById(R.id.profile_image)

    }



    override fun onBackPressed() {
        showDialog("Exit", "Are you sure you want to exit?")
    }

    private fun initFragments() {
        diagnosis = Diagnosis()
        medCenter = MedicalCenter()
        newsFragment = News()
    }

    private fun setDefaultFragment() {
        setFragment(diagnosis)
        bottomNavigation.selectedItemId = R.id.diagnosis
    }

    override fun onResume() {
        super.onResume()
        showToolBarAndBottomNav()
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        bottomNavigation.selectedItemId = selectedItemId
    }

    private fun setNavHeaderDetails() {
        email.text = mAuth.currentUser!!.email
        username.text = mAuth.currentUser!!.displayName
        GlideApp.with(this)
                .load(mAuth.currentUser!!.photoUrl)
                .placeholder(R.mipmap.ic_launcher_foreground)
                .fallback(R.mipmap.ic_launcher_foreground)
                .apply(RequestOptions.bitmapTransform(RoundedCorners(120)))
                .into(profileImage)
    }

    private fun initListeners() {
        navigationView.setNavigationItemSelectedListener(this)
        bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.diagnosis -> {
                    setFragment(diagnosis)
                    selectedItemId = R.id.diagnosis
                    true
                }
                R.id.adviser -> {
                    setFragment(medCenter)
                    selectedItemId = R.id.adviser
                    true
                }
                R.id.health_tips -> {
                    setFragment(newsFragment)
                    selectedItemId = R.id.health_tips
                    true
                }
                else -> false
            }
        }

        profileImage.setOnClickListener {
            if(prefs.getWantsToUsePin()) {
                if(hasVerifiedPin) {
                    toggleDrawer()
                    goToProfile()
                }else {
                    toggleDrawer()
                    showLockFragment()
                }
            }else {
                toggleDrawer()
                goToProfile()
            }
        }

        mLoginListener = object : PFLockScreenFragment.OnPFLockScreenLoginListener {

            override fun onCodeInputSuccessful() {
                Toast.makeText(this@Home, "Code successful", Toast.LENGTH_SHORT).show()
                goToProfile()
//                isLoggedIn = true
//                showTing()
            }

            override fun onFingerprintSuccessful() {
                Toast.makeText(this@Home, "Fingerprint successful", Toast.LENGTH_SHORT).show()
//                isLoggedIn = true
//                showTing()
            }

            override fun onPinLoginFailed() {
//                isLoggedIn = false
                Toast.makeText(this@Home, "Pin failed", Toast.LENGTH_SHORT).show()
            }

            override fun onFingerprintLoginFailed() {
//                isLoggedIn = false
                Toast.makeText(this@Home, "Fingerprint failed", Toast.LENGTH_SHORT).show()
            }
        }

    }


    private fun setFragment(fragment: Fragment) {
        val transactionManager: FragmentTransaction? = supportFragmentManager.beginTransaction()

        if (transactionManager != null) {
            transactionManager.replace(R.id.main_content, fragment)
            transactionManager.addToBackStack(null)
            transactionManager.commit()
        }
    }

    private fun initFireBase() {
        mAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase.getReference(FIREBASE_REFERENCE)
        Log.i(TAG, "current user: ${mAuth.currentUser!!.email}")
        showProfileEditDialog()
    }

    private fun showProfileEditDialog() {

        mFBaseUser = FBaseUser()
        mDatabaseReference.child("users").child(mAuth.currentUser!!.uid).child("basic_information").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.e(TAG, p0.details)
            }

            override fun onDataChange(p0: DataSnapshot) {
                mFBaseUser = if(p0.getValue(FBaseUser::class.java) == null) {
                    FBaseUser()
                }else {
                    p0.getValue(FBaseUser::class.java)!!
                }
                Log.i(TAG, "user = $mFBaseUser")

                if (mFBaseUser.gender.equals("") || mFBaseUser.dateOfBirth == null) {
                    showProfileUpdateDialog()
                }
            }

        })
    }

    private fun showProfileUpdateDialog() {
        val dialog = LovelyStandardDialog(this)
        dialog.setTitle("Update Profile")
                .setMessage("Please update your profile details to help us provide you with a more personalized experience")
                .setTopColor(resources.getColor(android.R.color.holo_blue_bright))
                .setIcon(R.drawable.ic_info_outline)
                .setPositiveButton("OK") { goToBasicInfo() }
                .setCancelable(false)
                .show()

    }

    private fun goToBasicInfo() {
        val intent = Intent(this, BasicInfo::class.java)
        startActivity(intent)
    }

    private fun initDrawer() {
        drawerToggle = ActionBarDrawerToggle(this, drawerLayout, R.string.openDrawer, R.string.closeDrawer)
        drawerLayout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()
    }

    private fun toggleDrawer() {
        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawer(Gravity.START)
        } else {
            drawerLayout.openDrawer(Gravity.START, true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.profile -> {
//                if("prefHasProfileisLocked"==1){
//                    val intent = Intent(this, Profile::class.java)
//                    startActivity(intent)
//                }
                toggleDrawer()
                if(prefs.getWantsToUsePin()) {
                    if(hasVerifiedPin) {
                        goToProfile()
                    }else {
                        showLockFragment()
                    }
                }else {
                    goToProfile()
                }
                return true
            }

            R.id.settings -> {
                val intent = Intent(this, Settings::class.java)
                startActivity(intent)
                toggleDrawer()
                return true
            }

            R.id.faq -> {
                val intent = Intent(this, Faq::class.java)
                startActivity(intent)
                toggleDrawer()
                return true
            }

            R.id.share -> {
                toggleDrawer()
                try {
                    val intent = Intent(Intent.ACTION_SEND)
                    intent.type = "text/plain"
                    intent.putExtra(Intent.EXTRA_SUBJECT, "MediBot")
                    var sAux = "\nTry this awesome app for quick and easy diagnosis\n"
                    sAux += "https://play.google.com/store/apps/details?id=com.nexus.apps.medibot"
                    intent.putExtra(Intent.EXTRA_TEXT, sAux)
                    startActivity(Intent.createChooser(intent, "Share With Friends"))
                } catch (ex: Exception) {
                    Log.e(TAG, ex.message)
                }
                return true
            }

            R.id.about -> {
                val intent = Intent(this, About::class.java)
                startActivity(intent)
                toggleDrawer()
                return true
            }

            R.id.logout -> {
                toggleDrawer()
                showDialog("Logout", "Are you sure you want to logout?")
                return true
            }
            else -> {
                false
            }
        }

    }

    private fun showDialog(heading: String, message: String) {
        val dialog = AlertDialog.Builder(this)

        dialog.setTitle(heading)
                .setMessage(message)
                .setPositiveButton(heading) { _, _ ->
                    if(heading.equals("logout", true)){
                        mAuth.signOut()
                        PFFingerprintPinCodeHelper.getInstance().delete()
                        prefs.clearPreferences()
                        clearDefaultSharedPreferences()
                        goToLogin()
                    }else {
                        finish()
                    }
                }.setNegativeButton("CANCEL", null)
                .show()
    }


    private fun goToLogin() {
        val intent = Intent(this, Login::class.java)
        startActivity(intent)
        finish()
    }

    private fun showLockFragment() {
        var isExist = PFFingerprintPinCodeHelper.getInstance().isPinCodeEncryptionKeyExist

        mLoginListener = object : PFLockScreenFragment.OnPFLockScreenLoginListener {

            override fun onCodeInputSuccessful() {
                hasVerifiedPin = true
                goToProfile()
            }

            override fun onFingerprintSuccessful() {
                hasVerifiedPin = true
                goToProfile()
            }

            override fun onPinLoginFailed() {
                if(counter == 0) {
                    mAuth.signOut()
                    PFFingerprintPinCodeHelper.getInstance().delete()
                    goToLogin()
                }else {
                    counter--
                }

                Toast.makeText(this@Home, "You have $counter attempts remaining", Toast.LENGTH_SHORT).show()
            }

            override fun onFingerprintLoginFailed() {
                if(counter == 0) {
                    mAuth.signOut()
                    PFFingerprintPinCodeHelper.getInstance().delete()
                    goToLogin()
                }else {
                    counter--
                }

                Toast.makeText(this@Home, "You have $counter attempts remaining", Toast.LENGTH_SHORT).show()
            }
        }

        mCodeCreateListener = PFLockScreenFragment.OnPFLockScreenCodeCreateListener { encodedCode ->
            Toast.makeText(this@Home, "Code created", Toast.LENGTH_SHORT).show()
            prefs.setEncryptedPin(encodedCode)
        }
        showLockScreenFragment(isExist)
    }

    private fun showLockScreenFragment(isPinExist: Boolean) {
        val builder = PFFLockScreenConfiguration.Builder(this)
                .setTitle(if (isPinExist) "Unlock with your pin code or fingerprint" else "Create Code")
                .setCodeLength(4)
                .setLeftButton("Can't remember") { }
                .setUseFingerprint(hasFingerprintHardware(this))

        val fragment = PFLockScreenFragment()

        builder.setMode(if (isPinExist)
            PFFLockScreenConfiguration.MODE_AUTH
        else
            PFFLockScreenConfiguration.MODE_CREATE)
        if (isPinExist) {
            fragment.setEncodedPinCode(prefs.getEncryptedPin())
            fragment.setLoginListener(mLoginListener)
        }

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        hideToolBarAndBottomNav()

        fragment.setConfiguration(builder.build())
        fragment.setCodeCreateListener(mCodeCreateListener)
        supportFragmentManager.beginTransaction()
                .replace(R.id.main_content, fragment).commit()
    }


    private fun goToProfile() {
        val intent = Intent(this, Profile::class.java)
        startActivity(intent)
    }

    private fun hideToolBarAndBottomNav() {
        bottomNavigation.visibility = View.GONE
        toolBar.visibility = View.GONE
    }

    private fun showToolBarAndBottomNav() {
        bottomNavigation.visibility = View.VISIBLE
        toolBar.visibility = View.VISIBLE
    }

    private fun clearDefaultSharedPreferences() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val prefEditor = prefs.edit()
        prefEditor.clear().apply()
    }
}
