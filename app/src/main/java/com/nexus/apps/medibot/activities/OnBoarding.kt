package com.nexus.apps.medibot.activities

import android.content.Intent
import android.os.Bundle
import com.codemybrainsout.onboarder.AhoyOnboarderActivity
import com.codemybrainsout.onboarder.AhoyOnboarderCard
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.utils.PrefsManager

class OnBoarding : AhoyOnboarderActivity() {

    private lateinit var pref: PrefsManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        pref = PrefsManager(this)

        if (pref.getHasSeenOnboarding()) {
            goToSplashScreen()
        } else {
            initViews()
        }
    }

    private fun initViews() {

        val ahoyOnboarderCard1 = AhoyOnboarderCard(resources.getString(R.string.diag_text), resources.getString(R.string.diag_desc), R.drawable.diag)
        val ahoyOnboarderCard2 = AhoyOnboarderCard(resources.getString(R.string.news_text), resources.getString(R.string.news_desc), R.drawable.news1)
        val ahoyOnboarderCard3 = AhoyOnboarderCard(resources.getString(R.string.center_text), resources.getString(R.string.center_desc), R.drawable.med)


        val pages = ArrayList<AhoyOnboarderCard>()

        pages.add(ahoyOnboarderCard1)
        pages.add(ahoyOnboarderCard2)
        pages.add(ahoyOnboarderCard3)

        for (page: AhoyOnboarderCard in pages) {
            page.setTitleColor(R.color.white)
            page.setDescriptionColor(R.color.grey_200)
            page.setBackgroundColor(R.color.black_transparent)
            page.setIconLayoutParams(400, 400, 200, 50, 50, 20)
        }

        setFinishButtonTitle("Finish")
        showNavigationControls(true)
        setGradientBackground()

        setOnboardPages(pages)
    }

    private fun goToSplashScreen() {
        val intent = Intent(this, SplashScreen::class.java)
        startActivity(intent)
        finish()
    }

    override fun onFinishButtonPressed() {
        pref.setHasSeenOnboarding()
        goToSplashScreen()
    }

}
