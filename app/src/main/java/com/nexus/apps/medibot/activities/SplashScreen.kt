package com.nexus.apps.medibot.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ProgressBar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.utils.FIREBASE_REFERENCE
import com.nexus.apps.medibot.utils.PrefsManager
import com.nexus.apps.medibot.utils.isConnectedToInternet

class SplashScreen : AppCompatActivity() {

    private lateinit var prefs: PrefsManager
    private lateinit var mAuth: FirebaseAuth
    private lateinit var progressBar: ProgressBar
    private lateinit var mDatabase: FirebaseDatabase
    private lateinit var mDatabaseReference: DatabaseReference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        prefs = PrefsManager(this)
        mAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase.getReference(FIREBASE_REFERENCE)

        initView()

        if (isConnectedToInternet(this)) {
            checkActiveUser()
        } else {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Warning")
                    .setMessage("You need internet connection else some of the features of this app wont work well")
                    .setPositiveButton("OK") { _, _ -> checkActiveUser() }
                    .setNegativeButton("EXIT") { p0, _ ->
                        p0!!.cancel()
                        exitApplication()
                    }.show()
//            Toast.makeText(this, "Please make sure you have internet connection before you continue", Toast.LENGTH_LONG).show()
        }
    }

    private fun initView() {
        progressBar = findViewById(R.id.pb_loading)
    }

    private fun goToLogin() {
        val intent = Intent(this, Login::class.java)
        startActivity(intent)
        finish()
    }


    private fun checkActiveUser() {
        val currentUser = mAuth.currentUser
        if (currentUser != null) {
            Log.d(this.localClassName, "currentUser: ${currentUser.email}")
            Handler().postDelayed({
                goToHome()
            }, 1000)
        } else {
            goToLogin()
        }
    }

    private fun exitApplication() {
        finish()
    }

    private fun goToHome() {
        val intent = Intent(this, Home::class.java)
        startActivity(intent)
        finish()
    }
}
