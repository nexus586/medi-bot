package com.nexus.apps.medibot.adapters

import android.content.Context
import android.net.Uri
import android.support.customtabs.CustomTabsIntent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.models.Condition

class DiagnosisResultsAdapter(private var context: Context, private var conditions: ArrayList<Condition>) : RecyclerView.Adapter<DiagnosisResultsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiagnosisResultsAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.diagnosis_results, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = conditions.size

    override fun onBindViewHolder(holder: DiagnosisResultsAdapter.ViewHolder, position: Int) {
        val condition = conditions[position]

        holder.name.text = condition.name
        holder.prevalence.text = String.format("Prevalence: %s", condition.prevalence)
        holder.severity.text = String.format("Severity: %s", condition.severity)
        holder.probability.text = condition.probability.toString() + "%"

        holder.moreInfo.setOnClickListener {
            Log.d("nexusTest", "condition name: ${condition.common_name}")
            val url = "https://google.com.gh/search?q=" + condition.name.replace(" ", "+").toLowerCase()

            Log.d("nexusTest", "nhsUrl :$url")
            val tabBuilder = CustomTabsIntent.Builder()
            tabBuilder.setToolbarColor(context.resources.getColor(R.color.colorPrimary))
            val tabsIntent = tabBuilder.build()
            tabsIntent.launchUrl(context, Uri.parse(url))
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.name)
        var prevalence: TextView = itemView.findViewById(R.id.prevalence)
        var severity: TextView = itemView.findViewById(R.id.severity)
        var moreInfo: TextView = itemView.findViewById(R.id.more_info)
        var probability: TextView = itemView.findViewById(R.id.probability)

    }
}