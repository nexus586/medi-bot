package com.nexus.apps.medibot.models

data class Issue(
        var Id: Int? = 0,
        var Name: String? = "",
        var IcdName: String? = "",
        var ProfName: String? = "",
        var Accuracy: Float? = 0f,
        var Ranking: Float? = 0f
)