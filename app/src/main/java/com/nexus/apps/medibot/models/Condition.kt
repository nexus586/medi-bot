package com.nexus.apps.medibot.models

data class Condition(
        var id: String = "",
        var name: String = "",
        var common_name: String = "",
        var probability: Int = 0,
        var prevalence: String = "",
        var duration: String = "",
        var severity: String = "",
        var diagnosis: String = ""
)