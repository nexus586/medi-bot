package com.nexus.apps.medibot.activities

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import android.widget.Toast
import com.beautycoder.pflockscreen.PFFLockScreenConfiguration
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment
import com.beautycoder.pflockscreen.security.PFFingerprintPinCodeHelper
import com.google.firebase.auth.FirebaseAuth
import com.nexus.apps.medibot.AppCompatPreferenceActivity
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.utils.PrefsManager
import kotlinx.android.synthetic.main.lock_container.view.*

class LockScreen: AppCompatActivity() {

    private lateinit var prefs: PrefsManager
    private lateinit var mLoginListener: PFLockScreenFragment.OnPFLockScreenLoginListener
    private lateinit var mCodeCreateListener: PFLockScreenFragment.OnPFLockScreenCodeCreateListener
    private var counter: Int = 4


    private lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lock_container)

        mAuth = FirebaseAuth.getInstance()

        prefs = PrefsManager(this)
        showLockFragment()

    }

    override fun onBackPressed() {
        if(counter > 0) {
            Toast.makeText(this, "Please enter pin", Toast.LENGTH_LONG).show()
        }
    }

    private fun showLockFragment() {
        val isExist = PFFingerprintPinCodeHelper.getInstance().isPinCodeEncryptionKeyExist
        mLoginListener = object : PFLockScreenFragment.OnPFLockScreenLoginListener {

            override fun onCodeInputSuccessful() {
                if(prefs.getWantsToUsePin()) {
                    goToHome()
                }else {
                    Toast.makeText(this@LockScreen, "Pin disabled", Toast.LENGTH_LONG).show()
                    PFFingerprintPinCodeHelper.getInstance().delete()
                    goToHome()
                }
            }

            override fun onFingerprintSuccessful() {
                if(prefs.getWantsToUsePin()) {
                    goToHome()
                }else {
                    Toast.makeText(this@LockScreen, "Pin disabled", Toast.LENGTH_LONG).show()
                    PFFingerprintPinCodeHelper.getInstance().delete()
                    goToHome()
                }
            }

            override fun onPinLoginFailed() {
                if(counter == 0) {
                    mAuth.signOut()
                    goToLogin()
                }else
                    counter--

                Toast.makeText(this@LockScreen, "You have $counter attempts remaining", Toast.LENGTH_SHORT).show()
            }

            override fun onFingerprintLoginFailed() {
                Toast.makeText(this@LockScreen, "Fingerprint failed", Toast.LENGTH_SHORT).show()
            }
        }

        mCodeCreateListener = PFLockScreenFragment.OnPFLockScreenCodeCreateListener { encodedCode ->
            Toast.makeText(this, "Code created", Toast.LENGTH_SHORT).show()
            prefs.setEncryptedPin(encodedCode)
            finish()
            goToHome()
        }
        showLockScreenFragment(isExist)
    }

    private fun goToHome() {
        startActivity(Intent(this, Home::class.java))
    }

    private fun showLockScreenFragment(isPinExist: Boolean) {
        val builder = PFFLockScreenConfiguration.Builder(this)
                .setTitle(if (isPinExist) "Unlock with your pin code or fingerprint" else "Create Code")
                .setCodeLength(4)
                .setLeftButton("Can't remember") { }
                .setUseFingerprint(true)
                .setNextButton("Finish")
        val fragment = PFLockScreenFragment()

        builder.setMode(if (isPinExist)
            PFFLockScreenConfiguration.MODE_AUTH
        else
            PFFLockScreenConfiguration.MODE_CREATE)
        if (isPinExist) {
            fragment.setEncodedPinCode(prefs.getEncryptedPin())
            fragment.setLoginListener(mLoginListener)
        }

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        fragment.setConfiguration(builder.build())
        fragment.setCodeCreateListener(mCodeCreateListener)
        supportFragmentManager.beginTransaction()
                .replace(R.id.lock_container, fragment).commit()

    }

    private fun goToLogin() {
        val intent = Intent(this, Login::class.java)
        startActivity(intent)
        finish()
    }
}