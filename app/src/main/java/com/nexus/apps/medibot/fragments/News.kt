package com.nexus.apps.medibot.fragments


import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.adapters.NewsAdapter
import com.nexus.apps.medibot.models.Article
import com.nexus.apps.medibot.utils.*
import com.yarolegovich.lovelydialog.LovelyStandardDialog
import org.json.JSONObject

/**
 * A simple [Fragment] subclass.
 *
 */
class News : Fragment() {

    private val TAG = this::class.java.simpleName

    private lateinit var sponsorText: TextView
    private lateinit var newsRecycler: RecyclerView
    private lateinit var newsAdapter: NewsAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var loadingView: RelativeLayout
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var testView: View

    private var articlesList = ArrayList<Article>()

    private lateinit var notConnectedView: RelativeLayout
    private lateinit var retryButton: Button

    private var hasLoadedMessages = false
    private var hasLoadedNewMessages = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_news, container, false)

        testView = view

        initViews(view)

        if (!isConnectedToInternet(view.context)) {
            showNotConnectedView(view)
            loadingView.visibility = View.GONE
        } else {
            if (staticNews.size > 0) {
                initAdapters(view)
                initListeners(view)
            } else {
                hideViews()
                loadNewsItems(view)
            }
        }


        return view
    }

    private fun initViews(view: View) {
        sponsorText = view.findViewById(R.id.news_api_link)
        newsRecycler = view.findViewById(R.id.news_recycler)
        loadingView = view.findViewById(R.id.loading_center)
        swipeRefresh = view.findViewById(R.id.swipeRefresh)
        notConnectedView = view.findViewById(R.id.not_connected_view)
        retryButton = view.findViewById(R.id.retry_button)
    }

    private fun initAdapters(view: View) {
//        if(staticNews.size > 0) {
            newsAdapter = NewsAdapter(view.context, staticNews)
//        } else {
//            newsAdapter =
//        }
        layoutManager = LinearLayoutManager(view.context)

        layoutManager.orientation = LinearLayoutManager.VERTICAL
        layoutManager.isSmoothScrollbarEnabled = true

        newsRecycler.adapter = newsAdapter
        newsRecycler.layoutManager = layoutManager

    }

    private fun initListeners(view: View) {
        swipeRefresh.setOnRefreshListener {
            refreshNewsDataItems(view)
        }
    }


    private fun loadNewsItems(view: View) {

        view.run {
            val url = "$NEWS_API_URL?country=us&category=health"

            Log.d(TAG, "inside load news item")

            try {
                AndroidNetworking.get(url)
                        .addHeaders("X-Api-Key", NEWS_API_API_KEY)
                        .setTag(TAG)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsJSONObject(object : JSONObjectRequestListener {
                            override fun onResponse(response: JSONObject?) {
                                if (response != null) {
                                    Log.d(TAG, "response is: $response")
                                    articlesList = getArticlesFromResult(response)
                                    staticNews = articlesList
                                    Log.d(TAG, "Articles list: $articlesList")
                                    if (hasLoadedNewMessages) {
                                        swipeRefresh.isRefreshing = false
                                        Toast.makeText(view.context, "News updated", Toast.LENGTH_SHORT).show()
                                    }
                                    initAdapters(view)
                                    showViews()
                                    initListeners(view)
                                }
                            }

                            override fun onError(anError: ANError?) {
                                showErrorDialog(view)
                                swipeRefresh.isRefreshing = false
                                Log.e(TAG, "An unexpected error occurred: $anError")
                            }

                        })
            } catch (ex: Exception) {
                Log.e(TAG, "an error occurred: $ex")
            }
        }
    }

    private fun refreshNewsDataItems(view: View) {
        view.run {
            val url = "$NEWS_API_URL?country=us&category=health"

            Toast.makeText(view.context, "updating news", Toast.LENGTH_LONG).show()

            try {
                AndroidNetworking.get(url)
                        .addHeaders("X-Api-Key", NEWS_API_API_KEY)
                        .setTag(TAG)
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsJSONObject(object : JSONObjectRequestListener {
                            override fun onResponse(response: JSONObject?) {
                                if (response != null) {
                                    swipeRefresh.isRefreshing = false
                                    Log.d(TAG, "response is: $response")
                                    articlesList = getArticlesFromResult(response)
                                    staticNews = articlesList
                                    Log.d(TAG, "Articles list: $articlesList")
                                    newsAdapter.notifyDataSetChanged()
                                }
                            }

                            override fun onError(anError: ANError?) {
                                Toast.makeText(view.context, "Unable to update news", Toast.LENGTH_SHORT).show()
                                swipeRefresh.isRefreshing = false
                                Log.e(TAG, "An unexpected error occurred: $anError")
                            }

                        })
            } catch (ex: Exception) {
                Log.e(TAG, "an error occurred: $ex")
            }
        }
    }

    private fun showNotConnectedView(view: View) {
        notConnectedView.visibility = View.VISIBLE
        retryButton.setOnClickListener {
            if(!isConnectedToInternet(view.context)) {
                Toast.makeText(view.context, "Please check your internet connection and try again", Toast.LENGTH_SHORT).show()
            }else {
                loadingView.visibility = View.VISIBLE
                notConnectedView.visibility = View.GONE
                loadNewsItems(view)
            }
        }
    }

    private fun hideViews() {
        newsRecycler.visibility = View.GONE
        sponsorText.visibility = View.GONE

        loadingView.visibility = View.VISIBLE
    }

    private fun showViews() {
        newsRecycler.visibility = View.VISIBLE
        sponsorText.visibility = View.VISIBLE

        loadingView.visibility = View.GONE
    }

    private fun showErrorDialog(view: View) {
        val dialog = LovelyStandardDialog(view.context)
        dialog.setTitle("Error")
                .setMessage("Unable to reach server")
                .setTopColor(Color.RED)
                .setIcon(resources.getDrawable(R.drawable.ic_info_outline))
                .setPositiveButton("Retry") { loadNewsItems(view) }
                .show()
    }

}
