package com.nexus.apps.medibot.activities


import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.agrawalsuneet.loaderspack.loaders.PulseLoader
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.nexus.apps.medibot.R
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.RelativeLayout
import android.widget.Toast
import com.nexus.apps.medibot.adapters.MedicationsAdapter
import com.nexus.apps.medibot.models.Medications
import com.nexus.apps.medibot.utils.FIREBASE_REFERENCE


class Medication : AppCompatActivity() {

    private val TAG = this::class.java.simpleName

    private lateinit var pulseLoader: PulseLoader
    private lateinit var isLoadingText: TextView
    private lateinit var fab: FloatingActionButton
    private lateinit var recyclerView: RecyclerView
    private lateinit var mFirebaseAuth: FirebaseAuth
    private lateinit var mDatabase: FirebaseDatabase
    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var listener: ValueEventListener
    private lateinit var medicationDatabaseReference: DatabaseReference
    private lateinit var adapter: MedicationsAdapter
    private lateinit var notAvailableLayout: RelativeLayout

    private var isLoading = true
    private var medicationsList: ArrayList<Medications> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_medication)

        initViews()
        initFirebase()
        initListeners()

        if(isLoading) {
            hideViews()
        }
    }

    override fun onResume() {
        super.onResume()
        medicationsList = ArrayList()
        if(callingActivity != null) {
            if(callingActivity!!.className.equals(AddMedication::class.java)) {
                refreshPage()
            }
        }

        initFirebase()
    }

    private fun refreshPage() {
        finish()
        startActivity(intent)
    }

    private fun initFirebase() {
        mFirebaseAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase.getReference(FIREBASE_REFERENCE)
        getMedicationsFromFirebase()
    }

    private fun initListeners() {
        fab.setOnClickListener {
            startActivity(Intent(this, AddMedication::class.java))
        }
    }

    private fun initAdapters() {
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        adapter = MedicationsAdapter(this@Medication, medicationsList)
        recyclerView.adapter = adapter
    }

    private fun initViews() {
        val actionBar = supportActionBar
        actionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_outline_profile)
            title = "Medication"
        }

        pulseLoader = findViewById(R.id.pulse_loader)
        isLoadingText = findViewById(R.id.isLoadingText)
        fab = findViewById(R.id.fab)
        recyclerView = findViewById(R.id.medication)
        notAvailableLayout = findViewById(R.id.not_available_layout)

        if(callingActivity != null) {
            if(callingActivity!!.className.equals(AddMedication::class.java)) {
                refreshPage()
            }
        }
    }

    private fun showViews() {
        if (medicationsList.size == 0) {
            notAvailableLayout.visibility = View.VISIBLE
        }else{
            medicationDatabaseReference.removeEventListener(listener)
            recyclerView.visibility = View.VISIBLE
        }

        fab.visibility = View.VISIBLE
        isLoadingText.visibility = View.GONE
        pulseLoader.visibility = View.GONE
    }

    private fun hideViews() {
        fab.visibility = View.GONE
        recyclerView.visibility = View.GONE

        isLoadingText.visibility = View.VISIBLE
        pulseLoader.visibility = View.VISIBLE
    }

    private fun getMedicationsFromFirebase() {
        medicationDatabaseReference = mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("medications")
        listener = medicationDatabaseReference.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.w(TAG, "loadPost:onCancelled", p0.toException())
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (postSnapshot in dataSnapshot.children){
                    val temp = Medications(
                            medicationName = postSnapshot.child("medicationName").value.toString(),
                            medicationId = postSnapshot.child("medicationId").value.toString(),
                            dosage = postSnapshot.child("dosage").value.toString())
                    if(temp.medicationName != "null"){
                        if(!medicationsList.contains(temp)){
                            medicationsList.add(temp)
                        }
                    }
                }
                Log.i(TAG, "Medications loaded. Initializing views")
                isLoading = false
                initAdapters()
                showViews()
            }

        })
    }

    private fun deleteMedication(id: String) {
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("medications").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.e(TAG, "error: ${p0.message}")
                Log.e(TAG, "error details: ${p0.details}")
            }

            override fun onDataChange(p0: DataSnapshot) {
                for(snapshot in p0.children) {
                    if(snapshot.child("medicationId").value.toString().equals(id, true)) {
                        snapshot.ref.removeValue()
                    }
                }
                Toast.makeText(this@Medication, "Medication deleted refreshing", Toast.LENGTH_SHORT).show()
                finish()
                startActivity(intent)
            }

        })
    }

    fun showDeleteDialog(name: String, id: String) {

        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Delete?")
                .setMessage("Are you sure you want to delete medication with name $name?")
                .setPositiveButton("DELETE") { _, _ ->
                    deleteMedication(id)
                }.setNegativeButton("CANCEL", null)
                .setIcon(R.drawable.medication)
                .show()

    }
}

