package com.nexus.apps.medibot.activities

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.adapters.BotAdapter
import com.nexus.apps.medibot.models.*
import com.nexus.apps.medibot.utils.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import javax.net.ssl.HttpsURLConnection
import kotlin.collections.ArrayList

class PersonalDiagnosis : AppCompatActivity() {

    private val TAG = PersonalDiagnosis::class.simpleName

    private lateinit var recyclerView: RecyclerView
    private lateinit var sendButton: Button
    private lateinit var symptomText: EditText
    private lateinit var autoCompleteTextView: AutoCompleteTextView
    private lateinit var mAuth: FirebaseAuth
    private lateinit var mFirebaseReference: DatabaseReference
    private lateinit var mFirebaseDatabase: FirebaseDatabase
    private lateinit var adapter: BotAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var toolbar: Toolbar
    private lateinit var bottomLayout: LinearLayout
    private lateinit var conditions: ArrayList<Condition>
    var resultConditions: ArrayList<Condition> = ArrayList()
    private var symptoms: ArrayList<Symptoms>? = null
    private var symptomsJsonArray: JSONArray = JSONArray()
    private var age = 22
    private var gender = "male"
    private var symptomsArray = JSONArray()
    private var questionCount = 0
    private var count = 0
    private var symptomCount = 0
    private lateinit var responses: Array<String>
    private var counter = 0
    private var diagnosisJSONObject = JSONObject()
    private var hasLoadedSymptoms: Boolean = false
    private lateinit var autoCompleteAdapter: ArrayAdapter<String>
    private lateinit var user: FBaseUser
    private var diagnosisChatMessages: ArrayList<BotMessage> = ArrayList()
    private lateinit var coordinatorLayout: CoordinatorLayout


    private lateinit var prefsManager: PrefsManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personal_diagnosis)

        val intentThatStartedThisActivity = intent
        initFirebase()


        initViews()
        if (hasLoadedSymptoms) {
            initAdapter()
            initListeners()
        } else if (intentThatStartedThisActivity.hasExtra(Intent.EXTRA_TEXT)) {
            val diagnosisChatId = intentThatStartedThisActivity.getStringExtra(Intent.EXTRA_TEXT)
            Log.i(TAG, "Intent started with chat id: " + diagnosisChatId.toString())
            getDiagnosisChatMessagesFromFirebase(diagnosisChatId)
        } else {
            autoCompleteTextView.isEnabled = false
            symptomText.isEnabled = false
            LoadSymptoms().execute()
        }


    }

    private fun initViews() {
        prefsManager = PrefsManager(this)
        title = "Personal Diagnosis"
        recyclerView = findViewById(R.id.message_container)
        sendButton = findViewById(R.id.btn_send)
        symptomText = findViewById(R.id.symptom_text)
        toolbar = findViewById(R.id.toolbar)
        toolbar.title = "Personal Diagnosis"
        bottomLayout = findViewById(R.id.chat_container)
        autoCompleteTextView = findViewById(R.id.autoCompleteTextView)
        responses = getResponses()
        coordinatorLayout = findViewById(R.id.coordinator)

        autoCompleteTextView.threshold = 1

        setSupportActionBar(toolbar)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.apply {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_back)
        }
    }


    private fun initFirebase() {
        mAuth = FirebaseAuth.getInstance()
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mFirebaseReference = mFirebaseDatabase.getReference(FIREBASE_REFERENCE)
        getUserDetailsFromFirebase()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = MenuInflater(this)
        inflater.inflate(R.menu.diagnosis_menu, menu)
        return true
    }

    private fun getDiagnosisChatMessagesFromFirebase(diagnosisChatId: String) {
        Log.i(TAG, "Calling firebase to retrieve diagnosis chat ID: $diagnosisChatId")
        mFirebaseReference.child("users").child(mAuth.currentUser!!.uid).child("diagnoses").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", p0.toException())
            }

            override fun onDataChange(p0: DataSnapshot) {
                for (postSnapshot in p0.children) {
                    if (postSnapshot.child("id").value.toString().trim() == diagnosisChatId.trim()) {
                        Log.i(TAG, "Diagnosis ID matched with ID in firebase. Retrieving messages")
                        postSnapshot.child("messages").children.forEach {
                            val temp = it.getValue((BotMessage::class.java))
                            if (temp != null)
                                diagnosisChatMessages.add(temp)
                        }
                        Log.i(TAG, "Retrieved " + diagnosisChatMessages.size.toString() + " messages for chat " + diagnosisChatId)
                        showMessagesInView()
                    }
                }
            }

        })
    }

    private fun showMessagesInView() {
        bottomLayout.visibility = View.INVISIBLE
        val bar: View = findViewById(R.id.bar_above_chat_container)
        bar.visibility = View.INVISIBLE
        if (diagnosisChatMessages.size > 0) {
            Log.i(TAG, "Messages retrieved from firebase. Passing to BotAdapter")
            adapter = BotAdapter(this, diagnosisChatMessages)
            layoutManager = LinearLayoutManager(this)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            layoutManager.isSmoothScrollbarEnabled = true
            recyclerView.layoutManager = layoutManager
            recyclerView.adapter = adapter
        } else {
            Log.w(TAG, "PULLED AN EMPTY DIAGNOSIS CHAT FROM FIREBASE")
        }
    }

    private fun initListeners() {
        sendButton.setOnClickListener {
            if (symptomText.text.toString().isEmpty()) {
                symptomText.showSoftInputOnFocus = true
            } else {
                sendMyMessage(symptomText.text.toString())
                hideSoftKeyboard()
                bottomLayout.visibility = View.GONE
//                symptomText.text = null
            }
        }

        autoCompleteTextView.onItemClickListener = AdapterView.OnItemClickListener { _, _, _, _ ->

            val symptom = autoCompleteTextView.text.toString()

            try {
                val jsonArray = JSONArray()

                if (symptomsArray.length() == 0) {
                    val jb = JSONObject()
                    try {
                        jb.put("id", findJsonItemIdByName(symptom))
                        jb.put("choice_id", "present")
                    } catch (ex: Exception) {
                        Log.e(TAG, ex.message)
                    }
                    jsonArray.put(jb)
                } else {
                    val len = symptomsArray.length() + 1

                    for (i in (0 until len)) {
                        if (i == (len - 1)) {
                            val jb = JSONObject()
                            jb.put("id", findJsonItemIdByName(symptom))
                            jb.put("choice_id", "present")
                            jsonArray.put(jb)
                        } else {
                            jsonArray.put(symptomsArray.getJSONObject(i))
                        }
                    }
                }

                symptomsArray = jsonArray
            } catch (ex: Exception) {
                Log.e(TAG, ex.message)
            }

            sendMyMessage(symptom)
            hideSoftKeyboard()
            bottomLayout.visibility = View.GONE
            sendResponse()
            autoCompleteTextView.setText("")
        }
    }

    private fun findJsonItemIdByName(name: String): String {
        var id = ""

        for (i in (0 until symptomsJsonArray.length())) {
            val jb = symptomsJsonArray.getJSONObject(i)
            if (jb.getString("name").equals(name, true)) {
                id = jb.getString("id")
            }
        }
        return id
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {

            R.id.home -> {
                super.onOptionsItemSelected(item)
                true
            }

            R.id.refresh -> {
                finish()
                startActivity(intent)
                true
            }

            else -> false
        }
    }

    private fun initAdapter() {
        val messages = ArrayList<BotMessage>()

        if(prefsManager.getHasSeenPersonalDiagnosisTutorial()) {
            val botMessage1 = BotMessage(senderId = 2, type = TYPE_TEXT, message = String.format(resources.getString(R.string.other_intro), mAuth.currentUser!!.displayName))
            messages.add(botMessage1)
        }else {
            val botMessage1 = BotMessage(senderId = 2, type = TYPE_TEXT, message = String.format(resources.getString(R.string.intro), mAuth.currentUser!!.displayName))
            messages.add(botMessage1)

            val botMessage2 = BotMessage(senderId = 2, type = TYPE_TEXT, message = resources.getString(R.string.prototype))
            messages.add(botMessage2)
            val botMessage4 = BotMessage(senderId = 2, type = TYPE_TEXT, message = resources.getString(R.string.info))
            messages.add(botMessage4)
            prefsManager.setHasSeenPersonalDiagnosisTutorial()
        }


        adapter = BotAdapter(this, messages)
        layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        layoutManager.isSmoothScrollbarEnabled = true

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter

    }

    private fun hideSoftKeyboard() {
        if (currentFocus != null) {
            val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(currentFocus.windowToken, 0)
        }
    }

    fun continueWithSymptoms() {
        bottomLayout.visibility = View.VISIBLE
    }

    fun beginDiagnosis() {
        autoCompleteTextView.visibility = View.GONE
        hideSoftKeyboard()
        CheckDiagnosis().execute()
    }

    fun sendMyMessage(message: String) {
        val botMessage = BotMessage(message = message, senderId = 1, type = TYPE_TEXT)

        showMessage(botMessage)
    }


    private fun saveMessagesToFirebase(diagnosisChat: DiagnosisChat) {
        Log.i(TAG, "Calling firebase to save diagnosis")
        mFirebaseReference.child("users").child(mAuth.currentUser!!.uid).child("diagnoses").push().setValue(diagnosisChat)
    }

    fun showMessage(message: BotMessage) {

        runOnUiThread {
            adapter.add(message)
            adapter.notifyDataSetChanged()
            scrollDown()
        }
    }

    private fun scrollDown() {
        layoutManager.scrollToPosition(adapter.itemCount - 1)
    }

    fun sendMyReplyMessage(message: String, symptom_id: String, choice_id: String) {
        val botMessage = BotMessage(senderId = 1, message = message, type = TYPE_TEXT)

        showMessage(botMessage)

        try {
            val array = JSONArray()

            val len = symptomsArray.length() + 1
            for (i in (0 until len)) {
                if (i == (len - 1)) {
                    val jb = JSONObject()
                    jb.put("id", symptom_id)
                    jb.put("choice_id", choice_id)
                    array.put(jb)
                } else {
                    array.put(symptomsArray.getJSONObject(i))
                }
            }

            symptomsArray = array
        } catch (e: Exception) {
            Log.e(TAG, e.message)
        }

        if (questionCount > 13) {
            val newBotMessage = BotMessage(senderId = 2, type = TYPE_TEXT, message = resources.getString(R.string.conclusion))
            showMessage(newBotMessage)
            GetCondition(conditions[0].id, conditions[0].probability).execute()
        } else {
            CheckDiagnosis().execute()
        }
    }

    fun sendMyReplyMessageWithoutQuery(message: String, symptom_id: String, choice_id: String) {
        val botMessage = BotMessage(senderId = 1, message = message, type = TYPE_TEXT)

        showMessage(botMessage)

        try {

            val jArray = JSONArray()
            val len = symptomsArray.length() + 1
            for (i in (0 until len)) {
                if (i == (len - 1)) {
                    val obj = JSONObject()
                    obj.put("id", symptom_id)
                    obj.put("choice_id", choice_id)
                    jArray.put(obj)
                } else {
                    jArray.put(symptomsArray.getJSONObject(i))
                }
            }

            symptomsArray = jArray

            handleMultipleQuestions(symptoms!![symptomCount])
        } catch (e: Exception) {
            Log.e(TAG, e.message)
        }
    }

    fun handleMultipleQuestions(symptom: Symptoms) {
        val botMessage = BotMessage(senderId = 2)

        if (symptomCount == (symptoms!!.size - 1)) {
            botMessage.type = TYPE_SINGLE
            symptomCount = 0
        } else {
            botMessage.type = TYPE_GROUP_MULTIPLE
        }
        botMessage.message = symptom.name
        botMessage.symptomId = symptom.id
        botMessage.option1 = "Yes"
        botMessage.option2 = "No"
        botMessage.option3 = "Don't know"
        symptomCount++

        showMessage(botMessage)
    }

    private fun isDiseaseFound(): Boolean {
        for (condition in conditions) {
            if (condition.probability > 75)
                return true
        }
        return false
    }

    private fun getResponses(): Array<String> {
        return resources.getStringArray(R.array.options)
    }

    private fun sendResponse() {
        if (counter == responses.size) {
            counter = 0
        }
        val botMessage = BotMessage(message = responses[counter], option1 = "Yes", option1Id = "affirm",option2 = "No", option2Id = "negate",type = TYPE_SINGLE)
        showMessage(botMessage)
        counter++
    }


    private fun getUserDetailsFromFirebase() {
        user = FBaseUser()

        mFirebaseReference.child("users").child(mAuth.currentUser!!.uid).child("basic_information").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.e(TAG, p0.message)
            }

            override fun onDataChange(p0: DataSnapshot) {
                user = p0.getValue(FBaseUser::class.java)!!

                gender = user.gender ?: "male"

                age = if (user.dateOfBirth != null) {
                    getAgeFromDOB(user.dateOfBirth!!)
                } else {
                    21
                }
            }

        })
    }

    fun getAgeFromDOB(date: Date): Int {
        val calender = Calendar.getInstance()
        val currentYear = calender.get(Calendar.YEAR)

        val userDOB = Calendar.getInstance()
        userDOB.time = date

        val userYear = userDOB.get(Calendar.YEAR)

        return currentYear - userYear
    }

    inner class GetCondition(private var conditionId: String, private var probability: Int) : AsyncTask<String, String, String>() {
        private var success = 0
        private var condition = Condition()

        override fun doInBackground(vararg p0: String?): String? {

            val httpEndpoint: URL?
            try {
                httpEndpoint = URL("https://api.infermedica.com/v2/conditions/$conditionId")
                val secureConnection = httpEndpoint.openConnection() as HttpsURLConnection
                secureConnection.setRequestProperty(APP_ID, SECONDARY_INFERMEDICA_APP_ID)
                secureConnection.setRequestProperty(APP_KEY, SECONDARY_INFERMEDICA_APP_KEY)
                secureConnection.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON_VALUE)

                secureConnection.requestMethod = "GET"

                if (secureConnection.responseCode == 200) {
                    success = 1

                    val responseBody = secureConnection.inputStream

                    val bR = BufferedReader(InputStreamReader(responseBody))
                    var line: String?

                    val responseBuilder = StringBuilder()
                    do {
                        line = bR.readLine() ?: null
                        if (line == null)
                            break
                        else
                            responseBuilder.append(line)
                    } while (true)
                    responseBody.close()

                    val result = JSONObject(responseBuilder.toString())
                    condition = getConditionFromResults(result)
                    condition.probability = probability

                } else {
                    success = 0
                    Log.e("nexus", "failure")
                }

                secureConnection.disconnect()

            } catch (e: Exception) {
                Log.e(TAG, e.message)
                Snackbar.make(coordinatorLayout, "Sorry, an unexpected error occurred. Please try again later.", Snackbar.LENGTH_LONG).show()
            }

            return null
        }

        override fun onPostExecute(result: String?) {

            if (success == 1) {
                resultConditions.add(condition)

                if (count <= 4) {

                    val botMessage = BotMessage(type = TYPE_DIAGNOSIS_RESULTS,
                            conditions = resultConditions)
                    showMessage(botMessage)

                    val diagnosis = BotMessage(type = TYPE_TEXT, message = resultConditions[0].diagnosis)
                    showMessage(diagnosis)

                    val botMessage2 = BotMessage(type = TYPE_TEXT, message = resources.getString(R.string.disclosure))
                    showMessage(botMessage2)
                    Log.i(TAG, "Diagnosis complete. Building diagnosis chat object to send to firebase.")
                    val dateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm")
                    val date = Date()
                    val diagnosisChat = DiagnosisChat(
                            messages = adapter.getMessages(),
                            id = UUID.randomUUID().toString().split('-')[0],
                            date = dateFormat.format(date),
                            recommendation = resultConditions[0].name
                    )
                    saveMessagesToFirebase(diagnosisChat)
                } else {
                    GetCondition(conditions[count].id, conditions[count].probability).execute()
                    count++
                }
            }
        }

    }

    fun getConditions(conditions: ArrayList<Condition>): ArrayList<String> {
        val arrayList = ArrayList<String>()

        for (i in (0 until conditions.size)) {
            arrayList.add(conditions[i].name)
        }

        return arrayList
    }


    inner class CheckDiagnosis : AsyncTask<String, String, String>() {

        private var diagnosisObj: DiagnosisObj = DiagnosisObj()
        private var success: Int = 0

        override fun onPreExecute() {
            toolbar.apply {
                subtitle = "Processing information...."
            }
        }

        override fun doInBackground(vararg p0: String?): String? {
            val url: URL?

            try {
                url = URL("https://api.infermedica.com/v2/diagnosis")
                val secureConnection = url.openConnection() as HttpsURLConnection

                secureConnection.setRequestProperty(APP_ID, SECONDARY_INFERMEDICA_APP_ID)
                secureConnection.setRequestProperty(APP_KEY, SECONDARY_INFERMEDICA_APP_KEY)
                secureConnection.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON_VALUE)

                secureConnection.requestMethod = "POST"

                diagnosisJSONObject.put("sex", gender.toLowerCase())
                diagnosisJSONObject.put("age", age)
                diagnosisJSONObject.put("evidence", symptomsArray)

                Log.i(TAG, "diagnosisObject being sent: $diagnosisJSONObject")

                val myData = diagnosisJSONObject.toString()

                secureConnection.doOutput = true

                secureConnection.outputStream.write(myData.toByteArray())

                if (secureConnection.responseCode == 200) {
                    success = 1

                    val responseBody = secureConnection.inputStream

                    val bR = BufferedReader(InputStreamReader(responseBody))
                    var line: String?

                    val responseBuilder = StringBuilder()
                    do {
                        line = bR.readLine() ?: null
                        if (line == null)
                            break
                        else
                            responseBuilder.append(line)

                    } while (true)

                    responseBody.close()

                    val result = JSONObject(responseBuilder.toString())
                    diagnosisObj = getDiagnosis(result)
                    conditions.clear()
                    conditions = diagnosisObj.conditions!!

                } else {
                    val responseBody = secureConnection.inputStream

                    success = 0
                    Log.e("nexus", "failure: an unexpected error occurred: $responseBody")
                }

                secureConnection.disconnect()

            } catch (e: Exception) {
                Snackbar.make(coordinatorLayout, "Sorry, an unexpected error occurred. Please try again later.", Snackbar.LENGTH_LONG).show()
                Log.e(TAG, e.message)
            }

            return null
        }

        override fun onPostExecute(result: String?) {
            toolbar.apply {
                subtitle = ""
            }
            if (success == 1) {
                questionCount++

                if (isDiseaseFound()) {
                    val botMessage = BotMessage(
                            senderId = 2,
                            type = TYPE_TEXT,
                            message = resources.getString(R.string.conclusion))

                    showMessage(botMessage)

                    GetCondition(conditions[0].id, conditions[0].probability).execute()
                } else {
                    if (diagnosisObj.type.equals(TYPE_GROUP_MULTIPLE, true)) {

                        val botMessage = BotMessage(
                                senderId = 2,
                                type = TYPE_TEXT,
                                message = diagnosisObj.extraQuestion
                        )

                        showMessage(botMessage)
                        symptoms = convertDiagnosisToBotMessage(diagnosisObj).symptoms
                        handleMultipleQuestions(symptom = symptoms!![0])
                    } else {
                        showMessage(convertDiagnosisToBotMessage(diagnosisObj))
                    }
                }
            }else {
//                Toast.makeText(this@PersonalDiagnosis, "An error occurred. Please check your connection and try again later", Toast.LENGTH_LONG).show()
                Snackbar.make(coordinatorLayout, "An error occurred. Please check your connection and try again later", Snackbar.LENGTH_SHORT).show()
            }
        }
    }

    inner class LoadSymptoms : AsyncTask<String, String, String>() {

        override fun doInBackground(vararg p0: String?): String? {
            try {
                val jsonObject = JSONObject(loadJsonFromAsset("symptoms.json", this@PersonalDiagnosis))

                val jsonArray = jsonObject.getJSONArray("symptoms")

                symptomsJsonArray = jsonArray

                conditions = ArrayList()

                for (i in (0 until jsonArray.length())) {
                    val jb = jsonArray.getJSONObject(i)

                    val condition = Condition(name = jb.getString("name"), id = jb.getString("id"))

                    conditions.add(condition)
                }

            } catch (e: Exception) {
                Log.e("nexus", e.message)
            }

            return null
        }

        override fun onPostExecute(result: String?) {
            autoCompleteTextView.isEnabled = true
            symptomText.isEnabled = true
            autoCompleteAdapter = ArrayAdapter(
                    this@PersonalDiagnosis,
                    android.R.layout.simple_list_item_1,
                    getConditions(conditions)
            )
            autoCompleteTextView.setAdapter(autoCompleteAdapter)
            initAdapter()
            initListeners()
            hasLoadedSymptoms = true
        }
    }

}
