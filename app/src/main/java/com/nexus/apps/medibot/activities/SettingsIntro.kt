package com.nexus.apps.medibot.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.airbnb.lottie.LottieAnimationView
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.Settings
import com.nexus.apps.medibot.utils.PrefsManager

class SettingsIntro : AppCompatActivity() {

    private lateinit var settingsButton: Button
    private lateinit var animation: LottieAnimationView
    private lateinit var prefsManager: PrefsManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings_intro)

        prefsManager = PrefsManager(this)

        initViews()
        initListeners()
    }

    private fun initViews() {
        settingsButton = findViewById(R.id.btn_settings)
        animation = findViewById(R.id.lottieAnimationView)
    }

    private fun initListeners() {
        settingsButton.setOnClickListener {
            prefsManager.setHasSeenSecurityIntro(true)
            goToSettings()
        }
    }


    private fun goToSettings() {
        val intent = Intent(this, Settings::class.java)
        startActivity(intent)
        finish()
    }
}

