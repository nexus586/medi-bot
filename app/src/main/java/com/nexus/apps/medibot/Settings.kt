package com.nexus.apps.medibot

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.preference.*
import android.view.MenuItem
import com.nexus.apps.medibot.activities.LockScreen
import com.nexus.apps.medibot.utils.PrefsManager


/**
 * A [PreferenceActivity] that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 *
 * See [Android Design: Settings](http://developer.android.com/design/patterns/settings.html)
 * for design guidelines and thej [Settings API Guide](http://developer.android.com/guide/topics/ui/settings.html)
 * for more information on developing a Settings UI.
 */
class Settings : AppCompatPreferenceActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar()
        title = "Settings"

    }


    /**
     * Set up the [android.app.ActionBar], if the API is available.
     */
    private fun setupActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    /**
     * {@inheritDoc}
     */
    override fun onIsMultiPane(): Boolean {
        return isXLargeTablet(this)
    }

    /**
     * {@inheritDoc}
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    override fun onBuildHeaders(target: List<PreferenceActivity.Header>) {
        loadHeadersFromResource(R.xml.pref_headers, target)
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    override fun isValidFragment(fragmentName: String): Boolean {
        return PreferenceFragment::class.java.name == fragmentName
                || ProfileLockPreferenceFragment::class.java.name == fragmentName
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    class ProfileLockPreferenceFragment: PreferenceFragment() {

        private lateinit var currentContext: Activity
        private lateinit var prefsManager: PrefsManager

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.pref_lock_settings)
            setHasOptionsMenu(true)
            currentContext = activity
            prefsManager = PrefsManager(currentContext)
            prefChange(findPreference("profile_lock_switch"))
            //            (Settings::prefChange)(Settings(), findPreference("profile_lock_switch"))
        }

        private fun prefChange(preference: Preference){
            if(isAdded && currentContext != null){
                val preferenceChangeListener = Preference.OnPreferenceChangeListener { preference_, value ->
                    if(preference_ is SwitchPreference && value == true){
                        prefsManager.setWantsToUsePin(true)
                        goToLockScreen()
                    }else {
                        prefsManager.setWantsToUsePin(false)
                        goToLockScreen()
                    }
                    true
                }
                preference.onPreferenceChangeListener = preferenceChangeListener
            }
        }


        private fun goToLockScreen() {
            startActivity(Intent(currentContext, LockScreen::class.java))
        }

        override fun onOptionsItemSelected(item: MenuItem): Boolean {
            val id = item.itemId
            if (id == android.R.id.home) {
                startActivity(Intent(activity, Settings::class.java))
                return true
            }
            return super.onOptionsItemSelected(item)
        }

    }

    companion object {
        /**
         * Helper method to determine if the device has an extra-large screen. For
         * example, 10" tablets are extra-large.
         */
        private fun isXLargeTablet(context: Context): Boolean {
            return context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK >= Configuration.SCREENLAYOUT_SIZE_XLARGE
        }
    }


}
