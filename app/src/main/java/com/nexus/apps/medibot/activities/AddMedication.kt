package com.nexus.apps.medibot.activities

import android.app.*
import android.app.Notification.EXTRA_NOTIFICATION_ID
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SwitchCompat
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.models.Medications
import java.util.*
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.drawable.Icon
import android.os.SystemClock
import android.widget.ImageButton
import com.nexus.apps.medibot.receivers.MedicationTakenReceiver
import com.nexus.apps.medibot.receivers.NotificationPublisher
import com.nexus.apps.medibot.utils.FIREBASE_REFERENCE
import com.nexus.apps.medibot.utils.medicationId
import com.yarolegovich.lovelydialog.LovelyProgressDialog
import java.text.SimpleDateFormat
import kotlin.collections.ArrayList


class AddMedication : AppCompatActivity() {

    // Variable used to identify class during logging
    private val TAG = this::class.java.simpleName

    // Declaration of variables used in the view of the class
    private lateinit var medicationName: EditText
    private lateinit var dosage: EditText
    private lateinit var permanentSwitch: SwitchCompat
    private lateinit var durationFrom: EditText
    private lateinit var durationTo: EditText
    private lateinit var reminderSwitch: SwitchCompat
    private lateinit var reminderTime: EditText
    private lateinit var mFirebaseAuth: FirebaseAuth
    private lateinit var mDatabase: FirebaseDatabase
    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var saveButton: Button
    private lateinit var timerButton: ImageButton
    private lateinit var fromButton: ImageButton
    private lateinit var toButton: ImageButton


    private var isNewMedication: Boolean = false
    private var firebaseId: String = ""
    private var timesTaken: ArrayList<String>? = ArrayList()

    private lateinit var loadingDialog: LovelyProgressDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_medication)
        val intentThatStartedThisActivity = intent
        initViews()
        isNewMedication = intentThatStartedThisActivity.getStringExtra("MEDICATION_ID") == null
        if(isNewMedication) {
            title = "Add Medication"
        }else{
            title = "Edit Medication"
            medicationId = intentThatStartedThisActivity.getStringExtra("MEDICATION_ID")
        }
        initFirebase()
        initListeners()
    }


    // initialization of firebase variables
    private fun initFirebase(){
        mFirebaseAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase.getReference(FIREBASE_REFERENCE)
        if(!isNewMedication){
            retrieveDataFromFirebase(medicationId)
        }

    }

    /**
     * This method is used to update the durationFrom property of the Medication class
     * It displays a date picker dialog that is used to select the durationFrom property
     */
    private fun updateDateFrom() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)
        val datePicker = DatePickerDialog(this, android.R.style.Theme_Holo,DatePickerDialog.OnDateSetListener { _, p1, p2, p3 ->
            val gotDate = Calendar.getInstance()
            gotDate.set(p1, p2, p3)
            durationFrom.setText(String.format("%s/%s/%s", gotDate.get(Calendar.YEAR).toString(), gotDate.get(Calendar.MONTH).toString(), gotDate.get(Calendar.DAY_OF_MONTH).toString()))
        }, year, month, day)
        datePicker.show()
    }


    /**
     * This method is used to update the durationTo property of the Medication class
     * It displays a date picker dialog that is used to select the durationTo property
     */
    private fun updateDateTo() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)
        val datePicker = DatePickerDialog(this, android.R.style.Theme_Holo,DatePickerDialog.OnDateSetListener { _, p1, p2, p3 ->
            val gotDate = Calendar.getInstance()
            gotDate.set(p1, p2, p3)
            durationTo.setText(String.format("%s/%s/%s", gotDate.get(Calendar.YEAR).toString(), gotDate.get(Calendar.MONTH).toString(), gotDate.get(Calendar.DAY_OF_MONTH).toString()))
        }, year, month, day)
        datePicker.show()
    }


    private fun scheduleNotification(notification: Notification, notificationTime: Calendar){
        val someId = "notification-id"
        val someNotification = "notification"
        val notificationIntent : Intent = Intent(this@AddMedication, NotificationPublisher::class.java)
        notificationIntent.putExtra(someId, 1)
        notificationIntent.putExtra(someNotification, notification)
        val pendingIntent: PendingIntent = PendingIntent.getBroadcast(this@AddMedication, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val alarmManager: AlarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val time24h = (24 * 60 * 60 * 1000).toLong()
        alarmManager.set(AlarmManager.RTC, notificationTime.timeInMillis, pendingIntent)
    }

    private fun getNotification(content: String): Notification{
        val CHANNEL_ID = "my_channel_01"
        val someId = "notification-id"
        val builder: Notification.Builder = Notification.Builder(this@AddMedication)
        val takenMedicationIntent = Intent(this@AddMedication,  MedicationTakenReceiver::class.java)
        val takenMedicationPendingIntent : PendingIntent =
                PendingIntent.getBroadcast(this@AddMedication, 0, takenMedicationIntent, 0)
        takenMedicationIntent.putExtra(someId, 1)
        val icon: Icon = Icon.createWithResource(this, android.R.drawable.ic_dialog_info)
//        Compat issues : https://stackoverflow.com/questions/45878921/notificationmanagercompat-on-android-oreo
        val action: Notification.Action =
                Notification.Action.Builder(icon, "I have taken the medication.", takenMedicationPendingIntent).build()


        builder.apply {
            setContentTitle("Please take your medication")
            setContentText(content)
            setSmallIcon(Icon.createWithBitmap(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher_foreground)))
            setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.medication))
            setChannelId(CHANNEL_ID)
            setContentIntent(takenMedicationPendingIntent)
            setActions(action)
        }
        return builder.build()
    }

    // initialization of the various event listeners of the views in the app
    private fun initListeners(){
        permanentSwitch.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked){
                durationFrom.setText("")
                fromButton.isEnabled = false
                durationTo.setText("")
                toButton.isEnabled = false
            }else{
                fromButton.isEnabled = true
                toButton.isEnabled = true
            }
        }


        fromButton.setOnClickListener {
            updateDateFrom()
        }

        toButton.setOnClickListener {
            updateDateTo()
        }

        saveButton.setOnClickListener {
            if (medicationId.isEmpty()){
                medicationId = UUID.randomUUID().toString().split('-')[0]
            }

            if(!permanentSwitch.isChecked) {
                if(durationTo.text.isNullOrEmpty() || durationFrom.text.isNullOrEmpty() || durationFrom.text.toString().equals("No date set", true) || durationTo.text.toString().equals("No date set", true)) {
                    Toast.makeText(this, "Please enter a date or check the permanent button", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
            }


            val durationToHere = if(permanentSwitch.isChecked) {
                null
            }else {
                if(durationTo.text.isNullOrEmpty() || durationTo.text.toString().equals("No date set", true)) {
                    null
                }else {
                    Date(durationTo.text.toString())
                }
            }

            val durationFromHere = if(permanentSwitch.isChecked) {
                null
            }else {
                if(durationFrom.text.isNullOrEmpty() || durationFrom.text.toString().equals("No date set", true)) {
                    null
                }else {
                    Date(durationFrom.text.toString())
                }
            }

            val timeHere = if(reminderSwitch.isChecked) {
                reminderTime.text.toString()

            }else {
                null
            }

            val medication = Medications(
                    medicationName = medicationName.text.toString(),
                    medicationId = medicationId,
                    dosage = dosage.text.toString(),
                    permanent = permanentSwitch.isChecked,
                    durationFrom = durationFromHere,
                    durationTo = durationToHere,
                    reminder = reminderSwitch.isChecked,
                    time = timeHere,
                    timesTaken = timesTaken

            )

            if(title == "Edit Medication"){
                updateMedicationInFirebase(medication)
            }else{
                saveMedicationInFirebase(medication)
            }
        }

        timerButton.setOnClickListener {
            reminderTime.setText("null")
            val c = Calendar.getInstance()
            val hour = c.get(Calendar.HOUR)
            val minute = c.get(Calendar.MINUTE)
            val tpd = TimePickerDialog(this,TimePickerDialog.OnTimeSetListener(function = { view, h, m ->
                val minutePicked = when(m.toString().length == 1){
                    true -> "0$m"
                    false -> m.toString()
                }
                reminderTime.setText(String.format("%s:%s", h.toString(), minutePicked))
                if(!reminderTime.text.toString().isEmpty() && reminderTime.text.toString() != "null"){
                    val notificationTime : Calendar = Calendar.getInstance()
                    notificationTime.timeInMillis = System.currentTimeMillis()
                    notificationTime.set(Calendar.HOUR_OF_DAY, Integer.parseInt(reminderTime.text.toString().split(":")[0]))
                    notificationTime.set(Calendar.MINUTE, Integer.parseInt(reminderTime.text.toString().split(":")[1]))
                    scheduleNotification(getNotification("It's time to take your "+medicationName.text.toString()+ " medication"), notificationTime)
                }

            }),hour,minute,false)

            tpd.show()
        }



    }


    /**
     * @param medicationId: String
     * Retrieves the medication object with the supplied ID from firebase so that it can be updated
     * or viewed
     */
    private fun retrieveDataFromFirebase(medicationId: String){
        Log.i(TAG, "Calling firebase to retrieve medication $medicationId details")
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("medications").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {
                val t = object : GenericTypeIndicator<ArrayList<String>>() {

                }
                for (dataSnapshot in p0.children){
                    if(dataSnapshot.child("medicationId").value.toString() == medicationId){
                        firebaseId = dataSnapshot.key.toString()
                        Log.i(TAG, "Found medication $medicationId in firebase. Pulling details")
                        val medication = Medications(
                                medicationName = dataSnapshot.child("medicationName").value.toString(),
                                medicationId = dataSnapshot.child("medicationId").value.toString(),
                                dosage = dataSnapshot.child("dosage").value.toString(),
                                permanent = dataSnapshot.child("permanent").value.toString().toBoolean(),
                                durationFrom = dataSnapshot.child("durationFrom").getValue((Date::class.java)),
                                durationTo = dataSnapshot.child("durationTo").getValue((Date::class.java)),
                                reminder = dataSnapshot.child("reminder").value.toString().toBoolean(),
                                time = dataSnapshot.child("time").value.toString(),
                                timesTaken = dataSnapshot.child("timesTaken").getValue(t)

                        )
                        if(medication.medicationName != "null"){
                            val pattern = "yyyy/MM/dd"
                            val df = SimpleDateFormat(pattern)

                            val durationFromText = if(medication.durationFrom == null) {
                                "No date set"
                            }else {
                                df.format(medication.durationFrom)
                            }

                            val durationToText = if(medication.durationTo == null) {
                                "No date set"
                            }else {
                                df.format(medication.durationTo)
                            }

                            val reminderTimeText = if(medication.time == null) {
                                "No Time Set"
                            }else {
                                medication.time.toString()
                            }

                            medicationName.setText(medication.medicationName)
                            dosage.setText(medication.dosage)
                            permanentSwitch.isChecked = medication.permanent
                            durationFrom.setText(durationFromText)
                            durationTo.setText(durationToText)
                            reminderSwitch.isChecked = medication.reminder
                            reminderTime.setText(reminderTimeText)
                            timesTaken = medication.timesTaken

                        }
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError) {
                Log.i(TAG, p0.message)
            }
        })

    }


    private fun saveMedicationInFirebase(medication: Medications){
        Log.i(TAG, medication.dosage)
        showSavingDialog("Adding", "Adding your medication. Please wait")
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("medications").push().setValue(medication).addOnCompleteListener { task ->
            if(task.isSuccessful) {
                Toast.makeText(this@AddMedication, "Save Completed Successfully", Toast.LENGTH_LONG).show()
                cancelDialog()
                this@AddMedication.onBackPressed()
            }else {
                Log.e(TAG, task.exception.toString())
                cancelDialog()
                Toast.makeText(this@AddMedication, "Unable to save medication. Please try again later", Toast.LENGTH_LONG).show()
                this.onBackPressed()
            }
        }
    }


    private fun updateMedicationInFirebase(medication: Medications){
        showSavingDialog("Updating", "Updating your medication information. Please wait")
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("medications").child(firebaseId).setValue(medication).addOnCompleteListener { task ->
            if(task.isSuccessful) {
                Toast.makeText(this@AddMedication, "Update Completed Successfully", Toast.LENGTH_LONG).show()
                cancelDialog()
                this@AddMedication.onBackPressed()
            }else {
                Log.e(TAG, task.exception.toString())
                cancelDialog()
                Toast.makeText(this@AddMedication, "Unable to update medication. Please try again later", Toast.LENGTH_LONG).show()
                this.onBackPressed()
            }
        }
    }


    private fun initViews(){
        medicationName = findViewById(R.id.medication_name)
        dosage = findViewById(R.id.dosage)
        permanentSwitch = findViewById(R.id.permanent)
        durationFrom = findViewById(R.id.from)
        durationTo = findViewById(R.id.to)
        reminderSwitch = findViewById(R.id.reminder)
        reminderTime = findViewById(R.id.reminder_time)
        saveButton = findViewById(R.id.save_medication)
        timerButton = findViewById(R.id.timer_button)
        fromButton = findViewById(R.id.fromButton)
        toButton = findViewById(R.id.toButton)

        loadingDialog = LovelyProgressDialog(this)

        val actionBar = supportActionBar
        actionBar?.apply {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_back)
        }

        durationFrom.showSoftInputOnFocus = false
        durationTo.showSoftInputOnFocus = false
        reminderTime.showSoftInputOnFocus = false

    }

    private fun showSavingDialog(title: String, message: String) {
        loadingDialog.setTitle(title)
                .setMessage(message)
                .setCancelable(true)
                .show()
    }

    private fun cancelDialog() {
        loadingDialog.dismiss()
    }
}