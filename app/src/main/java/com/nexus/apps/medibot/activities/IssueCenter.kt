package com.nexus.apps.medibot.activities

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.models.Issue
import com.nexus.apps.medibot.models.IssueInfo
import com.nexus.apps.medibot.utils.API_MEDIC_LIVE_HEALTH_SERVICE
import com.nexus.apps.medibot.utils.getIssueInfoFromResults
import com.nexus.apps.medibot.utils.loadJsonFromAsset
import com.nexus.apps.medibot.utils.staticToken
import com.yarolegovich.lovelydialog.LovelyInfoDialog
import org.json.JSONArray
import org.json.JSONObject

class IssueCenter : AppCompatActivity() {

    //    vals
    private val TAG = this::class.simpleName

    //    vars
    private var issues = ArrayList<Issue>()
    private var issuesJsonArray = JSONArray()
    private var issueID = 0
    private var token: String = ""

    private var hasLoadedSymptoms = false

    //    lateinit vars
    private lateinit var autoComTextView: AutoCompleteTextView
    private lateinit var autoCompleteAdapter: ArrayAdapter<String>
    private lateinit var resultCenter: CardView
    private lateinit var messageCenter: LinearLayout
    private lateinit var intro: RelativeLayout
    private lateinit var loadingCenter: RelativeLayout
    private lateinit var infoCenter: LinearLayout
    private lateinit var issueName: TextInputEditText
    private lateinit var issueProfName: TextInputEditText
    private lateinit var issueShortDesc: TextInputEditText
    private lateinit var issueTreatmentDesc: TextInputEditText
    private lateinit var issueSymptoms: TextInputEditText
    private lateinit var btnSend: Button
    private lateinit var toolbar: Toolbar

    private var issueInfo = IssueInfo()
    private var issueText = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_issue_center)

        initViews()
        if (hasLoadedSymptoms) {
            initListeners()
        } else {
            LoadIssues().execute()
        }

        if(intent.hasExtra("ISSUE_TEXT")){
            issueText = intent.getStringExtra("ISSUE_TEXT")
        }
    }

    private fun initViews() {
        token = intent.getStringExtra("token")
        autoComTextView = findViewById(R.id.autoCompleteTextView)
        autoComTextView.visibility = View.GONE
        autoComTextView.threshold = 1

        toolbar = findViewById(R.id.toolbar)
        toolbar.title = "Issue Center"

        setSupportActionBar(toolbar)
        val actionBar: ActionBar? = supportActionBar
        actionBar?.apply {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_back)
        }
        if(!issueText.isEmpty()) {
            autoComTextView.setText(issueText)
        }

        resultCenter = findViewById(R.id.results_center)
        messageCenter = findViewById(R.id.message_center)
        intro = findViewById(R.id.intro)
        loadingCenter = findViewById(R.id.loading_center)
        infoCenter = findViewById(R.id.info_center)
        issueName = findViewById(R.id.issue_name)
        issueProfName = findViewById(R.id.issue_prof_name)
        issueShortDesc = findViewById(R.id.issue_short_desc)
        issueTreatmentDesc = findViewById(R.id.issue_treat_desc)
        issueSymptoms = findViewById(R.id.issue_symptoms)
        btnSend = findViewById(R.id.btn_send)
    }

    private fun initListeners() {
        btnSend.setOnClickListener {

            if (autoComTextView.text.isNullOrEmpty()) {
                Toast.makeText(this@IssueCenter, "Please select an issue", Toast.LENGTH_LONG).show()
            } else {
                val text = autoComTextView.text
                issueID = getJsonItemIdByName(text.toString())
                hideSoftKeyboard()
                getIssueInfo()
            }
        }
    }


    private fun hideInitialViewsAndShowIncomingViews() {
        messageCenter.visibility = View.GONE
        intro.visibility = View.GONE

        resultCenter.visibility = View.VISIBLE
    }

    private fun hideSoftKeyboard() {
        if (currentFocus != null) {
            val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(currentFocus.windowToken, 0)
        }
    }

    private fun hideIsLoadingInfo() {
        loadingCenter.visibility = View.GONE
        infoCenter.visibility = View.VISIBLE
    }

    private fun getJsonItemIdByName(name: String): Int {
        var id = 0

        for (i in (0 until issuesJsonArray.length())) {
            val jb = issuesJsonArray.getJSONObject(i)

            if (jb.getString("Name").equals(name, true)) {
                id = jb.getInt("ID")
            }
        }

        return id
    }

    fun getIssues(issue: ArrayList<Issue>): ArrayList<String> {
        val issueList = ArrayList<String>()

        for (i in (0 until issue.size)) {
            issueList.add(issue[i].Name!!)
        }

        return issueList
    }

    private fun populateInfoCenter() {
        issueName.setText(issueInfo.name)
        issueProfName.setText(issueInfo.profName)
        issueShortDesc.setText(issueInfo.descriptionShort)
        issueSymptoms.setText(issueInfo.possibleSymptoms)
        issueTreatmentDesc.setText(issueInfo.treatmentDescription)
    }

    private fun getIssueInfo() {
        hideInitialViewsAndShowIncomingViews()

        this.runOnUiThread {
            try {
                val url = StringBuilder(API_MEDIC_LIVE_HEALTH_SERVICE)
                        .append("issues/")
                        .append(issueID)
                        .append("/info")
                        .append("?token=$staticToken&")
                        .append("language=en-gb").toString()

                AndroidNetworking.get(url)
                        .setTag(TAG)
                        .setPriority(Priority.IMMEDIATE)
                        .build()
                        .getAsJSONObject(object : JSONObjectRequestListener {
                            override fun onResponse(response: JSONObject?) {
                                issueInfo = getIssueInfoFromResults(response!!)
                                Log.d(TAG, "Received info = $issueInfo")

                                populateInfoCenter()
                                hideIsLoadingInfo()
                            }

                            override fun onError(anError: ANError?) {
                                Log.e(TAG, "error: ${anError!!.message}")
                                Log.e(TAG, "error: ${anError.errorDetail}")
                                showErrorDialog(anError.message)
                                Log.e(TAG, "error: ${anError.response}")
                            }

                        })

            } catch (ex: Exception) {
                Log.e(TAG, ex.message)
            }
        }
    }

    private fun showErrorDialog(message: String?) {
        LovelyInfoDialog(this)
                .setTitle("Error")
                .setMessage(message)
                .setConfirmButtonText("OK")
                .setTopColor(resources.getColor(android.R.color.holo_red_dark))
                .show()
    }

    inner class LoadIssues : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg p0: String?): String? {

            try {
                // TODO replace anonymous diagnosis with appropriate context
                val jsonObject = JSONObject(loadJsonFromAsset("issues.json", this@IssueCenter))

                val jsonArray = jsonObject.getJSONArray("issues")

                issuesJsonArray = jsonArray

                for (i in (0 until jsonArray.length())) {
                    val jb = jsonArray.getJSONObject(i)

                    val issue = Issue(
                            Name = jb.getString("Name"),
                            Id = jb.getInt("ID")
                    )

                    issues.add(issue)
                }

                Log.d(TAG, "issueArray: $issues")

            } catch (ex: Exception) {
                Log.e(TAG, ex.message)
            }

            return null
        }

        override fun onPostExecute(result: String?) {
            hasLoadedSymptoms = true
            initListeners()
            autoCompleteAdapter = ArrayAdapter(
                    this@IssueCenter,
                    android.R.layout.simple_list_item_1,
                    getIssues(issues)
            )
            autoComTextView.setAdapter(autoCompleteAdapter)
            autoComTextView.visibility = View.VISIBLE
            autoComTextView.setText(issueText)
        }

    }

}
