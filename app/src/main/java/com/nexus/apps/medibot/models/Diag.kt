package com.nexus.apps.medibot.models

data class Diag(
        var issue: Issue? = null,
        var specialization: ArrayList<Specialization>? = null
)