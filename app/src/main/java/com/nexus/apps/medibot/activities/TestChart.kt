package com.nexus.apps.medibot.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ProgressBar
import android.widget.Toast

import com.anychart.AnyChart
import com.anychart.AnyChartView
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.chart.common.listener.Event
import com.anychart.chart.common.listener.ListenersInterface
import com.anychart.charts.Cartesian
import com.anychart.core.cartesian.series.Column
import com.anychart.enums.Anchor
import com.anychart.enums.HoverMode
import com.anychart.enums.Position
import com.anychart.enums.TooltipPositionMode
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.dialogs.TimesTakenDialog
import com.nexus.apps.medibot.models.Medications
import com.nexus.apps.medibot.utils.FIREBASE_REFERENCE

import java.util.ArrayList

class TestChart : AppCompatActivity() {

    private lateinit var mFirebaseAuth: FirebaseAuth
    private lateinit var mFirebaseDatabase: FirebaseDatabase
    private lateinit var mDatabaseReference: DatabaseReference

    private var chartData = ArrayList<DataEntry>()

    private lateinit var anyChartView: AnyChartView
    private lateinit var progressBar: ProgressBar
    private lateinit var fab: FloatingActionButton


    private val TAG = this::class.java.toString()

    private var medicationList = ArrayList<Medications>()

    private fun initFirebase() {
        mFirebaseAuth = FirebaseAuth.getInstance()
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase.getReference(FIREBASE_REFERENCE)
    }

    private fun loadMedicationFromFirebase() {
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("medications").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.w(TAG, "loadPost:onCancelled", p0.toException())
            }

            override fun onDataChange(p0: DataSnapshot) {
                val t = object : GenericTypeIndicator<ArrayList<String>>() {

                }

                for (data in p0.children) {

                    val temp = Medications(
                            medicationName = data.child("medicationName").value.toString(),
                            medicationId = data.child("medicationId").value.toString(),
                            dosage = data.child("dosage").value.toString(),
                            timesTaken = data.child("timesTaken").getValue(t)
                    )
                    if(temp.medicationName != "null"){
                        if(!medicationList.contains(temp)){
                            medicationList.add(temp)
                        }
                    }
                }

                Log.d(TAG, "medication size = ${medicationList.size}")

                for(data in (0 until medicationList.size)) {
                    val timesTaken = if(medicationList[data].timesTaken == null) {
                      0
                    } else {
                        medicationList[data].timesTaken!!.size
                    }

                    chartData.add(CustomDataEntry(medicationList[data].medicationName, timesTaken, medicationList[data].medicationId))
                }

                val cartesian = AnyChart.column()


                cartesian.setOnClickListener(object: ListenersInterface.OnClickListener(Array(3) {"x"; "value"; "medicationId"}){
                    override fun onClick(event: Event?) {
                        Log.d(TAG, "eventData is: ${event!!.data}")

                        Log.d(TAG, "item Clicked id = ${event.data["medicationId"]}")

                        loadMedicationById(event.data["medicationId"]!!)
                    }

                })

                val column = cartesian.column(chartData)

                column.tooltip()
                        .titleFormat("{%X}")
                        .position(Position.CENTER_BOTTOM)
                        .anchor(Anchor.CENTER_BOTTOM)
                        .offsetX(0.0)
                        .offsetY(5.0)
                        .format("{%Value}{groupsSeparator: }")

                cartesian.animation(true)
                cartesian.title("Medication Chart")

                cartesian.yScale().minimum(0.0)

                cartesian.yAxis(0).labels().format("{%Value}{groupsSeparator: }")

                cartesian.tooltip().positionMode(TooltipPositionMode.POINT)
                cartesian.interactivity().hoverMode(HoverMode.BY_X)

                cartesian.xAxis(0).title("Medication")
                cartesian.yAxis(0).title("Times Taken")

                anyChartView.setChart(cartesian)

            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_chart)

        initFirebase()

        anyChartView = findViewById(R.id.any_chart_view)
        progressBar = findViewById(R.id.progress_bar)
        anyChartView.setProgressBar(progressBar)
        fab = findViewById(R.id.fab_medication)

        fab.setOnClickListener {
            startActivity(Intent(this, Medication::class.java))
        }

        loadMedicationFromFirebase()
        title = "Medication Chart"

        actionBar?.apply {
            setHomeAsUpIndicator(R.drawable.ic_back)
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
    }


    private inner class CustomDataEntry(medication: String, timesTaken: Number, medicationId: String) : ValueDataEntry(medication, timesTaken) {
        init {
            setValue("medicationId", medicationId)
        }
    }

    private fun loadMedicationById(medicationId: String) {
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("medications").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {

                val t = object : GenericTypeIndicator<ArrayList<String>>() {

                }

                for (data in p0.children) {
                    if(data.child("medicationId").value!!.equals(medicationId)) {
                        if(data.child("timesTaken").getValue(t) != null) {
                            val timesTakenArrayList = data.child("timesTaken").getValue(t)
                            var timesTakenString = "Date Taken\t\t\t\t\t \t\tTime Taken "
                            timesTakenArrayList!!.forEach {
                                timesTakenString = timesTakenString + "\n" + it.split(" ")[0]  +"\t\t\t\t\t\t\t\t\t" + it.split(" ")[1]
                            }
                            TimesTakenDialog(this@TestChart,  timesTakenString).show()
                        }else {
                            Toast.makeText(this@TestChart, "No data to be shown yet", Toast.LENGTH_SHORT).show()
                        }

                    }
                }

            }

        })
    }
}