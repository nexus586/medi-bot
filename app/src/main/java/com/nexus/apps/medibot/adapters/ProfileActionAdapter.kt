package com.nexus.apps.medibot.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.activities.*
import com.nexus.apps.medibot.models.ProfileAction


class ProfileActionAdapter(private var context: Context, private var profileAction: ArrayList<ProfileAction>) : BaseAdapter() {

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
        var view1 = view
        if (view1 == null) {
            view1 = LayoutInflater.from(context).inflate(R.layout.profile_item, viewGroup, false)
        }

        val action = this.getItem(i) as ProfileAction

        val img: ImageView = view1!!.findViewById(R.id.profile_action_image)
        val mA: TextView = view1.findViewById(R.id.profile_action_text)

        mA.text = action.name
        img.setImageResource(action.drawable)

        view1.setOnClickListener {

            when {
                action.name.equals("Basic Info", true) -> {
                    val intent = Intent(context, BasicInfo::class.java)
                    context.startActivity(intent)
                }
                action.name.equals("Health Info", true) -> {
                    // TODO: complete activity to take health info
//                                    Toast.makeText(context, "Activity not yet ready", Toast.LENGTH_SHORT).show()
                    val intent = Intent(context, HealthInfo::class.java)
                    context.startActivity(intent)
                }
                action.name.equals("Medication", true) -> {
                    //TODO: create view for loading medication and creating medication
//                                    Toast.makeText(context, "Activity not yet ready", Toast.LENGTH_SHORT).show()
                                    val intent  = Intent(context, Medication::class.java)
                                    context.startActivity(intent)
                }
                action.name.equals("Allergies", true) -> {
                    val intent = Intent(context, Allergies::class.java)
                    context.startActivity(intent)
                }
                action.name.equals("History", true) -> {
//                                    Toast.makeText(context, "Activity not yet ready", Toast.LENGTH_SHORT).show()
                    val intent = Intent(context, History::class.java)
                    context.startActivity(intent)
                }

                action.name.equals("Statistics", true) -> {
                    context.startActivity(Intent(context, Statistics::class.java))
                }
            }
        }

        return view1
    }

    override fun getItem(p0: Int): Any = profileAction[p0]

    override fun getItemId(p0: Int): Long = p0.toLong()

    override fun getCount(): Int = profileAction.size
}