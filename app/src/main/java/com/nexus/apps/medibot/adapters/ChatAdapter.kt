package com.nexus.apps.medibot.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.models.DiagnosisChat
import kotlinx.android.synthetic.main.chat_item.view.*

class ChatAdapter(private var context: Context, val clickListener: (DiagnosisChat) -> Unit, private var diagnosisChats: ArrayList<DiagnosisChat>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.chat_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = diagnosisChats.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(diagnosisChats[position], clickListener)

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(diagnosisChat: DiagnosisChat, clickListener: (DiagnosisChat) -> Unit) {
            itemView.chat_time.text = diagnosisChat.date
            itemView.diagnosis_message.text = diagnosisChat.recommendation
            itemView.diagnosis_chat_id.text = diagnosisChat.id
            itemView.setOnClickListener { clickListener(diagnosisChat) }
        }
    }
}