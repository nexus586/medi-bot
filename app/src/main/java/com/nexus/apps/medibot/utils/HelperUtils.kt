package com.nexus.apps.medibot.utils

import android.content.Context
import android.util.Log


fun loadJsonFromAsset(filename: String, context: Context): String? {
    val json: String

    try {
        val inputStream = context.assets.open(filename)

        val size = inputStream.available()

        val buffer = ByteArray(size)

        inputStream.read(buffer)

        inputStream.close()

        json = String(buffer, charset("UTF-8"))
    } catch (ex: Exception) {
        Log.e("loadJsonFromAsset", ex.message)
        ex.printStackTrace()
        return null
    }

    return json
}
