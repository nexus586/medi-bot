package com.nexus.apps.medibot.models

import java.util.*
import kotlin.collections.ArrayList

data class Medications(
        var medicationName: String,
        var medicationId: String,
        var dosage: String,
        var durationFrom: Date? = null,
        var durationTo: Date? = null,
        var permanent: Boolean = false,
        var reminder: Boolean = false,
        var time: String? = null,
        var timesTaken: ArrayList<String>? = ArrayList()
)