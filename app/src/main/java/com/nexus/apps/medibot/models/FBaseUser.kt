package com.nexus.apps.medibot.models

import java.util.*

data class FBaseUser(
//        @PrimaryKey
        val userID: String?,
        val username: String?,
        val first_name: String?,
        val last_name: String?,
        val email: String?,
        val phoneNumber: String?,
//        @Nullable
        var gender: String?,
//        @Nullable
        var dateOfBirth: Date?
) {
    constructor() : this(
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            null)
}