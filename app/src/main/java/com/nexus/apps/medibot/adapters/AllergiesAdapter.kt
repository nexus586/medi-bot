package com.nexus.apps.medibot.adapters

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.activities.Allergies
import com.nexus.apps.medibot.models.Allergy

class AllergiesAdapter(private var context: Context, private var allergies: ArrayList<Allergy>) : RecyclerView.Adapter<AllergiesAdapter.ViewHolder>() {
    private val TAG = this::class.java.simpleName
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllergiesAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.allergies_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return this.allergies.size
    }

    override fun onBindViewHolder(holder: AllergiesAdapter.ViewHolder, position: Int) {
        val allergy = allergies[position]
        holder.allergy.text = allergy.allergy
        holder.allergyContainer.setOnClickListener {
            if(holder.buttonsContainer.visibility == View.VISIBLE) {
                holder.buttonsContainer.visibility = View.GONE
            }else {
                holder.buttonsContainer.visibility = View.VISIBLE
            }

            holder.allergyContainer.cardElevation = 6f
        }
        holder.btnEdit.setOnClickListener {
            (context as Allergies).showEditDialog(allergy.allergy, allergy.id)
        }

        holder.btnDelete.setOnClickListener {
            (context as Allergies).showDeleteDialog(allergy.allergy, allergy.id)
        }


    }

    fun add(allergy: Allergy) {
        allergies.add(allergy)
    }

    fun add(allergies: ArrayList<Allergy>) {
        for (allergy in allergies) {
            allergies.add(allergy)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var allergy: TextView = itemView.findViewById(R.id.allergy)
        var allergyContainer: CardView = itemView.findViewById(R.id.allergy_container)
        var buttonsContainer: LinearLayout = itemView.findViewById(R.id.options_container)
        var btnEdit: Button = itemView.findViewById(R.id.btn_edit)
        var btnDelete: Button = itemView.findViewById(R.id.btn_delete)
    }
}