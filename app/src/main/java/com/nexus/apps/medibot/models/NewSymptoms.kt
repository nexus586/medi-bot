package com.nexus.apps.medibot.models

data class NewSymptoms(
        var ID: Int? = 0,
        var Name: String? = ""
)