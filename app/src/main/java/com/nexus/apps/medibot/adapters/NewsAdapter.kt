package com.nexus.apps.medibot.adapters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.support.constraint.ConstraintLayout
import android.support.customtabs.CustomTabsIntent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.models.Article
import com.nexus.apps.medibot.utils.GlideApp

class NewsAdapter(private var context: Context, private var articles: ArrayList<Article>) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = articles.size

    override fun onBindViewHolder(holder: NewsAdapter.ViewHolder, position: Int) {
        val article = articles[position]

        holder.articleDesc.text = article.description
        holder.articleTitle.text = article.title
        GlideApp
                .with(context)
                .load(article.imgUrl)
                .placeholder(R.drawable.news1)
                .fallback(R.drawable.news1)
                .apply(RequestOptions.bitmapTransform(RoundedCorners(16)))
                .into(holder.imageView)
        holder.source.text = article.source.name
        holder.container.setOnClickListener {
            val url = article.url


            val tabBuilder = CustomTabsIntent.Builder()
            tabBuilder.setToolbarColor(context.resources.getColor(R.color.colorPrimary))
            val tabsIntent = tabBuilder
                    .setShowTitle(true)
                    .enableUrlBarHiding()
                    .addDefaultShareMenuItem()
                    .build()
            tabsIntent.launchUrl(context, Uri.parse(url))
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val container: ConstraintLayout = itemView.findViewById(R.id.heading_container)
        val articleTitle: TextView = itemView.findViewById(R.id.article_title)
        val articleDesc: TextView = itemView.findViewById(R.id.article_description)
        val imageView: ImageView = itemView.findViewById(R.id.news_image)
        val source: TextView = itemView.findViewById(R.id.source)
    }

}