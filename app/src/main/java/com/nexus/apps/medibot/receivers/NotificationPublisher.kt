package com.nexus.apps.medibot.receivers

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color

class NotificationPublisher : BroadcastReceiver(){

    public val NOTIFICATION_ID: String = "notification-id"
    public val NOTIFICATION: String = "notification"
    public val notificationChannelId = "my_channel_01"
    val channelName: CharSequence = "some_channel_name"
    val channelDescription = "my channel description"
    val importance: Int = NotificationManager.IMPORTANCE_HIGH



    override fun onReceive(context: Context?, intent: Intent?) {
        val notificationManager: NotificationManager = context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val mChannel = NotificationChannel(notificationChannelId, channelName, importance)
        mChannel.apply {
            description = channelDescription
            enableLights(true)
            lightColor = Color.RED
            vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
        }

        notificationManager.createNotificationChannel(mChannel)

        val notification: Notification = intent!!.getParcelableExtra(NOTIFICATION)
        val id: Int = intent.getIntExtra(NOTIFICATION_ID, 0)
        notificationManager.notify(id, notification)
    }

}