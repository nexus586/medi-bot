package com.nexus.apps.medibot.activities

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.models.FBaseUser
import com.nexus.apps.medibot.utils.FIREBASE_REFERENCE
import com.nexus.apps.medibot.utils.PrefsManager
import com.yarolegovich.lovelydialog.LovelyProgressDialog
import java.text.SimpleDateFormat
import java.util.*

class SignUp : AppCompatActivity() {

    private val TAG = SignUp::class.simpleName

    private lateinit var firstName: TextInputEditText
    private lateinit var lastName: TextInputEditText
    private lateinit var email: TextInputEditText
    private lateinit var phone: TextInputEditText
    private lateinit var password: TextInputEditText
    private lateinit var confPassword: TextInputEditText
    private lateinit var username: TextInputEditText
    private lateinit var textDateOfBirth: TextInputEditText
    private lateinit var textGender: TextInputEditText

    private lateinit var genderRadioGroup: RadioGroup
    private lateinit var maleRadioButton: RadioButton
    private lateinit var femaleRadioButton: RadioButton

    private lateinit var btnSignUp: Button


    private lateinit var prefsManager: PrefsManager

    private lateinit var mAuth: FirebaseAuth
    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var mFirebaseDatabase: FirebaseDatabase
    private lateinit var userObj: FBaseUser

    private lateinit var firstNamelayout: TextInputLayout
    private lateinit var lastNamelayout: TextInputLayout
    private lateinit var emaillayout: TextInputLayout
    private lateinit var usernamelayout: TextInputLayout
    private lateinit var passwordlayout: TextInputLayout
    private lateinit var genderlayout: TextInputLayout
    private lateinit var phonelayout: TextInputLayout
    private lateinit var confPasslayout: TextInputLayout
    private lateinit var doblayout: TextInputLayout

    private var date: Date = Date()

    private lateinit var pDialog: LovelyProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        initFireBase()
        prefsManager = PrefsManager(this)

        initViews()
        initListeners()
    }

    private fun initViews() {
        firstName = findViewById(R.id.first_name)
        lastName = findViewById(R.id.last_name)
        email = findViewById(R.id.tiet_email)
        phone = findViewById(R.id.tiet_phone)
        password = findViewById(R.id.tiet_password)
        confPassword = findViewById(R.id.tiet_conf_password)
        btnSignUp = findViewById(R.id.btn_sign_up)
        username = findViewById(R.id.tiet_username)

        firstNamelayout = findViewById(R.id.firstname_layout)
        lastNamelayout = findViewById(R.id.lastname_layout)
        emaillayout = findViewById(R.id.email_layout)
        usernamelayout = findViewById(R.id.username_layout)
        genderlayout = findViewById(R.id.gender_holder)
        phonelayout = findViewById(R.id.phone_layout)
        confPasslayout = findViewById(R.id.confpass_layout)
        doblayout = findViewById(R.id.dob_layout)
        passwordlayout = findViewById(R.id.password_layout)


        textGender = findViewById(R.id.tv_gender)
        textDateOfBirth = findViewById(R.id.edt_dob)
        genderRadioGroup = findViewById(R.id.gender_group)
        maleRadioButton = findViewById(R.id.gender_male)
        femaleRadioButton = findViewById(R.id.gender_female)
    }

    private fun initFireBase() {
        mAuth = FirebaseAuth.getInstance()
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase.getReference(FIREBASE_REFERENCE)
    }

    private fun initListeners() {
        btnSignUp.setOnClickListener {
            validate()
        }

        textDateOfBirth.setOnClickListener {
            showDatePickerDialog()
        }

        genderRadioGroup.setOnCheckedChangeListener { _, p1 ->
            when (p1) {
                R.id.gender_male -> {
                    textGender.setText(resources.getString(R.string.male))
                }
                R.id.gender_female -> {
                    textGender.setText(resources.getString(R.string.female))
                }
            }
        }
    }

    private fun initUser(uid: String) {
        userObj = FBaseUser(
                userID = uid,
                username = username.text.toString(),
                first_name = firstName.text.toString(),
                last_name = lastName.text.toString(),
                email = email.text.toString(),
                phoneNumber = phone.text.toString(),
                gender = textGender.text.toString(), dateOfBirth = date
        )
    }

    private fun signUpWithFirebase(email: String, password: String) {
        showProgressDialog()
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Log.d(TAG, "createUserWithEmail:success")
                        initUser(mAuth.currentUser!!.uid)
                        val user = mAuth.currentUser!!
                        updateUserDetailsOnFirebase(user)
                        addUserDetailsToFirebase(userObj)
                    } else {
                        Log.w(TAG, "createUserWithEmail:failure", task.exception)
                        showErrorDialog(task.exception?.message!!)
                        pDialog.dismiss()
                    }
                }
    }


    private fun showErrorDialog(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error")
                .setMessage(message)
                .setCancelable(true)
                .show()
    }


    private fun showProgressDialog() {
        pDialog = LovelyProgressDialog(this)
        pDialog.setMessage("Signing Up")
        pDialog.setCancelable(false)
        pDialog.setTopColor(resources.getColor(android.R.color.holo_blue_bright))
                .setIcon(resources.getDrawable(R.drawable.ic_info_outline))
        pDialog.show()
    }


    private fun addUserDetailsToFirebase(user: FBaseUser) {
        mDatabaseReference.child("users").child(user.userID.toString()).child("basic_information").setValue(user).addOnCompleteListener { task ->

            if (task.isSuccessful) {
                Toast.makeText(this@SignUp, "Signed up successfully", Toast.LENGTH_LONG).show()
                pDialog.dismiss()
                Handler().postDelayed({
                    if(prefsManager.getHasSeenSecurityIntro()) {
                        goToHome()
                    }else {
                        goToSecurityIntro()
                    }

                }, 1000)
            } else {
                pDialog.dismiss()
                showErrorDialog(task.exception?.message!!)
            }
        }
    }

    private fun goToHome() {
        val intent = Intent(this, Home::class.java)
        startActivity(intent)
        finish()
    }


    private fun validate() {
        clearErrors()

        if (username.text.isNullOrEmpty()
                || firstName.text.isNullOrEmpty()
                || lastName.text.isNullOrEmpty()
                || phone.text.isNullOrEmpty()
                || email.text.isNullOrEmpty()
                || textGender.text.isNullOrEmpty()
                || textDateOfBirth.text.isNullOrEmpty()
                || password.text.isNullOrEmpty()
                || confPassword.text.isNullOrEmpty()) {
            showErrorDialog("Required fields are empty or missing")
        } else if (password.text!!.length < 6) {
            passwordlayout.error = "Password should not be less than 6 characters"
        } else if(!password.text.toString().equals(confPassword.text.toString())) {
            passwordlayout.error = "Passwords do not match"
            confPasslayout.error = "Passwords do not match"
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()) {
            emaillayout.error = "Please enter a valid email"
        } else {
            signUpWithFirebase(email = email.text.toString(), password = password.text.toString())
        }
    }

    private fun updateUserDetailsOnFirebase(user: FirebaseUser) {
        val displayName = userObj.first_name + " " + userObj.last_name
        val profileChangeRequest: UserProfileChangeRequest = UserProfileChangeRequest.Builder().setDisplayName(displayName).build()
        user.updateProfile(profileChangeRequest)
    }

    private fun showDatePickerDialog() {
        val simpleFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)

        val minDate = simpleFormat.parse("1960-01-01")

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)

        val datePicker = DatePickerDialog(this, android.R.style.Theme_Holo, DatePickerDialog.OnDateSetListener { _, p1, p2, p3 ->
            val gotDate = Calendar.getInstance()
            gotDate.set(p1, p2, p3)
            textDateOfBirth.setText(String.format("%s/%s/%s", gotDate.get(Calendar.YEAR).toString(), gotDate.get(Calendar.MONTH).toString(), gotDate.get(Calendar.DAY_OF_MONTH).toString()))
            date = Date(gotDate.timeInMillis)
        }, year, month, day)
        datePicker.datePicker.minDate = minDate.time
        datePicker.datePicker.maxDate = Date().time
        datePicker.show()
    }


    private fun goToSecurityIntro() {
        startActivity(Intent(this, SettingsIntro::class.java))
        finish()
    }

    private fun clearErrors() {
        username.error = null
        firstName.error = null
        lastName.error = null
        phone.error = null
        email.error = null
        textGender.error = null
        textDateOfBirth.error = null
        password.error = null
        confPassword.error = null
    }

}
