package com.nexus.apps.medibot.models


data class RiskFactor(
        var key: String = "",
        var value: String = "NOT_ANSWERED",
        var description: String = ""
)