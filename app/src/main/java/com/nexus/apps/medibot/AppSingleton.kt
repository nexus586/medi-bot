package com.nexus.apps.medibot

import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.google.firebase.database.FirebaseDatabase


class AppSingleton() : MultiDexApplication() {

    constructor(context: Context) : this()

    private val TAG = AppSingleton::class.java.simpleName

    private lateinit var instance: AppSingleton
    private lateinit var database: FirebaseDatabase

    override fun onCreate() {
        super.onCreate()
        instance = this

        database = FirebaseDatabase.getInstance()
        database.setPersistenceEnabled(true)

        Log.d(TAG, "AppSingleton has been called")

        AndroidNetworking.initialize(applicationContext)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

//    private fun getInstance(): AppSingleton = instance

    companion object {
        @Volatile
        private var INSTANCE: AppSingleton? = null

        fun getInstance(context: Context) =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: AppSingleton(context).also {
                        INSTANCE = it
                    }
                }
    }

}