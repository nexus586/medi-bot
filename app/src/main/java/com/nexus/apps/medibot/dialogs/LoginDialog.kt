package com.nexus.apps.medibot.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.TextView
import com.nexus.apps.medibot.R

class LoginDialog(context: Context, message: String) : Dialog(context) {

    private lateinit var text: TextView
    private var string: String = message

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_dialog)

        initView()
    }

    private fun initView() {
        text = findViewById(R.id.tvMessage)
        text.text = string
        setCancelable(true)
    }
}