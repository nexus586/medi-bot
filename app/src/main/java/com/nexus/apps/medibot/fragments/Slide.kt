package com.nexus.apps.medibot.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.nexus.apps.medibot.R

class Slide : Fragment() {

    private var ARG_POSITION = "position"
    private var position: Int = 0

    private var icons: Array<Int> = arrayOf(
            R.drawable.diagnosis,
            R.drawable.tips,
            R.drawable.doctor
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            position = it.getInt(ARG_POSITION)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_slide, container, false)

        val title: TextView = view.findViewById(R.id.tv_title)
        val desc: TextView = view.findViewById(R.id.tv_desc)
        val imgView: ImageView = view.findViewById(R.id.img_slide)

        val titles: Array<String> = activity!!.resources.getStringArray(R.array.titles)
        val descriptions: Array<String> = activity!!.resources.getStringArray(R.array.content_desc)

        title.text = titles[position]
        desc.text = descriptions[position]
        imgView.setImageResource(icons[position])

        return view
    }

    companion object {

        @JvmStatic
        fun newInstance(pos: Int) =
                Slide().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_POSITION, pos)
                    }
                }
    }
}
