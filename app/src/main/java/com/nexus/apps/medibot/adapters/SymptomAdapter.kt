package com.nexus.apps.medibot.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.nexus.apps.medibot.R


class SymptomAdapter(private var context: Context, private var symptoms: ArrayList<String>) : RecyclerView.Adapter<SymptomAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewHolder = LayoutInflater.from(parent.context).inflate(R.layout.symptom_item, parent, false)
        return ViewHolder(viewHolder)
    }

    fun add(symptom: String) {
        symptoms.add(symptom)
    }

    fun add(symptoms: ArrayList<String>) {
        for (symptom in symptoms) {
            symptoms.add(symptom)
        }
    }

    override fun getItemCount() = symptoms.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val symptom = symptoms[position]
        holder.symptom.text = symptom
        holder.remove.visibility = View.VISIBLE
        holder.remove.setOnClickListener {
            symptoms.remove(symptom)
            notifyDataSetChanged()
        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var symptom: TextView = itemView.findViewById(R.id.symptom)
        var remove: ImageView = itemView.findViewById(R.id.remove_button)
    }
}