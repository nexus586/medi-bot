package com.nexus.apps.medibot.models

data class Symptoms(
        var id: String = "",
        var label: String = "",
        var name: String = ""
)