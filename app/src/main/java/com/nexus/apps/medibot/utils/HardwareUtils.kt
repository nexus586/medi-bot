package com.nexus.apps.medibot.utils

import android.content.Context
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat

fun hasFingerprintHardware(context: Context): Boolean {

    val fingerPrintCompat = FingerprintManagerCompat.from(context)

    if(fingerPrintCompat.isHardwareDetected) {
        return true
    }

    return false
}