package com.nexus.apps.medibot.models

data class DiagnosisObj(
        var question: String = "",
        var type: String = "",
        var extraQuestion: String = "",
        var conditions: ArrayList<Condition>? = null,
        var symptoms: ArrayList<Symptoms>? = null
)