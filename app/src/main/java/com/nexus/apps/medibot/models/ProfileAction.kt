package com.nexus.apps.medibot.models

data class ProfileAction(
        val name: String,
        val drawable: Int
)