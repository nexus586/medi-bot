package com.nexus.apps.medibot.receivers

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.nexus.apps.medibot.models.Medications
import com.nexus.apps.medibot.utils.FIREBASE_REFERENCE
import com.nexus.apps.medibot.utils.medicationId
import java.text.SimpleDateFormat
import java.util.*


class MedicationTakenReceiver : BroadcastReceiver(){
    private val TAG = this::class.java.simpleName
    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var mFirebaseAuth: FirebaseAuth
    private lateinit var mDatabase: FirebaseDatabase
    private var firebaseId: String = ""
//    private var medicationId : String = ""

    override fun onReceive(context: Context?, intent: Intent?) {
        mFirebaseAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase.getReference(FIREBASE_REFERENCE)
        addTimeTakenToFirebase()
        val NOTIFICATION_ID: String = "notification-id"
        val notificationManager: NotificationManager = context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val id: Int = intent!!.getIntExtra(NOTIFICATION_ID, 1)
        notificationManager.cancel(id)
    }

    private fun addTimeTakenToFirebase(){
        getMedicationFromFirebase(medicationId)
    }

    private fun getMedicationFromFirebase(medicationFirebaseId: String){
        Log.i(TAG, "Calling firebase to retrieve medication: $medicationFirebaseId")
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("medications").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", p0.toException())
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val t = object : GenericTypeIndicator<ArrayList<String>>() {

                }
                for (postSnapshot in dataSnapshot.children) {
                    if (postSnapshot.child("medicationId").value.toString().trim() == medicationFirebaseId) {
                        firebaseId = postSnapshot.key.toString()
                        Log.i(TAG, "MedicationID matched with ID in firebase. Retrieving medication")
                        val temp  = Medications(
                                medicationName = postSnapshot.child("medicationName").value.toString(),
                                medicationId = postSnapshot.child("medicationId").value.toString(),
                                dosage = postSnapshot.child("dosage").value.toString(),
                                permanent = postSnapshot.child("permanent").value.toString().toBoolean(),
                                durationFrom = postSnapshot.child("durationFrom").getValue((Date::class.java)),
                                durationTo = postSnapshot.child("durationTo").getValue((Date::class.java)),
                                reminder = postSnapshot.child("reminder").value.toString().toBoolean(),
                                time = postSnapshot.child("time").value.toString(),
                                timesTaken = postSnapshot.child("timesTaken").getValue(t)
                        )
                        val sdf = SimpleDateFormat("dd/M/yyy hh:mm:ss")
                        val currentDate : String = sdf.format(Date())
                        if(temp.timesTaken == null){
                            temp.timesTaken = ArrayList()
                            temp.timesTaken?.add(currentDate)
                        }else{
                            temp.timesTaken?.add(currentDate)
                        }
                        updateMedicationInFirebase(temp)
                    }
                }
            }
        })
    }

    private fun updateMedicationInFirebase(medication: Medications?){
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("medications").child(firebaseId).setValue(medication).addOnCompleteListener { task ->
            if(task.isSuccessful) {
//                Toast.makeText(this@MedicationTakenService, "Update Completed Successfully", Toast.LENGTH_LONG).show()
            }else {
                Log.e(TAG, task.exception.toString())
//                Toast.makeText(this@MedicationTakenService, "Unable to update medication. Please try again later", Toast.LENGTH_LONG).show()
            }
        }
    }

}
