package com.nexus.apps.medibot.utils

import android.util.Base64
import java.io.UnsupportedEncodingException
import java.security.NoSuchAlgorithmException
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec


fun convertToMD5(password: String, url: String): String {
    val keySpec = SecretKeySpec(password.toByteArray(), "HmacMD5")

    var computedHashString = ""

    try {
        val mac = Mac.getInstance("HmacMD5")
        mac.init(keySpec)
        val result = mac.doFinal(url.toByteArray())


        computedHashString = Base64.encodeToString(result, Base64.NO_WRAP)

    } catch (ex: UnsupportedEncodingException) {
        ex.printStackTrace()
    } catch (ex: NoSuchAlgorithmException) {
        ex.printStackTrace()
    }

    return computedHashString
}