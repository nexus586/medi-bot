package com.nexus.apps.medibot.fragments


import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.agrawalsuneet.loaderspack.loaders.PulseLoader
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.activities.IssueCenter
import com.nexus.apps.medibot.activities.SpecialistCenter
import com.nexus.apps.medibot.utils.*
import com.yarolegovich.lovelydialog.LovelyStandardDialog
import org.json.JSONObject


/**
 * A simple [Fragment] subclass.
 *
 */
class MedicalCenter : Fragment() {

    private val TAG = this::class.java.simpleName

    private lateinit var issueCenter: CardView
    private lateinit var specialistCenter: CardView
    private lateinit var imageView: ImageView
    private lateinit var pulseLoader: PulseLoader
    private lateinit var accessText: TextView

    private lateinit var notConnectedView: RelativeLayout

    private lateinit var retryButton: Button

    private var token = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_medical_center, container, false)
        initViews(view)


        if (!isConnectedToInternet(view.context)) {
            showInternetDialog(view)
            hideInitialViews(view)
        } else {
            if (!staticToken.isEmpty()) {
                initListeners()
                showViews()
            } else {
                hideViews()
                getToken(view)
            }
        }

        return view
    }

    private fun hideInitialViews(view: View?) {
        pulseLoader.visibility = View.GONE
        accessText.visibility = View.GONE

        issueCenter.visibility = View.GONE
        specialistCenter.visibility = View.GONE
        imageView.visibility = View.GONE
    }


    private fun initViews(view: View) {
        issueCenter = view.findViewById(R.id.issue_center_card_item)
        specialistCenter = view.findViewById(R.id.specialist_center_card_item)
        pulseLoader = view.findViewById(R.id.pulse_loader)
        accessText = view.findViewById(R.id.access_text)
        imageView = view.findViewById(R.id.med_center_img)

        notConnectedView = view.findViewById(R.id.not_connected_view)
        retryButton = view.findViewById(R.id.retry_button)

    }

    private fun initListeners() {
        issueCenter.setOnClickListener {
            val intent = Intent(view!!.context, IssueCenter::class.java)
            intent.putExtra("token", token)
            startActivity(intent)
        }

        specialistCenter.setOnClickListener {
            val intent = Intent(view!!.context, SpecialistCenter::class.java)
            intent.putExtra("token", token)
            startActivity(intent)
        }
    }

    private fun hideViews() {
        issueCenter.visibility = View.GONE
        specialistCenter.visibility = View.GONE
        imageView.visibility = View.GONE

        pulseLoader.visibility = View.VISIBLE
        accessText.visibility = View.VISIBLE
    }

    private fun showViews() {
        issueCenter.visibility = View.VISIBLE
        specialistCenter.visibility = View.VISIBLE
        imageView.visibility = View.VISIBLE

        pulseLoader.visibility = View.GONE
        accessText.visibility = View.GONE
    }

    private fun showErrorContainer() {
        pulseLoader.visibility = View.GONE
        accessText.visibility = View.GONE
    }

    private fun showErrorDialog(view: View) {
        val dialog = LovelyStandardDialog(view.context)
        dialog.setTitle("Error")
                .setMessage("Unable to reach server")
                .setTopColor(Color.RED)
                .setIcon(resources.getDrawable(R.drawable.ic_info_outline))
                .setPositiveButton("Retry") { getToken(view) }
                .show()
    }

    private fun showInternetDialog(view: View) {
        notConnectedView.visibility = View.VISIBLE

        retryButton.setOnClickListener {
            if(!isConnectedToInternet(view.context)) {
                Toast.makeText(view.context, "Please check your internet connection and try again later", Toast.LENGTH_SHORT).show()
            }else {
                notConnectedView.visibility = View.GONE
                getToken(this.view!!)
                hideViews()
            }
        }
    }

    private fun getToken(view: View) {
        view.run {
            var transferResponse: String? = ""

            try {
                val urlString = StringBuilder(API_MEDIC_LIVE_AUTH_SERVICE).toString()

                val authString = "Bearer $API_MEDIC_LIVE_USERNAME:${convertToMD5(API_MEDIC_LIVE_PASS, API_MEDIC_LIVE_AUTH_SERVICE)}"

                AndroidNetworking.post(urlString)
                        .setTag(TAG)
                        .setPriority(Priority.HIGH)
                        .addHeaders("Content-Type", "application/json")
                        .addHeaders("Authorization", authString)
                        .build()
                        .getAsJSONObject(object : JSONObjectRequestListener {
                            override fun onResponse(response: JSONObject?) {
                                transferResponse = response.toString()
                                token = response!!.getString("Token")

                                staticToken = token

                                Log.d(TAG, "response is: $token")

                                showViews()
                                initListeners()
                            }

                            override fun onError(anError: ANError?) {
                                transferResponse = anError.toString()
                                showErrorDialog(view)
                                Log.e(TAG, "error: ${anError!!.message}")
                                Log.e(TAG, "error: ${anError.response}")
                            }

                        })

            } catch (ex: Exception) {
                Log.e(TAG, ex.message)
            }
        }
    }

}
