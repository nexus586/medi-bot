package com.nexus.apps.medibot.models

data class IssueInfo(
        var description: String? = "",
        var descriptionShort: String? = "",
        var medicalCondition: String? = "",
        var name: String? = "",
        var profName: String? = "",
        var possibleSymptoms: String? = "",
        var synonyms: String? = "",
        var treatmentDescription: String? = ""
)