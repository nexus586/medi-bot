package com.nexus.apps.medibot.models

data class Source(
        var id: Int?,
        var name: String
)