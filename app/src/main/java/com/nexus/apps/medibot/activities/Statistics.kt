package com.nexus.apps.medibot.activities

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.util.Log
import android.view.View
import android.widget.*
import com.airbnb.lottie.LottieAnimationView
import com.anychart.AnyChart
import com.anychart.AnyChartView
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.charts.Cartesian
import com.anychart.enums.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.models.Allergy
import com.nexus.apps.medibot.models.BotMessage
import com.nexus.apps.medibot.models.Medications
import com.nexus.apps.medibot.utils.FIREBASE_REFERENCE
import com.nexus.apps.medibot.utils.isConnectedToInternet




class Statistics : AppCompatActivity() {

    private val TAG = this::class.java.simpleName

    //    RelativeLayouts
    private lateinit var container: RelativeLayout
    private lateinit var loadingContainer: RelativeLayout

    //    CardViews
    private lateinit var medicationCard: CardView
    private lateinit var allergyCard: CardView
    private lateinit var chatHistoryCard: CardView

    //    TextViews
    private lateinit var medicationNumberText: TextView
    private lateinit var allergyNumberText: TextView
    private lateinit var chatHistoryNumberText: TextView

    private lateinit var notConnectedLayout: RelativeLayout
    private lateinit var retryButton: Button

    private lateinit var notConnectedView: LottieAnimationView

    private lateinit var mFirebaseAuth: FirebaseAuth
    private lateinit var mFirebaseDatabase: FirebaseDatabase
    private lateinit var mDatabaseReference: DatabaseReference


    private var hasLoadedAllergies = false
    private var hasLoadedMedication = false
    private var hasLoadedChat = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistics)

        val actionBar = supportActionBar
        actionBar?.apply {
            title = "Statistics"
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_outline_profile)
        }

        initViews()
        initFirebase()
    }

    private fun initViews() {
        medicationCard = findViewById(R.id.medication_card_item)
        allergyCard = findViewById(R.id.allergy_card_item)
        chatHistoryCard = findViewById(R.id.chat_card_item)
        chatHistoryNumberText = findViewById(R.id.diagnosis_num_text)
        allergyNumberText = findViewById(R.id.allergy_num_text)
        retryButton = findViewById(R.id.retry_button)
        medicationNumberText = findViewById(R.id.med_num_text)

        notConnectedView = findViewById(R.id.not_connected_image)
        container = findViewById(R.id.container)
        loadingContainer = findViewById(R.id.loading_container)
        notConnectedLayout = findViewById(R.id.not_connected_view)
    }


    private fun initListeners() {
        medicationCard.setOnClickListener {
            val intent = Intent(this, TestChart::class.java)
            startActivity(intent)
        }

        allergyCard.setOnClickListener {
            val intent = Intent(this, Allergies::class.java)
            startActivity(intent)
        }

        chatHistoryCard.setOnClickListener {
            val intent = Intent(this, History::class.java)
            startActivity(intent)
        }

    }

    private fun initFirebase() {
        mFirebaseAuth = FirebaseAuth.getInstance()
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase.getReference(FIREBASE_REFERENCE)
        if(isConnectedToInternet(this)){
            loadData()
        }else {
            showNotConnectedView()
        }
    }

    private fun getAllergiesSizeFromFirebase() {
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("allergies").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.w(TAG, "loadPost:onCancelled", p0.toException())
            }

            override fun onDataChange(p0: DataSnapshot) {
                Log.i(TAG, "Number of allergies = ${p0.childrenCount}")
                allergyNumberText.text = allergyNumberText.text.toString()+ ":"+ p0.childrenCount.toString()
                hasLoadedAllergies = true
                if (hasLoadedAllergies) {
                    getChatHistorySizeFromFirebase()
                }
            }
        })
    }

    private fun getMedicationSizeFromFirebase() {
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("medications").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.w(TAG, "loadPost:onCancelled", p0.toException())
            }

            override fun onDataChange(p0: DataSnapshot) {

                Log.i(TAG, "Number of medication = ${p0.childrenCount}")
                medicationNumberText.text =medicationNumberText.text.toString() +":"+ p0.childrenCount.toString()

                hasLoadedMedication = true
                if (hasLoadedMedication) {
                    showViews()
                    initListeners()
                }
            }
        })
    }

    private fun getChatHistorySizeFromFirebase() {
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("diagnoses").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.w(TAG, "loadPost:onCancelled", p0.toException())
            }

            override fun onDataChange(p0: DataSnapshot) {

                Log.i(TAG, "Number of chats = ${p0.childrenCount}")
                chatHistoryNumberText.text =chatHistoryNumberText.text.toString() +":"+ p0.childrenCount.toString()
                hasLoadedChat = true
                if (hasLoadedChat) {
                    getMedicationSizeFromFirebase()
                }
            }

        })
    }

    private fun loadData() {
        getAllergiesSizeFromFirebase()
    }

    private fun showViews() {
        container.visibility = View.VISIBLE
        loadingContainer.visibility = View.GONE
    }

    private fun showNotConnectedView() {
        notConnectedLayout.visibility = View.VISIBLE
        loadingContainer.visibility = View.GONE

        retryButton.setOnClickListener {
            Log.i(TAG, "retry button clicked")
            if(isConnectedToInternet(this@Statistics)) {
                loadingContainer.visibility = View.VISIBLE
                notConnectedLayout.visibility = View.GONE
                loadData()
            }else {
                Toast.makeText(this@Statistics, "Please check your internet connection and try again later", Toast.LENGTH_LONG).show()
            }
        }
    }

}
