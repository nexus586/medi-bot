package com.nexus.apps.medibot.models

data class Allergy(
        var id: String,
        var allergy: String
)