package com.nexus.apps.medibot.activities

import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.agrawalsuneet.loaderspack.loaders.PulseLoader
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.adapters.AllergiesAdapter
import com.nexus.apps.medibot.models.Allergy
import com.nexus.apps.medibot.utils.FIREBASE_REFERENCE
import com.yarolegovich.lovelydialog.LovelyInfoDialog
import com.yarolegovich.lovelydialog.LovelyTextInputDialog
import java.util.*
import kotlin.collections.ArrayList

class Allergies : AppCompatActivity() {

    private val TAG = this::class.java.simpleName

    private lateinit var pulseLoader: PulseLoader
    private lateinit var isLoadingText: TextView
    private lateinit var fab: FloatingActionButton
    private lateinit var recyclerView: RecyclerView
    private lateinit var mFirebaseAuth: FirebaseAuth
    private lateinit var mDatabase: FirebaseDatabase
    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var adapter: AllergiesAdapter
    private lateinit var notFoundLayout: RelativeLayout
    private lateinit var listener: ValueEventListener
    private lateinit var allergiesDatabaseReference: DatabaseReference

    private var isLoading = true

    private var allergiesList: ArrayList<Allergy> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_allergies)

        initViews()
        initFireBase()
        initListeners()
        if (isLoading) {
            hideViews()
        }
    }

    private fun initViews() {
        val actionBar = supportActionBar
        actionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_outline_profile)
            title = "Allergies"
        }

        pulseLoader = findViewById(R.id.pulse_loader)
        isLoadingText = findViewById(R.id.isLoadingText)
        fab = findViewById(R.id.fab)
        recyclerView = findViewById(R.id.allergies)
        notFoundLayout = findViewById(R.id.not_available_layout)
    }

    private fun initAdapters() {
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        adapter = AllergiesAdapter(this@Allergies, allergiesList)
        recyclerView.adapter = adapter
    }

    private fun initFireBase() {
        mFirebaseAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase.getReference(FIREBASE_REFERENCE)
        getAllergiesFromFirebase()
    }

    private fun initListeners() {
        fab.setOnClickListener {
            showAddAllergiesDialog()
        }
    }

    private fun showAddAllergiesDialog() {
        val dialog = LovelyTextInputDialog(this)
        dialog
                .setTitle("Add Allergies")
                .setHint("Please enter your allergy")
                .setConfirmButton("Add Allergy") { text -> addAllergyToFirebase(text, dialog) }
                .setTopColor(resources.getColor(R.color.colorPrimary))
                .show()
    }

    private fun addAllergyToFirebase(text: String, dialog: LovelyTextInputDialog) {
        Log.i(TAG, "Function to add allergy called")
        if (text.isEmpty()) {
            Log.i(TAG, "Allergy text field empty, alerting user")
            Toast.makeText(this@Allergies, "Please input an allergy", Toast.LENGTH_LONG).show()
            return
        }
        val allergyId: String = UUID.randomUUID().toString().split('-')[0]
        val allergy = Allergy(
                id = allergyId,
                allergy = text
        )
        Log.i(TAG, "Valid allergy input. Calling firebase to add allergy")
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("allergies").push().setValue(allergy).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                showSuccessDialog()
                notFoundLayout.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
                runOnUiThread {
                    adapter.add(allergy)
                    adapter.notifyDataSetChanged()
                }
                Log.i(TAG, "Allergy successfully saved to firebase. Added to UI view")
            } else {
                Log.i(TAG, "Failed to add allergy to firebase. Alerting user")
                showErrorDialog(task.exception!!.message)
            }

        }
    }

    private fun showSuccessDialog() {
        val dialog = LovelyInfoDialog(this)
        dialog.setTitle("Success")
                .setMessage("Allergy successfully added")
                .setTopColor(resources.getColor(R.color.colorPrimary))
                .setIcon(resources.getDrawable(R.drawable.ic_info_outline))
                .setConfirmButtonText("OK")
                .show()
    }

    private fun showErrorDialog(message: String?) {
        val dialog = LovelyInfoDialog(this)
        dialog.setTitle("Error")
                .setMessage(message)
                .setTopColor(Color.RED)
                .setIcon(resources.getDrawable(R.drawable.ic_info_outline))
                .setConfirmButtonText("OK")
                .show()
    }

    private fun getAllergiesFromFirebase() {
        Log.i(TAG, "Calling firebase to retrieve all allergies")
        allergiesDatabaseReference = mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("allergies")
        listener = allergiesDatabaseReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (postSnapshot in dataSnapshot.children) {
                    val temp = Allergy(allergy = postSnapshot.child("allergy").value.toString(), id = postSnapshot.child("id").value.toString())
                    allergiesList.add(temp)
                }
                Log.i(TAG, "Allergies loaded. Initializing views")
                isLoading = false
                initAdapters()
                showViews()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException())
                // ...
            }
        })
    }

    private fun showViews() {
        fab.visibility = View.VISIBLE
        allergiesDatabaseReference.removeEventListener(listener)

        if (allergiesList.size == 0) {
            notFoundLayout.visibility = View.VISIBLE
        } else {
            recyclerView.visibility = View.VISIBLE
        }

        isLoadingText.visibility = View.GONE
        pulseLoader.visibility = View.GONE
    }

    private fun hideViews() {
        fab.visibility = View.GONE
        recyclerView.visibility = View.GONE

        isLoadingText.visibility = View.VISIBLE
        pulseLoader.visibility = View.VISIBLE
    }

    fun showDeleteDialog(name: String, id: String) {
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle("Delete?")
                .setMessage("Are you sure you want to delete allergy with name $name?")
                .setPositiveButton("DELETE") { _, _ ->
                    deleteAllergies(id)
                }.setNegativeButton("CANCEL", null)
                .setIcon(R.drawable.allergy)
                .show()
    }

    private fun deleteAllergies(id: String){
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("allergies").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.e(TAG, "error: ${p0.message}")
                Log.e(TAG, "error details: ${p0.details}")
            }

            override fun onDataChange(p0: DataSnapshot) {
                for(snapshot in p0.children) {
                    if(snapshot.child("id").value.toString().equals(id, true)) {
                        snapshot.ref.removeValue()
                        adapter.notifyDataSetChanged()

                        Toast.makeText(this@Allergies, "Allergy deleted refreshing", Toast.LENGTH_SHORT).show()
                    }
                }
                finish()
                startActivity(intent)
            }

        })
    }

    private fun editAllergy(id: String, newName: String) {
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("allergies").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.e(TAG, "error: ${p0.message}")
                Log.e(TAG, "error details: ${p0.details}")
            }

            override fun onDataChange(p0: DataSnapshot) {
                for(snapshot in p0.children) {
                    if(snapshot.child("id").value.toString().equals(id, true)) {
                        snapshot.ref.child("allergy").setValue(newName)
                    }
                }
                Toast.makeText(this@Allergies, "Allergy updated refreshing", Toast.LENGTH_SHORT).show()
                finish()
                startActivity(intent)
            }

        })
    }

    fun showEditDialog(allergy: String, id: String) {

        Log.d(TAG, "id = [$id]")

        val dialog = LovelyTextInputDialog(this)
        dialog.setTitle("Edit")
                .setHint(allergy)
                .setMessage("Editing allergy with name $allergy")
                .setIcon(R.drawable.allergy)
                .setConfirmButton("Save") { text -> editAllergy(id, text!!) }
                .setCancelable(true)
                .setNegativeButton("Cancel", null)
                .show()
    }

}
