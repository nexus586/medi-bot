package com.nexus.apps.medibot.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.models.RiskFactor
import com.nexus.apps.medibot.utils.FIREBASE_REFERENCE

class HealthInfo : AppCompatActivity() {

    private val TAG = this::class.java.simpleName

    private lateinit var mFirebaseAuth: FirebaseAuth
    private lateinit var mDatabase: FirebaseDatabase
    private lateinit var mDatabaseReference: DatabaseReference
    private lateinit var riskFactorsDatabaseReference: DatabaseReference
    private lateinit var listener: ValueEventListener
    private lateinit var smokerRadioGroup: RadioGroup
    private lateinit var cancerRadioGroup: RadioGroup
    private lateinit var diabetesRadioGroup: RadioGroup
    private lateinit var smokerRiskYes: RadioButton
    private lateinit var smokerRiskNotAnswered: RadioButton
    private lateinit var smokerRiskNo: RadioButton
    private lateinit var cancerRiskYes: RadioButton
    private lateinit var cancerRiskNotAnswered: RadioButton
    private lateinit var cancerRiskNo: RadioButton
    private lateinit var diabetesRiskYes: RadioButton
    private lateinit var diabetesRiskNotAnswered: RadioButton
    private lateinit var diabetesRiskNo: RadioButton


    private val smokerKey = "p_28"
    private val smokerDescription = "smoker"
    private val cancerKey = "p_178"
    private val cancerDescription = "cancer"
    private val diabetesKey = "p_8"
    private val diabetesDescription = "diabetes"
    private var isLoading = true
    private var smokerRiskFactorId = ""
    private var cancerRiskFactorId =""
    private  var diabetesRiskFactorId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_health_info)
        initViews()
        initFireBase()
        initListeners()
    }

    private fun initFireBase() {
        mFirebaseAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase.getReference(FIREBASE_REFERENCE)
        getRiskFactorsFromFirebase()
    }

    override fun onResume() {
        super.onResume()
        initFireBase()
    }

    private fun initViews(){
        val actionBar = supportActionBar
        actionBar?.apply {
            setHomeAsUpIndicator(R.drawable.ic_outline_profile)
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
            title = "Health Information"
        }
        smokerRadioGroup = findViewById(R.id.smoker_radio_group)
        cancerRadioGroup = findViewById(R.id.cancer_radio_group)
        diabetesRadioGroup = findViewById(R.id.diabetes_radio_group)
        smokerRiskYes = findViewById(R.id.smoker_risk_yes)
        smokerRiskNotAnswered = findViewById(R.id.smoker_risk_not_answered)
        smokerRiskNo = findViewById(R.id.smoker_risk_no)
        cancerRiskYes = findViewById(R.id.cancer_risk_yes)
        cancerRiskNotAnswered = findViewById(R.id.cancer_risk_not_answered)
        cancerRiskNo = findViewById(R.id.cancer_risk_no)
        diabetesRiskYes = findViewById(R.id.diabetes_risk_yes)
        diabetesRiskNo = findViewById(R.id.diabetes_risk_no)
        diabetesRiskNotAnswered = findViewById(R.id.diabetes_risk_not_answered)
    }

    private fun initListeners(){
        smokerRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            when(checkedId){
                R.id.smoker_risk_yes -> {
                    updateRiskFactorFirebase(smokerRiskFactorId, RiskFactor(key = smokerKey, value = "YES", description = smokerDescription))
                }
                R.id.smoker_risk_not_answered -> {
                    updateRiskFactorFirebase(smokerRiskFactorId, RiskFactor(key = smokerKey, description = smokerDescription))
                }
                R.id.smoker_risk_no -> {
                    updateRiskFactorFirebase(smokerRiskFactorId, RiskFactor(key = smokerKey, value = "NO", description = smokerDescription))
                }
            }
        }

        cancerRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            when(checkedId){

                R.id.cancer_risk_yes -> {
                    updateRiskFactorFirebase(cancerRiskFactorId, RiskFactor(key = cancerKey, value = "YES", description = cancerDescription))
                }
                R.id.cancer_risk_not_answered -> {
                    updateRiskFactorFirebase(cancerRiskFactorId, RiskFactor(key = cancerKey, value = "NOT_ANSWERED", description = cancerDescription))
                }
                R.id.cancer_risk_no -> {
                    updateRiskFactorFirebase(cancerRiskFactorId, RiskFactor(key = cancerKey, value = "NO", description = cancerDescription))
                }
            }
        }

        diabetesRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            when(checkedId){
                R.id.diabetes_risk_yes -> {
                    updateRiskFactorFirebase(diabetesRiskFactorId, RiskFactor(key = diabetesKey, value = "YES", description = diabetesDescription))
                }
                R.id.diabetes_risk_not_answered -> {
                    updateRiskFactorFirebase(diabetesRiskFactorId, RiskFactor(key = diabetesKey, value = "NOT_ANSWERED", description = diabetesDescription))
                }
                R.id.diabetes_risk_no -> {
                    updateRiskFactorFirebase(diabetesRiskFactorId, RiskFactor(key = diabetesKey, value = "NO", description = diabetesDescription))
                }
            }
        }
    }


    private fun getRiskFactorsFromFirebase(){
        riskFactorsDatabaseReference  = mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("risk_factors")
        listener = riskFactorsDatabaseReference.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.w(TAG, "loadPost:onCancelled", p0.toException())
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if(dataSnapshot.children.count() == 0){
                    addDefaultRiskFactors()
                }
                for (postSnapshot in dataSnapshot.children){
                    val temp = RiskFactor(
                            key = postSnapshot.child("key").value.toString(),
                            value = postSnapshot.child("value").value.toString(),
                            description = postSnapshot.child("description").value.toString()
                    )

                    when (postSnapshot.child("key").value.toString()) {
                        smokerKey -> {
                            smokerRiskFactorId = postSnapshot.key.toString()
                        }
                        cancerKey -> {
                            cancerRiskFactorId = postSnapshot.key.toString()
                        }
                        diabetesKey -> {
                            diabetesRiskFactorId = postSnapshot.key.toString()
                        }
                    }

                    when(temp.key){
//                        smokers risk factor
                        smokerKey -> {
                            when(temp.value){
                                "YES" -> {
                                    smokerRiskYes.isChecked = true
                                }
                                "NOT_ANSWERED" -> {
                                    smokerRiskNotAnswered.isChecked = true
                                }
                                "NO" -> {
                                    smokerRiskNo.isChecked = true
                                }
                            }
                        }
//                        cancer risk factor
                        cancerKey -> {
                            when(temp.value){
                                "YES" -> {
                                    cancerRiskYes.isChecked = true
                                }
                                "NOT_ANSWERED" -> {
                                    cancerRiskNotAnswered.isChecked = true
                                }
                                "NO" -> {
                                    cancerRiskNo.isChecked = true
                                }
                            }
                        }
//                        diabetes risk factor
                        diabetesKey -> {
                            when(temp.value){
                                "YES" -> {
                                    diabetesRiskYes.isChecked = true
                                }
                                "NOT_ANSWERED" -> {
                                    diabetesRiskNotAnswered.isChecked = true
                                }
                                "NO" -> {
                                    diabetesRiskNo.isChecked = true
                                }
                            }
                        }
                    }
                }
                Log.i(TAG, "HealthInfo loaded. Initializing views")
                isLoading = false
                riskFactorsDatabaseReference.removeEventListener(listener)
            }

        })
    }

    private fun updateRiskFactorFirebase(riskFactorKey: String, updatedValue: RiskFactor ){
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("risk_factors").child(riskFactorKey).setValue(updatedValue).addOnCompleteListener { task ->
            if(task.isSuccessful) {
                Log.i(TAG, "Updated risk factor value")
            }else {
                Log.e(TAG, task.exception.toString())
            }
        }
    }

    private fun addDefaultRiskFactors(){
        val smokerDefault = RiskFactor(key = smokerKey, description = smokerDescription)
        val cancerDefault = RiskFactor(key = cancerKey, description = cancerDescription)
        val diabetesDefault = RiskFactor(key = diabetesKey, description = diabetesDescription)
        Log.i(TAG, "No risk factors saved in firebase. Adding default 3")
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("risk_factors").push().setValue(smokerDefault).addOnCompleteListener { task ->
            if(task.isSuccessful) {
                Log.i(TAG, "added default risk factor smoking to firebase")
            }else {
                Log.e(TAG, task.exception.toString())
            }
        }
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("risk_factors").push().setValue(cancerDefault).addOnCompleteListener { task ->
            if(task.isSuccessful) {
                Log.i(TAG, "added default risk factor cancer to firebase")
            }else {
                Log.e(TAG, task.exception.toString())
            }
        }
        mDatabaseReference.child("users").child(mFirebaseAuth.currentUser!!.uid).child("risk_factors").push().setValue(diabetesDefault).addOnCompleteListener { task ->
            if(task.isSuccessful) {
                Log.i(TAG, "added default risk factor diabetes to firebase")
            }else {
                Log.e(TAG, task.exception.toString())
            }
        }
    }
}
