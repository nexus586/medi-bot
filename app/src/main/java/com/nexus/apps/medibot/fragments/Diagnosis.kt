package com.nexus.apps.medibot.fragments


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nexus.apps.medibot.R
import com.nexus.apps.medibot.activities.AnonymousDiagnosis
import com.nexus.apps.medibot.activities.PersonalDiagnosis

/**
 * A simple [Fragment] subclass.
 *
 */
class Diagnosis : Fragment() {

    private lateinit var personalDiagnosisCardView: CardView
    private lateinit var anonymousDiagnosisCardView: CardView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_diagnosis, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initListeners()
    }

    private fun initViews() {
        personalDiagnosisCardView = view!!.findViewById(R.id.personal_diagnosis_item)
        anonymousDiagnosisCardView = view!!.findViewById(R.id.anonymous_diagnosis_item)
    }

    private fun initListeners() {
        personalDiagnosisCardView.setOnClickListener {
            val intent = Intent(view!!.context, PersonalDiagnosis::class.java)
            startActivity(intent)
        }

        anonymousDiagnosisCardView.setOnClickListener {
            val intent = Intent(view!!.context, AnonymousDiagnosis::class.java)
            startActivity(intent)
        }
    }


}
